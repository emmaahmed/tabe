<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('type',50)->nullable(); //text,switch,radio
            $table->enum('property_type',['specification','feature'])->default('specification'); //text,switch,radio
            $table->foreignId('parent_id')->nullable()->references('id')
                ->on('properties')->nullOnDelete();
            $table->boolean('is_category_type')->default(false);
            $table->foreignId('translation_id')->references('id')
                ->on('properties')->nullable();
            $table->string('image')->nullable();

            $table->string('language', 2);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
};
