<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->nullable()
                ->references('id')->on('users')->nullOnDelete();
            $table->foreignId('advertise_id')->nullable()
                ->references('id')->on('advertisements')->nullOnDelete();
            $table->string('key');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_attributes');
    }
}
