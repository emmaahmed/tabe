<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommercialAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercial_advertisements', function (Blueprint $table) {
            $table->id();
            $table->boolean('approved')->default(0);

            $table->foreignId('category_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('country_id')->nullable()->constrained()->cascadeOnDelete();

            $table->enum('type', ['home_slider', 'commercial_banners']);
            $table->string('image')->nullable();
            $table->integer('period')->default(1);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->string('company_name')->nullable();
            $table->string('email')->nullable();
            $table->string('country_code')->nullable();
            $table->string('phone')->nullable();

            $table->text('description')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial_advertisements');
    }
}
