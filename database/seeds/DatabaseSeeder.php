<?php

use App\Models\Permission;
use App\Models\User;
use \App\Models\ReportReason;
use \App\Models\Setting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(WelcomeSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(CompaniesSeeder::class);

        $admin = User::where('name', 'admin')->first();
        if($admin){
            return false;
        }

        $admin = User::create([
            'active'        => 1,
            'account_type'  => 'admin',
            'name'          => 'admin',
            'email'         => 'admin@admin.com',
            'phone'         => '123456',
            'password'      => bcrypt('123123'),
        ]);

        $permissionName =
            [
                'Read Dashboard',
                'Create Categories', 'Read Categories', 'Update Categories', 'Delete Categories',
                'Create Advertisements', 'Read Advertisements', 'Update Advertisements', 'Delete Advertisements',
                'Create Countries', 'Read Countries', 'Update Countries', 'Delete Countries',
                'Create Banners', 'Read Banners', 'Update Banners', 'Delete Banners',
                'Create Welcomes', 'Read Welcomes', 'Update Welcomes', 'Delete Welcomes',
                'Create Settings', 'Read Settings', 'Update Settings', 'Delete Settings',
                'Create Notifications', 'Read Notifications', 'Update Notifications', 'Delete Notifications',
                'Create Companies', 'Read Companies', 'Update Companies', 'Delete Companies',
                'Create Users', 'Read Users', 'Update Users', 'Delete Users',
                'Create Admins', 'Read Admins', 'Update Admins', 'Delete Admins',
                'Read Reports'
            ];
        $permissionSlug =
            [
                'read_dashboard',
                'create_categories', 'read_categories', 'update_categories', 'delete_categories',
                'create_advertisements', 'read_advertisements', 'update_advertisements', 'delete_advertisements',
                'create_countries', 'read_countries', 'update_countries', 'delete_countries',
                'create_banners', 'read_banners', 'update_banners', 'delete_banners',
                'create_welcomes', 'read_welcomes', 'update_welcomes', 'delete_welcomes',
                'create_settings', 'read_settings', 'update_settings', 'delete_settings',
                'create_notifications', 'read_notifications', 'update_notifications', 'delete_notifications',
                'create_companies', 'read_companies', 'update_companies', 'delete_companies',
                'create_users', 'read_users', 'update_users', 'delete_users',
                'create_admins', 'read_admins', 'update_admins', 'delete_admins',
                'read_reports',
            ];

        for ($i=0; $i<count($permissionName); $i++){
            $permissions = Permission::create([
                'name'       => $permissionName[$i],
                'slug'       => $permissionSlug[$i],
            ]);

            $admin->permissions()->attach($permissions);
        }

        $reasons = [
            'محتوي غير لائق',
            'إعلان كاذب',
            'إعلان في القسم الخطأ',
            'غير ذلك',
        ];

        foreach ($reasons as $reason) {
            ReportReason::create([
                'reason' => $reason
            ]);
        }

        $settings = ['about_us', 'privacy_policy', 'terms_condition', 'help'];

        foreach ($settings as $setting) {
            Setting::create([
                'key' => $setting,
                'value' => 'تتألف شروط الاستخدام هذه من الشروط والأحكام الواردة أدناه بالإضافة إلى الشروط الموضحة في سياسة الخصوصية (وفقًا للتعديلات التي يتم إجراؤها من حين لآخر).
1 - حماية معلوماتك الشخصية: نحن نود مساعدتك في إتخاذ كل الخطوات اللازمة لحماية خصوصيتك ومعلوماتك. لذلك، يُرجى قراءة سياسة الخصوصية كي يتسنى لك فهم نوعية المعلومات التي نقوم بجمعها والاحتياطات اللازمة التي نتخذها لحماية معلوماتك الشخصية.
2 - قد نقوم، دون تحمل أي مسؤولية تجاهك، بتعديل شروط الاستخدام هذه دون إشعار مسبق وفي أي وقت. وستعد متابعة استخدامك للتطبيق بعد إجراء التعديلات في شروط الاستخدام، موافقة منك على كل هذه التعديلات. في حالة عدم موافقتك على شروط الاستخدام هذه وعلى الالتزام بها، لن يسمح لك باستخدام التطبيق او اي من الخدمات المتاحة من خلاله .
3 - أنت تقر بأننا نمتلك كافة الحقوق والعناوين الخاصة والمتعلقة بالتطبيق .
4 - ووفقًا للحد الأقصى المسموح من قبل قوانين حقوق المستهلك (كما هو معمول بها)، تخضع شروط الاستخدام هذه لقوانين الدولة، بغض النظر عن اختلاف الأحكام القانونية.',
                'image' => $setting == 'about_us' ? 'settings/setting1.png' : null,
                'language' => 'ar'
            ]);
        }

        Setting::create([
            'key' => 'premium_cost',
            'value' => 200,
            'language' => 'ar'
        ]);
    }
}
