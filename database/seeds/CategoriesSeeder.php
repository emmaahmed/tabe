<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\AttributeCategory;
use App\Models\Title;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_name = [
            'إعلانات VIP', 'سيارات', 'دراجات هوائية', 'دراجات نارية',
            'قوارب', 'بانشيات', 'سكوتر', 'شركات'];

        $title = [
            'نوع السيارة', 'نوع الدراجة الهوائية', 'نوع الدراجة النارية',
            'نوع القوارب', 'نوع البانشيات', 'نوع السكوتر',

            'فئه السيارة', 'فئه الدراجة الهوائية', 'فئه الدراجة النارية',
            'فئه القوارب', 'فئه البانشيات', 'فئه السكوتر',

            'سنة الصنة', 'سنة الصنة', 'سنة الصنة',
            'سنة الصنة', 'سنة الصنة', 'سنة الصنة'];

        $image = [
            'categories/vip.png',
            'categories/cars.png',
            'categories/bicycles.png',
            'categories/motorcycles.png',
            'categories/boats.png',
            'categories/panchayat.png',
            'categories/scooter.png',
            'categories/companies.png',
        ];

        $sub_category_name = [
            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',

            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',

            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',

            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',

            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',

            'تيسلا', 'فولكس واجن', 'بي إم دبليو', 'لاند روفر',
            'فيات', 'ميني',
            ];

        $sub_category_child_name = [
            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',

            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',

            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',

            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',

            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',

            'تسلا موديل Y', 'تسلا موديل S', 'باسات بي1', 'بورا', 'رودستر', 'إي9',
            'Discovery', 'رانج روفر Vogue', 'فيات تيبو', 'فيات تيبو فيس ليفت',
            'موريس ميني مينور', 'ميني كوبر إس',
        ];

        $sub_category_child_child_name = [
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',

            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',

            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',

            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',

            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',

            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
            '2011', '2012', '2013', '2011', '2012', '2013', '2011', '2012', '2013',
        ];

        $sub_image = [
            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

            'categories/sub_categories/tesla.png', 'categories/sub_categories/volkswagen.png',
            'categories/sub_categories/bmw.png', 'categories/sub_categories/landrover.png',
            'categories/sub_categories/fiat.png', 'categories/sub_categories/mini.png',

        ];

        $attribute_title = [
            'نوع الإعلان',
            'نوع الوقود',
            'لون السيارة',
            'ماتريال المقاعد',
            'نوع الناقل',
            'نوع سقف السيارة',
            'سيليندرز',
            'التكيف',
        ];

        $attribute_image = [
            null,
            null,
            null,
            null,
            'categories/attribute/conveyor_type.png',
            'categories/attribute/roof_type.png',
            'categories/attribute/cylinders.png',
            'categories/attribute/conditioning.png',
        ];

        $attribute_title_graphical_control_element = [
            'radio',
            'select',
            'select',
            'select',
            'horizontal_view',
            'horizontal_view',
            'horizontal_view',
            'horizontal_view',
        ];

        $attribute_no_val = [
            'عدد الكيلو مترات',
            'رقم السيارة',
            'بيع سريع',
            'إتجاهات',
            'تأمين',
            'بلوتوث',
            'سيارة كاملة',
            'سينسور ركن',
        ];

        $attribute_no_val_image = [
            'categories/attribute/kilometers.png',
            'categories/attribute/kilometers.png',
            'categories/attribute/thunderbolt.png',
            'categories/attribute/compass.png',
            'categories/attribute/bluetooth.png',
            'categories/attribute/insurance.png',
            'categories/attribute/surface1.png',
            'categories/attribute/sensor.png',
        ];

        $attribute_no_val_graphical_control_element = [
            'text',
            'text',
            'switch',
            'switch',
            'switch',
            'switch'
        ];

        $attribute_value = [
            'شراء', 'بيع', 'قطع غيار', 'ونشات',
            'البنزين', 'غاز البترول المسال', 'وقود الديزل', 'زيت تسخين',
            'الأزرق', 'الأحمر', 'الرمادي', 'الأصفر',
            'فيكسكيب', 'والسر', 'بولي استر', 'مادة الغوص',
            'اليدوي', 'الاستيب ترونيك', 'الاوتوماتيكي', 'اليدوي',
            'غير موجوود', 'طبيعي', 'السقف الشمسي', 'السقف القمري',
            '4', '2', '3', '2.3',
            'ممتاز', 'جيد جدا', 'جيد', 'مقبول',
        ];

        for ($i = 0; $i < 18; $i++) {
            Title::create([
                'name' => $title[$i]
            ]);
        }

        for ($i = 0; $i < 8; $i++) {

            $category = Category::create([
                'name'          => $category_name[$i],
                'image'         => $image[$i],
                'is_company'    => $i == 7 ? 1 : 0,
                'language'      => 'ar',
            ]);

            if($i != 0 && $i != 7) {

                for ($a = 0; $a < 6; $a++) {
                    $sub_cat = Category::create([
                        'parent_id' => $category->id,
                        'title_id'  => 1,
                        'name'      => $sub_category_name[$a],
                        'image'     => $sub_image[$a],
                        'language'  => 'ar',
                    ]);

                    for ($d = 0; $d < 2; $d++) {
                        $child = Category::create([
                            'parent_id' => $sub_cat->id,
                            'title_id'  => 7,
                            'name'      => $sub_category_child_name[$d],
                            'language'  => 'ar',
                        ]);

                        for ($s = 0; $s < 3; $s++) {
                            Category::create([
                                'parent_id' => $child->id,
                                'title_id'  => 13,
                                'name'      => $sub_category_child_child_name[$s],
                                'language'  => 'ar',
                            ]);
                        }
                    }
                }
            }
        }

        for ($i = 0; $i < 8; $i++) {
            $attribute = AttributeCategory::create([
                'category_id' => 2,
                'used_tag' => $i == 0 ? 1 : 0,
                'key' => $attribute_title[$i],
                'image' => $attribute_image[$i],
                'graphical_control_element' => $attribute_title_graphical_control_element[$i],
                'language' => 'ar'
            ]);

            for ($d = 0; $d < 4; $d++) {
                AttributeCategory::create([
                    'parent_id' => $attribute->id,
                    'value' => $attribute_value[$d],
                    'language' => 'ar'
                ]);
            }
        }

        for ($i = 0; $i < 6; $i++) {
            AttributeCategory::create([
                'category_id' => 2,
                'key' => $attribute_no_val[$i],
                'image' => $attribute_no_val_image[$i],
                'graphical_control_element' => $attribute_no_val_graphical_control_element[$i],
                'language' => 'ar'
            ]);
        }
    }
}
