<?php

use Illuminate\Database\Seeder;

class WelcomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = ['welcome/welcome1.png', 'welcome/welcome2.png', 'welcome/welcome3.png'];

        foreach ($images as $image) {
            \App\Models\Welcome::create([
                'title' => 'شاشات ترحيبية',
                'content' => 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه … بروشور او فلاير على سبيل المثال … او نماذج مواقع انترنت',
                'image' => $image,
                'language' => 'ar',
            ]);
        }
    }
}
