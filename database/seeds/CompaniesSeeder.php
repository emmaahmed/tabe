<?php

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\CompanyImage;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = ['شركة المتحدة للتأمينات', 'شركة الأول لصيانة الدراجات النارية',
            'شركة المتحدة للتأمينات'];

        $image = [
            'companies/company1.png',
            'companies/company2.png',
            'companies/company3.png',
        ];


        $description = [
            'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا',
            'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا',
            'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا'
        ];

        $address = [
            'الزهراء شارع الاسماعلية مدينة نصر', 'الزهراء شارع الاسماعلية مدينة نصر',
            'الزهراء شارع الاسماعلية مدينة نصر',
        ];

        $images = [
            'companies/images/image1.png', 'companies/images/image2.png', 'companies/images/image3.png',
            'companies/images/image1.png', 'companies/images/image2.png', 'companies/images/image3.png',
            'companies/images/image1.png', 'companies/images/image2.png', 'companies/images/image3.png',
        ];

        for ($i = 0; $i < 3; $i++) {
            $company = Company::create([
                'name'          => $name[$i],
                'description'   => $description[$i],
                'image'         => $image[$i],
                'address'       => $address[$i],
                'latitude'      => '30.0644261',
                'longitude'     => '31.3504596',
                'language'      => 'ar',
            ]);

            for ($a = 0; $a < 3; $a++) {
                CompanyImage::create([
                    'company_id'    => $company->id,
                    'image'         => $images[$a],
                ]);
            }
        }
    }
}
