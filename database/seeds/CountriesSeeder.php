<?php

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\Governorate;
use App\Models\City;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name_ar = ['البحرين', 'الكويت', 'قطر', 'عمان', 'السعودية', 'الامارات'];
        $currency_ar = ['دينار', 'دينار', 'ريال', 'ريال', 'ريال', 'دينار'];
        $image = [
            'countries/flag-united-arab-emirates.png',
            'countries/flag-saudi-arabia.png',
            'countries/flag-oman.png',
            'countries/flag-qatar.png',
            'countries/flag-kuwait.png',
            'countries/flag-bahrain.png'
        ];


        $gov_name_ar = ['الظاهرة', 'الداخلية', 'الرياض', 'الدرعية', 'أبوظبي', 'دبي',
            'المحرق', 'العاصمة', 'الأحمدي', 'العاصمة', 'الرويس', 'الدوحة'];

        $city_name_ar = [
            'عسير', 'الشرقية', 'القصيم', 'عسير', 'الشرقية', 'القصيم',
            'القاهرة', 'الإسكندرية', 'أسوان', 'القاهرة', 'الإسكندرية', 'أسوان',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي'
        ];

        $area_name_ar = [
            'عسير', 'الشرقية', 'القصيم', 'عسير', 'الشرقية', 'القصيم',
            'القاهرة', 'الإسكندرية', 'أسوان', 'القاهرة', 'الإسكندرية', 'أسوان',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي',
            'عسير', 'الشرقية', 'القصيم', 'عسير', 'الشرقية', 'القصيم',
            'القاهرة', 'الإسكندرية', 'أسوان', 'القاهرة', 'الإسكندرية', 'أسوان',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي',
            'العاصمة', 'حولي', 'الأحمدي', 'العاصمة', 'حولي', 'الأحمدي'
        ];

        for ($i = 0; $i < 6; $i++) {
            $country = Country::create([
                'name'      => $name_ar[$i],
                'currency'  => $currency_ar[$i],
                'image'     => $image[$i],
                'language'  => 'ar',
            ]);

            for ($a = 0; $a < 2; $a++) {
                $gov = Governorate::create([
                    'country_id'    => $country->id,
                    'name'          => $gov_name_ar[$a],
                    'language'      => 'ar',
                ]);

                for ($y = 0; $y < 2; $y++) {
                    $city = City::create([
                        'governorate_id'    => $gov->id,
                        'name'              => $city_name_ar[$y],
                        'language'          => 'ar',
                    ]);
                }

                for ($s = 0; $s < 2; $s++) {
                    $city = \App\Models\Area::create([
                        'city_id'    => $city->id,
                        'name'       => $area_name_ar[$s],
                        'language'   => 'ar',
                    ]);
                }

            }
        }
    }
}
