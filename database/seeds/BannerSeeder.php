<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $link_type = ['', 'company', 'advertisement'];
        $images = ['banners/banner1.png', 'banners/banner2.png', 'banners/banner3.png'];
        $link_id = [0, 1, 1];

        for ($i = 0; $i < 3; $i++) {
            \App\Models\Banner::create([
                'link_type' => $link_type[$i],
                'link_id' => $link_id[$i],
                'image' => $images[$i],
            ]);
        }
    }
}
