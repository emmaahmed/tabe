<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Datatable\GetAdmins;
use App\Http\Controllers\Api\Datatable\GetCategories;
use App\Http\Controllers\Api\Datatable\GetHistories;
use App\Http\Controllers\Api\Datatable\GetCategoryAttributes;
use App\Http\Controllers\Api\Datatable\GetAdvertisements;
use App\Http\Controllers\Api\Datatable\GetCompanies;
use App\Http\Controllers\Api\Datatable\GetReportedAdvertisements;
use App\Http\Controllers\Api\Datatable\GetAdvertisementsComments;
use App\Http\Controllers\Api\Datatable\GetCountries;
use App\Http\Controllers\Api\Datatable\GetGovernorates;
use App\Http\Controllers\Api\Datatable\GetCities;
use App\Http\Controllers\Api\Datatable\GetAreas;
use App\Http\Controllers\Api\Datatable\GetBanners;
use App\Http\Controllers\Api\Datatable\GetWelcomes;
use App\Http\Controllers\Api\Datatable\GetReportReasons;
use App\Http\Controllers\Api\Datatable\GetUsers;
use App\Http\Controllers\Api\Datatable\GetNotifications;
use App\Http\Controllers\Api\Datatable\GetSettings;
use App\Http\Controllers\Api\Datatable\GetContactUs;
use App\Http\Controllers\Api\Datatable\GetComplainSuggestion;
use App\Http\Controllers\Api\Datatable\GetPackages;
use App\Http\Controllers\Api\Datatable\GetCommercialAdvertisements;

Route::prefix('/datatable')->group(function (){
    Route::get('admins', GetAdmins::class);
    Route::get('categories', GetCategories::class);
    Route::get('histories', GetHistories::class);
    Route::get('packages', GetPackages::class);
    Route::get('category/attributes', GetCategoryAttributes::class);
    Route::get('companies', GetCompanies::class);
    Route::get('advertisements', GetAdvertisements::class);
    Route::get('advertisements/reports', GetReportedAdvertisements::class);
    Route::get('advertisements/comments', GetAdvertisementsComments::class);
    Route::get('advertisements/commercial', GetCommercialAdvertisements::class);
//    Route::get('advertisements/report', GetAdvertisementReports::class);
    Route::get('countries', GetCountries::class);
    Route::get('governorates', GetGovernorates::class);
    Route::get('cities', GetCities::class);
    Route::get('areas', GetAreas::class);
    Route::get('banners', GetBanners::class);
    Route::get('welcomes', GetWelcomes::class);
    Route::get('report_reasons', GetReportReasons::class);
    Route::get('users', GetUsers::class);
    Route::get('notifications', GetNotifications::class);
    Route::get('settings', GetSettings::class);
    Route::get('contact_us', GetContactUs::class);
    Route::get('complain_suggestions', GetComplainSuggestion::class);

});

Route::prefix('/ajax')->group(function(){

});
