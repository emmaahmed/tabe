<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Company\AuthController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\AttributeCategoryController;
use App\Http\Controllers\Company\AdvertisementController;
use App\Http\Controllers\Company\CommentController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\GovernorateController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\AreaController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\WelcomeController;
use App\Http\Controllers\Admin\ReportReasonController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\ComplainSuggestionController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', function (){ return redirect()->route('admin.login'); });

Route::get('register', [AuthController::class, 'viewRegisterPage'])->name('viewRegister');
Route::post('register', [AuthController::class, 'register'])->name('register');
Route::get('login', [AuthController::class, 'viewLoginPage'])->name('login');
Route::post('login', [AuthController::class, 'authenticate'])->name('authenticate');

Route::middleware('auth')->group(function (){
    Route::get('dashboard', [HomeController::class, 'reportStatistics'])->name('dashboard.index');
//    Route::resource('categories', CategoryController::class, ['except' => 'create','edit']);
//    Route::resource('category_fields', AttributeCategoryController::class, ['except' => 'create','edit']);

    Route::get('advertisements/delete_image/{id}', [AdvertisementController::class, 'deleteImage'])->name('advertisements.deleteImage');
    Route::resource('advertisements', AdvertisementController::class, ['except' => 'show', 'store','create','edit']);

    Route::get('advertisements/reports', [AdvertisementController::class, 'reportedAdvertisements'])->name('advertisements.reports');
    Route::delete('advertisements/reports/destroy/{id}', [AdvertisementController::class, 'destroyReportedAdvertisements'])->name('advertisements.reports.destroy');

    Route::resource('comments', CommentController::class, ['except' => 'show', 'store','create','edit']);
//    Route::resource('countries', CountryController::class, ['except' => 'show','create','edit']);
//    Route::resource('governorates', GovernorateController::class, ['except' => 'show','create','edit']);
//    Route::resource('cities', CityController::class, ['except' => 'show','create','edit']);
//    Route::resource('areas', AreaController::class, ['except' => 'show','create','edit']);
//
//    Route::resource('banners', BannerController::class, ['except' => 'show','create','edit']);
//
//    Route::resource('notifications', NotificationController::class, ['except' => 'show','create','edit']);
//
//    Route::resource('settings', SettingController::class, ['except' => 'index','create','edit']);
//    Route::resource('contact_us', ContactUsController::class, ['except' => 'store', 'update', 'create','edit']);
//    Route::resource('complain_suggestions', ComplainSuggestionController::class, ['except' => 'store', 'update', 'create','edit']);
//    Route::resource('welcomes', WelcomeController::class, ['except' => 'show','create','edit']);
//    Route::resource('report_reasons', ReportReasonController::class, ['except' => 'show','create','edit']);
//
//    Route::get('admins/status/{id}', [AdminController::class, 'changeStatus'])->name('admins.changeStatus');
//    Route::resource('admins', AdminController::class, ['except' => 'show','create','edit']);
//
//    Route::post('users/notification/{id}', [UserController::class, 'sendNotification'])->name('users.sendNotification');
//    Route::get('users/status/{id}', [UserController::class, 'changeStatus'])->name('users.changeStatus');
//    Route::resource('users', UserController::class, ['except' => 'show','create','edit']);
//
//    Route::resource('companies', CompanyController::class, ['except' => 'create','edit']);
//
//    Route::resource('notifications', NotificationController::class, ['except' => 'show','create','update','edit']);

    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('set-locale/{locale}', function ($locale) {
        session()->put('locale', $locale);
        return redirect()->back();
    })->name('locale.setting');
});
