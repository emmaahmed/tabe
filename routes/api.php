<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V100\GetWelcomePage;
use App\Http\Controllers\Api\V100\Auth\RegisterUser;
use App\Http\Controllers\Api\V100\Auth\SendUserConfirmationCode;
use App\Http\Controllers\Api\V100\Auth\VerifyUserConfirmationCode;
use App\Http\Controllers\Api\V100\Auth\UserLogin;
use App\Http\Controllers\Api\V100\Auth\UserLogout;
use App\Http\Controllers\Api\V100\Auth\DeleteUser;
use App\Http\Controllers\Api\V100\Countries\GetCountries;
use App\Http\Controllers\Api\V100\Home\GetHomeDetails;
use App\Http\Controllers\Api\V100\Categories\GetCategories;
use App\Http\Controllers\Api\V100\Categories\GetSubCategory;
use App\Http\Controllers\Api\V100\Categories\GetAttributesCategory;
use App\Http\Controllers\Api\V100\Categories\GetAdvertisementTypes;
use App\Http\Controllers\Api\V100\Companies\GetCompanies;
use App\Http\Controllers\Api\V100\Advertisements\SetAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\GetAdvertisements;
use App\Http\Controllers\Api\V100\Advertisements\GetAdvertisementDetails;
use App\Http\Controllers\Api\V100\Advertisements\UpdateAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\DeleteAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\SetPurchasedAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\SetAdvertisementVip;
use App\Http\Controllers\Api\V100\Advertisements\DeleteAdvertisementImage;
use App\Http\Controllers\Api\V100\Advertisements\GetAdvertisementPremiumCost;
use App\Http\Controllers\Api\V100\Advertisements\Comments\GetAdvertisementComments;
use App\Http\Controllers\Api\V100\Advertisements\Comments\SetAdvertisementComment;
use App\Http\Controllers\Api\V100\Advertisements\Reports\SetAdvertisementReport;
use App\Http\Controllers\Api\V100\Advertisements\Reports\GetReportReasons;
use App\Http\Controllers\Api\V100\Advertisements\SearchAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\Commercial\SetCommercialAdvertisement;
use App\Http\Controllers\Api\V100\Advertisements\Commercial\GetCommercialAdvertisements;
use App\Http\Controllers\Api\V100\Favorites\SetFavorite;
use App\Http\Controllers\Api\V100\Favorites\GetFavorite;
use App\Http\Controllers\Api\V100\Follows\StoreFollower;
use App\Http\Controllers\Api\V100\Advertisements\Filters\GetFilterDetails;
use App\Http\Controllers\Api\V100\Advertisements\Filters\GetFilterResult;
use App\Http\Controllers\Api\V100\Advertisements\Filters\GetOrderBy;
use App\Http\Controllers\Api\V100\Advertisements\Filters\GetSearch;
use App\Http\Controllers\Api\V100\Advertisements\Filters\SetFilter;
use App\Http\Controllers\Api\V100\Advertisements\DeleteAdvertisementCronJob;
use App\Http\Controllers\Api\V100\Messages\GetChats;
use App\Http\Controllers\Api\V100\Messages\SetMessage;
use App\Http\Controllers\Api\V100\Messages\GetChatMessages;
use App\Http\Controllers\Api\V100\Profile\UpdateProfile;
use App\Http\Controllers\Api\V100\Profile\SetProfilePassword;
use App\Http\Controllers\Api\V100\Profile\GetProfile;
use App\Http\Controllers\Api\V100\Profile\GetProfileFavorite;
use App\Http\Controllers\Api\V100\Profile\GetProfileFollower;
use App\Http\Controllers\Api\V100\Profile\GetProfileFollowing;
use App\Http\Controllers\Api\V100\Profile\GetProfileAdvertisement;
use App\Http\Controllers\Api\V100\Profile\GetFollowingsAndFollowersCount;
use App\Http\Controllers\Api\V100\Notifications\GetNotifications;
use App\Http\Controllers\Api\V100\Settings\SetContactUs;
use App\Http\Controllers\Api\V100\Settings\GetContactUs;
use App\Http\Controllers\Api\V100\Settings\GetAboutUs;
use App\Http\Controllers\Api\V100\Settings\GetPrivacyPolicy;
use App\Http\Controllers\Api\V100\Settings\GetTermsAndCondition;
use App\Http\Controllers\Api\V100\Settings\GetHelp;
use App\Http\Controllers\Api\V100\Settings\SetComplaintsSuggestions;
use App\Http\Controllers\Api\V100\Companies\AddCompanyCategory;
use App\Http\Controllers\Api\V100\Packages\GetPackages;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function (){
    Route::get('cron-advertisement', DeleteAdvertisementCronJob::class);

    Route::get('welcome', GetWelcomePage::class);

    Route::post('register', RegisterUser::class);
    Route::post('code_send', SendUserConfirmationCode::class);
    Route::post('code_check', VerifyUserConfirmationCode::class);
    Route::post('login', UserLogin::class);

    Route::get('countries', GetCountries::class);

    Route::get('home', GetHomeDetails::class);

    Route::get('categories', GetCategories::class);
    Route::get('packages', GetPackages::class);
    Route::get('subcategory', GetSubCategory::class);
    Route::get('category/attributes', GetAttributesCategory::class);
    Route::get('get-advertisement-types', GetAdvertisementTypes::class);

    Route::get('companies', GetCompanies::class);
    Route::get('profile', GetProfile::class);

    Route::get('filter/details', GetFilterDetails::class);
    Route::get('filter/result', GetFilterResult::class);
    Route::post('filter/set-data', SetFilter::class);
    Route::get('order_by', GetOrderBy::class);
    Route::get('search', GetSearch::class);
    Route::get('search-text-results', [SearchAdvertisement::class,'searchResults']);
    Route::get('search-advertisement', [SearchAdvertisement::class,'searchAdvertisementByText']);

    Route::middleware('auth:sanctum')->group(function(){
        Route::post('logout', UserLogout::class);
        Route::get('delete_user', DeleteUser::class);

        Route::post('add-company-category', AddCompanyCategory::class);

        Route::post('set-advertisement', SetAdvertisement::class);
        Route::post('advertisement/comment', SetAdvertisementComment::class);
        Route::post('advertisement/report', SetAdvertisementReport::class);
        Route::get('advertisement/premium_cost', GetAdvertisementPremiumCost::class);
        Route::post('advertisement/{advertisement}', UpdateAdvertisement::class);
        Route::delete('advertisement/{advertisement}', DeleteAdvertisement::class);
        Route::post('advertisement/{advertisement}/purchased', SetPurchasedAdvertisement::class);
        //commercial

        Route::get('report_reasons', GetReportReasons::class);
        Route::post('advertisements/vip', SetAdvertisementVip::class);
        Route::delete('advertisement/image/{advertisement_image}', DeleteAdvertisementImage::class);

        Route::post('favorite', SetFavorite::class);
        Route::get('favorite', GetFavorite::class);

        Route::post('/follow', StoreFollower::class);

        Route::get('chats', GetChats::class);
        Route::post('message', SetMessage::class);
        Route::get('chat/messages', GetChatMessages::class);

        Route::get('notifications', GetNotifications::class);

        Route::post('profile', UpdateProfile::class);
        Route::post('profile/password', SetProfilePassword::class);
        Route::get('profile/favorite', GetProfileFavorite::class);
        Route::get('profile/followers', GetProfileFollower::class);
        Route::get('profile/followings', GetProfileFollowing::class);
        Route::get('profile/follower-and-followings-count', GetFollowingsAndFollowersCount::class);
        Route::get('profile/advertisements', GetProfileAdvertisement::class);


    });

        Route::get('advertisements', GetAdvertisements::class);
        Route::get('advertisement/{id}', GetAdvertisementDetails::class);
        Route::get('advertisement/{advertisement}/comments', GetAdvertisementComments::class);
    Route::post('commercial/set-advertisement', SetCommercialAdvertisement::class);
    Route::get('commercial/advertisements', GetCommercialAdvertisements::class);

        Route::get('contact_us', GetContactUs::class);
        Route::get('about_us', GetAboutUs::class);
        Route::get('privacy_policy', GetPrivacyPolicy::class);
        Route::get('terms_condition', GetTermsAndCondition::class);
        Route::get('help', GetHelp::class);

        Route::post('contact_us', SetContactUs::class);
        Route::post('complaints_suggestions', SetComplaintsSuggestions::class);
});

include 'api_datatable.php';
