url: `/api/v1/wallet` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  en | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "title": "شاشات ترحيبية",
            "content": "لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه … بروشور او فلاير على سبيل المثال … او نماذج مواقع انترنت",
            "image": "http://127.0.0.1:8000/storage/welcome/welcome1.png"
        },
        {
            "title": "شاشات ترحيبية",
            "content": "لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه … بروشور او فلاير على سبيل المثال … او نماذج مواقع انترنت",
            "image": "http://127.0.0.1:8000/storage/welcome/welcome2.png"
        },
        {
            "title": "شاشات ترحيبية",
            "content": "لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه … بروشور او فلاير على سبيل المثال … او نماذج مواقع انترنت",
            "image": "http://127.0.0.1:8000/storage/welcome/welcome3.png"
        }
    ]
}
```
