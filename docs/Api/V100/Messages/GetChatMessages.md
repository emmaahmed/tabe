url: `/api/v1/chat/messages` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| user_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "data": [
            {
                "id": 1,
                "message": "sadsadsa",
                "image": null,
                "time": "12:45",
                "user": {
                    "id": 3,
                    "name": "mohamed",
                    "email": null,
                    "phone": "1234567",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            },
            {
                "id": 2,
                "message": "sadsadsa",
                "image": null,
                "time": "12:49",
                "user": {
                    "id": 3,
                    "name": "mohamed",
                    "email": null,
                    "phone": "1234567",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            },
            {
                "id": 3,
                "message": "sadsadsa",
                "image": null,
                "time": "12:50",
                "user": {
                    "id": 3,
                    "name": "mohamed",
                    "email": null,
                    "phone": "1234567",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            },
            {
                "id": 4,
                "message": "sadsadsa",
                "image": null,
                "time": "12:51",
                "user": {
                    "id": 3,
                    "name": "mohamed",
                    "email": null,
                    "phone": "1234567",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            }
        ],
        "links": {
            "first": "https://myzoon.my-staff.net/api/v1/chat/1/messages?page=1",
            "last": "https://myzoon.my-staff.net/api/v1/chat/1/messages?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "https://myzoon.my-staff.net/api/v1/chat/1/messages",
            "per_page": 15,
            "to": 4,
            "total": 4
        }
    }
}
```
