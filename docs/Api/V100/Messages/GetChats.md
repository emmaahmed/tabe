url: `/api/v1/chats` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "data": [
            {
                "id": 1,
                "message": "sadsadsa",
                "image": null,
                "time": "08:35",
                "unread_count": 3,
                "user": {
                    "id": 3,
                    "name": "client 2",
                    "email": null,
                    "phone": "1234567",
                    "image": null,
                    "token": null
                }
            },
            {
                "id": 2,
                "message": "sadsadsa",
                "image": null,
                "created_at": "08:39",
                "unread_count": 3,
                "user": {
                    "id": 3,
                    "name": "client 2",
                    "email": null,
                    "phone": "1234567",
                    "image": null,
                    "token": null
                }
            },
            {
                "id": 3,
                "message": "sadsadsa",
                "image": null,
                "created_at": "08:59",
                "unread_count": 3,
                "user": {
                    "id": 3,
                    "name": "client 2",
                    "email": null,
                    "phone": "1234567",
                    "image": null,
                    "token": null
                }
            }
        ],
        "links": {
            "first": "http://127.0.0.1:8000/api/v1/messages?page=1",
            "last": "http://127.0.0.1:8000/api/v1/messages?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "http://127.0.0.1:8000/api/v1/messages",
            "per_page": 15,
            "to": 3,
            "total": 3
        }
    }
}
```
