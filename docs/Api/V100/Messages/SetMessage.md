url: `/api/v1/message` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| user_id |  required |  |
| message |  required |  |
| image |  nullable |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "id": 4,
        "message": "sadsadsa",
        "image": null,
        "time": "12:51",
        "user": {
            "id": 3,
            "name": "mohamed",
            "email": null,
            "phone": "1234567",
            "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
            "token": null,
            "is_follow": false
        }
    }
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
