url: `/api/v1/profile/advertisements` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| search |  optional |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "user": {
            "id": 6,
            "name": "Mohamed",
            "email": "mohamed123@yahoo.com",
            "phone": "01121805384",
            "image": "https://myzoon.my-staff.net/storage/users/C9xZmTiYWoGBX3Qcn35fOXfhYlwEQlq6a7tBkbtj.jpg",
            "token": null,
            "is_follow": false
        },
        "advertisements": {
            "data": [
                {
                    "id": 1,
                    "title": "title",
                    "price": 500,
                    "special": 0,
                    "is_favorite": true,
                    "image": "https://myzoon.my-staff.net/storage/advertisements/Zpf4KRAJLxWVLpRqmppV05P54bB7y9mUlcOFj4ev.jpg",
                    "count_images": 4,
                    "visible": 1,
                    "tag": {
                        "id": 5,
                        "name": "أخري",
                        "type": "radio",
                        "used_tag": 1
                    },
                    "created_at": "1 week ago",
                    "ended_at": 26
                },
                {
                    "id": 2,
                    "title": "title",
                    "price": 500,
                    "special": 1,
                    "is_favorite": true,
                    "image": "https://myzoon.my-staff.net/storage/advertisements/tso7BBZGYCg3iagaCgvBrJx9GYmsPs4bzqIKWO8h.jpg",
                    "count_images": 1,
                    "visible": 1,
                    "tag": {
                        "id": 5,
                        "name": "أخري",
                        "type": "radio",
                        "used_tag": 1
                    },
                    "created_at": "1 week ago",
                    "ended_at": 13
                },
                {
                    "id": 3,
                    "title": "Title",
                    "price": 20,
                    "special": 0,
                    "is_favorite": false,
                    "image": "https://myzoon.my-staff.net/storage/advertisements/QBxfG3HHTvkmTFjqtR3zEg5vcCHAXla8tmYnAggj.jpg",
                    "count_images": 2,
                    "visible": 1,
                    "tag": {
                        "id": 3,
                        "name": "للبيع",
                        "type": "radio",
                        "used_tag": 1
                    },
                    "created_at": "5 days ago",
                    "ended_at": 2
                },
                {
                    "id": 4,
                    "title": "hiii",
                    "price": 150,
                    "special": 0,
                    "is_favorite": false,
                    "image": "https://myzoon.my-staff.net/storage/advertisements/Lflf6KI5W6z3SueVhZgTvKhjX1CN0xTKqFvEUEyo.jpg",
                    "count_images": 3,
                    "visible": 1,
                    "tag": {
                        "id": 2,
                        "name": "مطلوبه",
                        "type": "radio",
                        "used_tag": 1
                    },
                    "created_at": "6 hours ago",
                    "ended_at": 29
                }
            ]
        }
    }
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
