url: `/api/v1/profile/favorite` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": [
        {
            "specifications": [
                {
                    "id": 4,
                    "name": "نوع الاعلان",
                    "values": [
                        {
                            "id": 5,
                            "name": "بيع",
                            "type": "radio",
                            "used_tag": 1
                        },
                        {
                            "id": 6,
                            "name": "ايجار",
                            "type": "radio",
                            "used_tag": 1
                        },
                        {
                            "id": 7,
                            "name": "مطلوب",
                            "type": "radio",
                            "used_tag": 1
                        }
                    ]
                }
            ],
            "id": 1,
            "title": "title",
            "price": 500,
            "special": 0,
            "is_favorite": true,
            "image": "http://127.0.0.1:8000/storage/advertisements/HvESg3KmHLcrnhok5axglY8Ctd4Ymk6D6vD89PLm.jpg",
            "tag": {
                "id": 5,
                "name": "بيع",
                "type": "radio",
                "used_tag": 1
            },
            "created_at": "3 days ago"
        }
    ]
}
```
Errors
```json
{
  "message": "Unauthenticated."
}
```
