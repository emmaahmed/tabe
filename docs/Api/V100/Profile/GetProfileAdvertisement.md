url: `/api/v1/profile/advertisements` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| search |  optional |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "advertisements": {
            "data": [
                {
                    "id": 1,
                    "title": "title",
                    "price": 500,
                    "special": 0,
                    "is_favorite": true,
                    "image": "http://127.0.0.1:8000/storage/advertisements/HvESg3KmHLcrnhok5axglY8Ctd4Ymk6D6vD89PLm.jpg",
                    "count_images": 4,
                    "visible": 0, // 0 تمت مراجعه الاعلان 1 قيد المراجعه,
                    "tag": {
                        "id": 5,
                        "name": "بيع",
                        "type": "radio",
                        "used_tag": 1
                    },
                    "created_at": "منذ 3 أيام",
                    "ended_at": 27
                }
            ],
            "links": {
                "first": "http://127.0.0.1:8000/api/v1/advertisements?page=1",
                "last": "http://127.0.0.1:8000/api/v1/advertisements?page=1",
                "prev": null,
                "next": null
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 1,
                "path": "http://127.0.0.1:8000/api/v1/advertisements",
                "per_page": 15,
                "to": 1,
                "total": 1
            }
        }
    }
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
