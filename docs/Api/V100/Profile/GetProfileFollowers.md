url: `/api/v1/profile/followers` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| company |  optional | company = 1 show all company followers |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "data": [
            {
                "id": 3,
                "name": "client",
                "email": null,
                "phone": "1234567",
                "image": null,
                "token": null,
                "is_follow": false
            }
        ],
        "links": {
            "first": "http://127.0.0.1:8000/api/v1/followers?page=1",
            "last": "http://127.0.0.1:8000/api/v1/followers?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "http://127.0.0.1:8000/api/v1/followers",
            "per_page": 15,
            "to": 1,
            "total": 1
        }
    }
}
```
