url: `/api/v1/categories` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "id": 2,
            "title": null,
            "name": "سيارات",
            "image": "http://127.0.0.1:8000/storage/categories/cars.png"
        },
        {
            "id": 9,
            "title": null,
            "name": "دراجات هوائية",
            "image": "http://127.0.0.1:8000/storage/categories/bicycles.png"
        },
        {
            "id": 16,
            "title": null,
            "name": "دراجات نارية",
            "image": "http://127.0.0.1:8000/storage/categories/motorcycles.png"
        },
        {
            "id": 23,
            "title": null,
            "name": "قوارب",
            "image": "http://127.0.0.1:8000/storage/categories/boats.png"
        },
        {
            "id": 30,
            "title": null,
            "name": "بانشيات",
            "image": "http://127.0.0.1:8000/storage/categories/panchayat.png"
        },
        {
            "id": 37,
            "title": null,
            "name": "سكوتر",
            "image": "http://127.0.0.1:8000/storage/categories/scooter.png"
        }
    ]
}
```
