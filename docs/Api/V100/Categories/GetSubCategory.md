url: `/api/v1/subcategory` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| category_id |  required |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "sub_categories": [
            {
                "id": 9,
                "name": "تيسلا",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/tesla.png",
                "attributes": 0
            },
            {
                "id": 10,
                "name": "فولكس واجن",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/volkswagen.png",
                "attributes": 0
            },
            {
                "id": 11,
                "name": "بي إم دبليو",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/bmw.png",
                "attributes": 0
            },
            {
                "id": 12,
                "name": "لاند روفر",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/landrover.png",
                "attributes": 0
            },
            {
                "id": 13,
                "name": "فيات",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/fiat.png",
                "attributes": 0
            },
            {
                "id": 14,
                "name": "ميني",
                "image": "http://127.0.0.1:8000/storage/categories/sub_categories/mini.png",
                "attributes": 0
            }
        ]
    }
}
```
