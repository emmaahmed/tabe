url: `/api/v1/category/attributes` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| category_id |  required |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "id": 1,
            "name": "نوع الإعلان",
            "type": "radio",
            "used_tag": 1,
            "image": null,
            "values": [
                {
                    "id": 2,
                    "name": "شراء"
                },
                {
                    "id": 3,
                    "name": "بيع"
                },
                {
                    "id": 4,
                    "name": "قطع غيار"
                },
                {
                    "id": 5,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 6,
            "name": "نوع الوقود",
            "type": "select",
            "used_tag": 0,
            "image": null,
            "values": [
                {
                    "id": 7,
                    "name": "شراء"
                },
                {
                    "id": 8,
                    "name": "بيع"
                },
                {
                    "id": 9,
                    "name": "قطع غيار"
                },
                {
                    "id": 10,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 11,
            "name": "لون السيارة",
            "type": "select",
            "used_tag": 0,
            "image": null,
            "values": [
                {
                    "id": 12,
                    "name": "شراء"
                },
                {
                    "id": 13,
                    "name": "بيع"
                },
                {
                    "id": 14,
                    "name": "قطع غيار"
                },
                {
                    "id": 15,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 16,
            "name": "ماتريال المقاعد",
            "type": "select",
            "used_tag": 0,
            "image": null,
            "values": [
                {
                    "id": 17,
                    "name": "شراء"
                },
                {
                    "id": 18,
                    "name": "بيع"
                },
                {
                    "id": 19,
                    "name": "قطع غيار"
                },
                {
                    "id": 20,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 21,
            "name": "نوع الناقل",
            "type": "horizontal_view",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/conveyor_type.png",
            "values": [
                {
                    "id": 22,
                    "name": "شراء"
                },
                {
                    "id": 23,
                    "name": "بيع"
                },
                {
                    "id": 24,
                    "name": "قطع غيار"
                },
                {
                    "id": 25,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 26,
            "name": "نوع سقف السيارة",
            "type": "horizontal_view",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/roof_type.png",
            "values": [
                {
                    "id": 27,
                    "name": "شراء"
                },
                {
                    "id": 28,
                    "name": "بيع"
                },
                {
                    "id": 29,
                    "name": "قطع غيار"
                },
                {
                    "id": 30,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 31,
            "name": "سيليندرز",
            "type": "horizontal_view",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/cylinders.png",
            "values": [
                {
                    "id": 32,
                    "name": "شراء"
                },
                {
                    "id": 33,
                    "name": "بيع"
                },
                {
                    "id": 34,
                    "name": "قطع غيار"
                },
                {
                    "id": 35,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 36,
            "name": "التكيف",
            "type": "horizontal_view",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/conditioning.png",
            "values": [
                {
                    "id": 37,
                    "name": "شراء"
                },
                {
                    "id": 38,
                    "name": "بيع"
                },
                {
                    "id": 39,
                    "name": "قطع غيار"
                },
                {
                    "id": 40,
                    "name": "ونشات"
                }
            ]
        },
        {
            "id": 41,
            "name": "عدد الكيلو مترات",
            "type": "text",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/kilometers.png",
            "values": []
        },
        {
            "id": 42,
            "name": "رقم السيارة",
            "type": "text",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/kilometers.png",
            "values": []
        },
        {
            "id": 43,
            "name": "بيع سريع",
            "type": "switch",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/thunderbolt.png",
            "values": []
        },
        {
            "id": 44,
            "name": "إتجاهات",
            "type": "switch",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/compass.png",
            "values": []
        },
        {
            "id": 45,
            "name": "تأمين",
            "type": "switch",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/bluetooth.png",
            "values": []
        },
        {
            "id": 46,
            "name": "بلوتوث",
            "type": "switch",
            "used_tag": 0,
            "image": "http://127.0.0.1:8000/storage/categories/attribute/insurance.png",
            "values": []
        }
    ]
}
```
