url: `/api/v1/home` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "slider": [
            {
                "id": 1,
                "link_type": "",
                "link_id": null,
                "image": "http://127.0.0.1:8000/storage/banners/banner1.png"
            },
            {
                "id": 2,
                "link_type": "company",
                "link_id": 1,
                "image": "http://127.0.0.1:8000/storage/banners/banner2.png"
            },
            {
                "id": 3,
                "link_type": "advertisement",
                "link_id": 1,
                "image": "http://127.0.0.1:8000/storage/banners/banner3.png"
            }
        ],
        "categories": [
            {
                "id": 1,
                "title": null,
                "name": "إعلانات VIP",
                "image": "http://127.0.0.1:8000/storage/categories/vip.png"
            },
            {
                "id": 2,
                "title": null,
                "name": "سيارات",
                "image": "http://127.0.0.1:8000/storage/categories/cars.png"
            },
            {
                "id": 3,
                "title": null,
                "name": "دراجات هوائية",
                "image": "http://127.0.0.1:8000/storage/categories/bicycles.png"
            },
            {
                "id": 4,
                "title": null,
                "name": "دراجات نارية",
                "image": "http://127.0.0.1:8000/storage/categories/motorcycles.png"
            },
            {
                "id": 5,
                "title": null,
                "name": "قوارب",
                "image": "http://127.0.0.1:8000/storage/categories/boats.png"
            },
            {
                "id": 6,
                "title": null,
                "name": "بانشيات",
                "image": "http://127.0.0.1:8000/storage/categories/panchayat.png"
            },
            {
                "id": 7,
                "title": null,
                "name": "سكوتر",
                "image": "http://127.0.0.1:8000/storage/categories/scooter.png"
            },
            {
                "id": 8,
                "title": null,
                "name": "شركات",
                "image": "http://127.0.0.1:8000/storage/categories/companies.png"
            }
        ]
    }
}
```
