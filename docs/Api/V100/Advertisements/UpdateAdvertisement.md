url: `/api/v1/advertisement/{id}` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| category_id |  required |  |
| sub_category_id |  required |  |
| attributes[index][key_id] |  required array |  |
| attributes[index][used_tag] |  required array |  |
| attributes[index][values][index][value_id] |  required array |  |
| attributes[index][values][index][used_tag] |  required array |  |
| attributes[index][values][index][text] |  nullable array |  |
| images |  required array |  |
| title |  required |  |
| price |  required |  |
| negotiable |  required in:0,1 | 0 not negotiable, 1 negotiable  |
| governorate_id |  required |  |
| city_id |  required |  |
| area_id |  required |  |
| latitude |  nullable |  |
| longitude |  nullable |  |
| description |  nullable |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": null
}
```
