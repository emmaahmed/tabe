url: `/api/v1/advertisement/{id}` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "id": 2,
        "title": "title",
        "description": "description",
        "images": [
            {
                "id": 2,
                "advertisement_id": 2,
                "image": "https://myzoon.my-staff.net/storage/advertisements/tso7BBZGYCg3iagaCgvBrJx9GYmsPs4bzqIKWO8h.jpg",
                "created_at": "2021-07-27T12:04:49.000000Z",
                "updated_at": "2021-07-27T12:04:49.000000Z"
            }
        ],
        "price": "500.00",
        "created_at": "منذ أسبوع",
        "negotiable": 1,
        "tag": {
            "id": 5,
            "name": "أخري",
            "type": "radio",
            "used_tag": 1
        },
        "values": [],
        "is_favorite": false,
        "latitude": "24.0812032",
        "longitude": "32.9089024",
        "governorate": "cairo",
        "city": "cairo",
        "area": "cairo",
        "user": {
            "id": 6,
            "name": "Mohamed",
            "email": "mohamed123@yahoo.com",
            "phone": "01121805384",
            "image": "https://myzoon.my-staff.net/storage/users/C9xZmTiYWoGBX3Qcn35fOXfhYlwEQlq6a7tBkbtj.jpg",
            "token": null,
            "is_follow": false
        },
        "comments_count": 12,
        "comments": [
            {
                "id": 1,
                "content": "new comment",
                "user": {
                    "id": 3,
                    "name": "mohamed",
                    "email": null,
                    "phone": "1234567",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            },
            {
                "id": 2,
                "content": "new ads comment",
                "user": {
                    "id": 4,
                    "name": "Mohamed Atef",
                    "email": "mohamed_atef250@yahoo.comhhh",
                    "phone": "01028991663",
                    "image": "https://myzoon.my-staff.net/storage/users/vPQsoNVoI8FS7cQ0xY2OJDLuuzrVFyMdzMGKL7do.jpg",
                    "token": null,
                    "is_follow": false
                }
            },
            {
                "id": 3,
                "content": "good advertisement",
                "user": {
                    "id": 5,
                    "name": "abdo",
                    "email": "mohamed@yahoo.com",
                    "phone": "01121805383",
                    "image": "https://myzoon.my-staff.net/storage/users/Group 28502.png",
                    "token": null,
                    "is_follow": false
                }
            }
        ],
        "category_name": "عقارات",
        "category_id": 2,
        "sub_category_name": "فلل سكنية",
        "sub_category_id": 10
}
```
