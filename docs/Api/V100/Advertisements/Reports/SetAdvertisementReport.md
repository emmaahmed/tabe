url: `/api/v1/advertisement/report` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| advertisement_id |  required |  |
| reason_id |  required |  |


```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": null
}
```
