url: `/api/v1/report_reasons` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "id": 1,
            "reason": "محتوي غير لائق"
        },
        {
            "id": 2,
            "reason": "اعلان كاذب"
        }
    ]
}
```
