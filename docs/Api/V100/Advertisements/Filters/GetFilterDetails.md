url: `/api/v1/filter/details` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| sub_category_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "governorates": [
            {
                "id": 1,
                "name": "القاهره"
            }
        ],
        "cities": [
            {
                "id": 1,
                "name": "مدينه"
            }
        ],
        "areas": [
            {
                "id": 1,
                "name": "منطقة"
            }
        ],
        "attributes": [
            {
                "id": 1,
                "name": "نوع الفرش",
                "values": [
                    {
                        "id": 2,
                        "name": "مفروش",
                        "type": "radio",
                        "used_tag": 0
                    },
                    {
                        "id": 3,
                        "name": "غير مفروش",
                        "type": "radio",
                        "used_tag": 0
                    }
                ]
            },
            {
                "id": 4,
                "name": "نوع الاعلان",
                "values": [
                    {
                        "id": 5,
                        "name": "بيع",
                        "type": "radio",
                        "used_tag": 1
                    },
                    {
                        "id": 6,
                        "name": "ايجار",
                        "type": "radio",
                        "used_tag": 1
                    },
                    {
                        "id": 7,
                        "name": "مطلوب",
                        "type": "radio",
                        "used_tag": 1
                    }
                ]
            }
        ]
    }
}
```
