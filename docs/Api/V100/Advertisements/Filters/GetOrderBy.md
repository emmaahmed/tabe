url: `/api/v1/order_by` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| order_by |  required in:newest,oldest,lowest,highest |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "data": [
            {
                "specifications": [
                    {
                        "id": 4,
                        "name": "نوع الاعلان",
                        "values": [
                            {
                                "id": 5,
                                "name": "بيع",
                                "type": "radio",
                                "used_tag": 1
                            },
                            {
                                "id": 6,
                                "name": "ايجار",
                                "type": "radio",
                                "used_tag": 1
                            },
                            {
                                "id": 7,
                                "name": "مطلوب",
                                "type": "radio",
                                "used_tag": 1
                            }
                        ]
                    }
                ],
                "id": 1,
                "title": "title",
                "price": 500,
                "special": 0,
                "is_favorite": false,
                "image": "http://127.0.0.1:8000/storage/advertisements/HvESg3KmHLcrnhok5axglY8Ctd4Ymk6D6vD89PLm.jpg",
                "tag": {
                    "id": 5,
                    "name": "بيع",
                    "type": "radio",
                    "used_tag": 1
                },
                "created_at": "5 hours ago"
            }
        ],
        "links": {
            "first": "http://127.0.0.1:8000/api/v1/order_by?page=1",
            "last": "http://127.0.0.1:8000/api/v1/order_by?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "http://127.0.0.1:8000/api/v1/order_by",
            "per_page": 15,
            "to": 1,
            "total": 1
        }
    }
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
