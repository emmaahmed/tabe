url: `/api/v1/search` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| search |  required |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "id": 1,
            "title": "title",
            "sub_category_id": 2
        }
    ]
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
