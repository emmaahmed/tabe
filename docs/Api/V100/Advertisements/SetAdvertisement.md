url: `/api/v1/advertisement` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| category_id |  required |  |
| sub_category_id |  required |  |
| sub_category_child_ids[index] |  required array |  |
| attributes[index][value_id] |  required array |  |
| attributes[index][used_tag] |  required array |  |
| attributes[index][text] |  required array |  |
| images[index] |  required array |  |
| title |  required |  |
| price |  required |  |
| negotiable |  required in:0,1 | 0 not negotiable, 1 negotiable  |
| is_new |  required in:0,1 | 0 old, 1 new  |
| contact_on |  required in:1,2,3 | 1 My registered mobile number, 2 Messages via the app, 3 Whatsapp application  |
| premium |  required in:0,1 | 0 normal, 1 vip  |
| governorate_id |  required |  |
| city_id |  required |  |
| area_id |  required |  |
| latitude |  nullable |  |
| longitude |  nullable |  |
| description |  nullable |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": null
}
```
