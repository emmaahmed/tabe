url: `/api/v1/advertisement/{id}/comments` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "comments_count": 3,
        "comments": {
            "data": [
                {
                    "id": 1,
                    "content": "comment",
                    "user": {
                        "id": 2,
                        "name": null,
                        "email": null,
                        "phone": "123456",
                        "image": null,
                        "token": null
                    }
                },
                {
                    "id": 2,
                    "content": "comment",
                    "user": {
                        "id": 2,
                        "name": null,
                        "email": null,
                        "phone": "123456",
                        "image": null,
                        "token": null
                    }
                },
                {
                    "id": 3,
                    "content": "comment",
                    "user": {
                        "id": 2,
                        "name": null,
                        "email": null,
                        "phone": "123456",
                        "image": null,
                        "token": null
                    }
                }
            ],
            "links": {
                "first": "http://127.0.0.1:8000/api/v1/advertisement/1/comments?page=1",
                "last": "http://127.0.0.1:8000/api/v1/advertisement/1/comments?page=1",
                "prev": null,
                "next": null
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 1,
                "path": "http://127.0.0.1:8000/api/v1/advertisement/1/comments",
                "per_page": 15,
                "to": 3,
                "total": 3
            }
        }
    }
}
```
