url: `/api/v1/advertisement/comment` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| advertisement_id |  required |  |
| content |  required |  |


```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": {
        "id": 4,
        "content": "comment",
        "user": {
            "id": 2,
            "name": null,
            "email": null,
            "phone": "123456",
            "image": null,
            "token": null
        }
    }
}
```
