url: `/api/v1/favorite` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| advertisement_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "is_favorite" : true
      }
}
```

Errors
```json
{
  "message": "Unauthenticated."
}
```
