url: `/api/v1/countries` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |

```json
{
    "code": 200,
    "message": "تم تنفيذ الإجراء بنجاح!",
    "data": [
        {
            "id": 1,
            "name": "البحرين",
            "currency": "دينار",
            "image": "http://127.0.0.1:8000/storage/countries/flag-united-arab-emirates.png"
        },
        {
            "id": 2,
            "name": "الكويت",
            "currency": "دينار",
            "image": "http://127.0.0.1:8000/storage/countries/flag-saudi-arabia.png"
        },
        {
            "id": 3,
            "name": "قطر",
            "currency": "ريال",
            "image": "http://127.0.0.1:8000/storage/countries/flag-oman.png"
        },
        {
            "id": 4,
            "name": "عمان",
            "currency": "ريال",
            "image": "http://127.0.0.1:8000/storage/countries/flag-qatar.png"
        },
        {
            "id": 5,
            "name": "السعودية",
            "currency": "ريال",
            "image": "http://127.0.0.1:8000/storage/countries/flag-kuwait.png"
        },
        {
            "id": 6,
            "name": "الامارات",
            "currency": "دينار",
            "image": "http://127.0.0.1:8000/storage/countries/flag-bahrain.png"
        }
    ]
}
```
