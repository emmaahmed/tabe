url: `/api/v1/story` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| user_id |  required |  |
| files |  required array |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": null
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```

```json
{
  "code": 401,
  "message": "يجب أن يكون لديك إعلان لكي تستطيع وضع حالة.",
}
```
