url: `/api/v1/story` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| user_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": [
        {
            "id": 1,
            "file_type": "image",
            "file": "image"
        }
    ]
}
```
