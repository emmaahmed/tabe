url: `/api/v1/story/seen` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| story_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": null
}
```

Errors
```json
{
  "code": 403,
  "message": "Unauthenticated."
}
```
