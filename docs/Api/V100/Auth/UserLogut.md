url: `/api/v1/logout` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |
| language |  ar | ar,en |


```json
{
    "code": 200,
    "message": "Logged out successfully.",
    "data": null
}
```

Errors
```json
{
  "message": "Unauthenticated."
}
```
