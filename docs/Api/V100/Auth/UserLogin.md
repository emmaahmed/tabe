url: `/api/v1/login` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| phone_email |  required |  |
| password |  required |  |
| device_token |  required |  |

```json
{
    "code": 200,
    "message": "تم تسجيل الدخول بنجاح.",
    "data": {
        "id": 2,
        "name": "Abdo labib",
        "email": "abdo@abdo.com",
        "phone": "123456",
        "image": null,
        "token": "8|a9NwSjBziL7WAW0yQhLuMYH4DP5RtK711oDPxOpM",
        "is_follow": false
    }
}
```

Errors
```json
{
  "code": 401,
  "message": "The selected phone is invalid."
}
```
