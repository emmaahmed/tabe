url: `/api/v1/code_send` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| phone |  required |  |
| type |  required | in: verify, reset |

```json
{
    "code": 200,
    "message": "Code sent successfully.",
    "data": null
}
```

Errors
```json
{
  "code": 401,
  "message": "The selected type is invalid.The selected phone is invalid."
}
```
