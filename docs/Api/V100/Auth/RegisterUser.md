url: `/api/v1/register` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| name |  required |  |
| phone |  required |  |
| email |  required |  |
| password |  required |  |
| password_confirmation |  required |  |
| device_token |  required |  |

```json
{
    "code": 200,
    "message": "Registered Successfully,please activate.",
    "data": null
}
```

Errors
```json
{
  "code": 401,
  "message": "The email has already been taken.The phone has already been taken."
}
```
