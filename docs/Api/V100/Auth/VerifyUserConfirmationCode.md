url: `/api/v1/code_check` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| phone |  required |  |
| type |  required | in: verify, reset |
| code |  required |  |

```json
{
    "code": 200,
    "message": "Verified successfully.",
    "data": {
            "token": "3|qLNKUjq45VJG3ED8rvkm5CiUWM38NuDF6hDKBuN4"
        }
}
```

Errors
```json
{
  "message": "Unauthenticated."
}
```
