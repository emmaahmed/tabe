url: `/api/v1/categories` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| city_id |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": [
        {
            "id": 1,
            "name": "cairo"
        }
    ]
}
```
