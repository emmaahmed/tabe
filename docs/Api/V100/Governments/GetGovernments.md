url: `/api/v1/governorates` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": [
        {
            "id": 1,
            "name": "cairo"
        }
    ]
}
```
