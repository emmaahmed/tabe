url: `/api/v1/notifications` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  ar | ar,en |
| Accept |  application/json |  |
| Authorization |  Bearer token |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "data": [
            {
                "id": 1,
                "text": "text",
                "type": "user",
                "user_id": 2,
                "follower_id": 3,
                "advertisement_id": null,
                "comment_id": null,
                "message_id": null,
                "created_at": "14 July 2021 16:49"
            }
        ],
        "links": {
            "first": "http://127.0.0.1:8000/api/v1/notifications?page=1",
            "last": "http://127.0.0.1:8000/api/v1/notifications?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "path": "http://127.0.0.1:8000/api/v1/notifications",
            "per_page": 15,
            "to": 1,
            "total": 1
        }
    }
}
```
