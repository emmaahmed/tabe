url: `/api/v1/terms_condition` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  en | ar,en |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "id": 5,
        "value": "<p>Terms and Condition :<br />\r\n&nbsp;</p>\r\n\r\n<p>Your order collects information about you when you use our mobile applications. This Privacy Policy states the basis on which your order will be relied upon to process information, including personal data that we collect from you or that you provide to us. Every time you access or use the Services or provide us with information, by doing so you accept and agree to the practices described in this Privacy Policy.</p>\r\n\r\n<p><br />\r\nThe information you provide will not</p>\r\n\r\n<p>We collect information that you provide to us directly through your access or use of services, for example, when you create or modify your account, request customized services, contact customer support or contact us in other ways. This information may include: your name, email, phone number, postal address, profile picture, payment method, financial data and credit card information, and any other information you choose to provide to us.</p>\r\n\r\n<p>Your Captain must be 18 years old.</p>",
        "image": "http://google.com/"
    }
}
```
