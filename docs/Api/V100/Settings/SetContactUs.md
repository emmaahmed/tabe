url: `/api/v1/contact_us` <br>
method : `GET` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  en | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| name |  required |  |
| email |  required |  |
| phone |  nullable |  |
| message |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": null
}
```
