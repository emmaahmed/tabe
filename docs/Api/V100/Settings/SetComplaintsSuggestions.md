url: `/api/v1/complaints_suggestions` <br>
method : `POST` <br>

Header

| Key | Value | Description |
| ---- | ---- | ---- |
| language |  en | ar,en |

params

| Parameter | Validation Rules | Description |
| ---- | ---- | ---- |
| message |  required |  |

```json
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": null
}
```
