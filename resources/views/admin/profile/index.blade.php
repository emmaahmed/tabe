@extends('layouts.admin', ['title' => __('Profile')])
@section('css')
    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Profile')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Profile')}}</li>
    </ol>
@endsection
@section('content')
    <div class="edit-profile">
        <div class="row">
            <div class="col-lg-12">
                <form class="card" action="{{route('admin.admins.update', auth()->id())}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">{{__('Name')}}</label>
                                    <input class="form-control" type="text" name="edit_name" value="{{auth()->user()->name}}">
                                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label">{{__('Email')}}</label>
                                    <input class="form-control" type="email" name="edit_email" value="{{auth()->user()->email}}">
                                    @include('admin.includes.alerts.error', ['input' => 'edit_email'])
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">{{__('Phone')}}</label>
                                    <input class="form-control" type="text" name="edit_phone" value="{{auth()->user()->phone}}">
                                    @include('admin.includes.alerts.error', ['input' => 'edit_phone'])
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">{{__('Password')}}</label>
                                    <input class="form-control" type="password" name="edit_password">
                                    @include('admin.includes.alerts.error', ['input' => 'edit_password'])
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-label">{{__('Password confirmation')}}</label>
                                    <input class="form-control" type="password" name="edit_password_confirmation">
                                    @include('admin.includes.alerts.error', ['input' => 'edit_password_confirmation'])
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="text-muted font-14" for="image">{{__('Image')}}</label>
                                <input type="file" class="dropify" name="edit_image" data-max-file-size="40M" data-default-file="{{auth()->user()->image}}"/>
                                @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-primary" type="submit">{{__('Update profile')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.includes.alerts.message')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
@endsection
