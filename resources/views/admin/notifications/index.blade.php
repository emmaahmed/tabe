@extends('layouts.admin', ['title' => __('Notifications')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Notifications')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Notifications')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{(__('Title'))}}</th>
                            <th>{{(__('Content'))}}</th>
                            <th>{{(__('Type'))}}</th>
                            <th>{{(__('Users'))}}</th>
                            <th>{{(__('Created at'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create notification -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.notifications.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <textarea name="title" class="form-control" rows="6" cols="80" placeholder="{{__('Title')}}"  data-emojiable="true" data-emoji-input="unicode"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'text'])
                </div>
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Content')}} <span style="color:red">*</span></label>
                    <textarea name="text" class="form-control" rows="6" cols="80" placeholder="{{__('Content')}}"  data-emojiable="true" data-emoji-input="unicode"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'text'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Choose Type')}}</label>
                    <select class="form-control" id="notify_type" name="notify_type">
                        <option label="{{__('Choose Type')}}"></option>

                        <option value="per_user">{{__('Per All Users')}}</option>
                        <option value="per_companies">{{__('Per Companies')}}</option>
                        <option value="per_country">{{__('Per Country')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'type'])
                </div>

{{--                <div class="col-sm-12 mt-2" id="notify_user" style="display: none">--}}
{{--                    <label class="text-muted font-14">{{__('Choose User')}}</label>--}}
{{--                        <select class="form-control" name="notify_user">--}}
{{--                            @foreach($users as $user)--}}
{{--                                <option value="{{$user->id}}">{{$user->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    @include('admin.includes.alerts.error', ['input' => 'notify_user'])--}}
{{--                </div>--}}

{{--                <div class="col-sm-12 mt-2" id="notify_company" style="display: none">--}}
{{--                    <label class="text-muted font-14">{{__('Choose Company')}}</label>--}}
{{--                        <select class="form-control" name="notify_company">--}}
{{--                                @foreach($companies as $company)--}}
{{--                                    <option value="{{$company->id}}">{{$company->name}}</option>--}}
{{--                                @endforeach--}}

{{--                        </select>--}}
{{--                    @include('admin.includes.alerts.error', ['input' => 'notify_company'])--}}
{{--                </div>--}}
                <div class="col-sm-12 mt-2" id="notify_country" style="display: none">
                    <label class="text-muted font-14">{{__('Choose Country')}}</label>
                        <select class="form-control" name="notify_country">
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach

                        </select>
                    @include('admin.includes.alerts.error', ['input' => 'notify_country'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End create notification -->

    <!-- Start delete notification -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete notification -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/notifications",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: 'title' },
                    { data: 'text' },
                    { data: 'is_all' },

                    { data: 'name' },
                    { data: 'created_at' },
                    { data: "operations", render: function (data, type, row) {
                            return '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                                '<i class="fa fa-trash"></i>' +
                                '</button>'
                        }
                    },
                ],
            });
        });

        $('#notify_type').on('change', function () {
            if ($(this).val() === 'per_country') {
                $('#notify_country').show('fade');
            }
            else{
                console.log('bage hena');
                $('#notify_country').hide('fade');

            }
        })

        function openModalDelete(user_id) {
            $('.action_form').attr('action', '{{route('admin.notifications.destroy', '')}}' + '/' + user_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.notifications.store')}}');
        })
    </script>
    <script>
        $(function() {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '/admin_main/images/img',
                popupButtonClasses: 'fa fa-smile-o'
                // iconSize: 30
            });
            // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
            // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
            // It can be called as many times as necessary; previously converted input fields will not be converted again
            window.emojiPicker.discover();
        });
    </script>

@endsection
