@extends('layouts.admin', ['title' => __('Users')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Users')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Users')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table-bordered cell-border', 'id' => 'users-table']) !!}
            </div>
        </div>
    </div>

    <!-- Start create admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.users.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Email')}} </label>
                    <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="{{__('Email')}}">
                    @include('admin.includes.alerts.error', ['input' => 'email'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Phone')}} <span style="color:red">*</span></label>
                    <input type="text" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('Phone')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'phone'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password')}} <span style="color:red">*</span></label>
                    <input type="password" name="password" class="form-control" placeholder="{{__('Password')}}">
                    @include('admin.includes.alerts.error', ['input' => 'password'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Countries')}}</label>
                    <select class="form-control" name="country_id">
                        <option selected disabled>{{__('Select Action')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'country_id'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}}</label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End create admin -->

    <!-- Start edit admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_name" name="edit_name" class="form-control" placeholder="{{__('Name')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Email')}}</label>
                    <input type="email" id="e_email" name="edit_email" class="form-control" placeholder="{{__('Email')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_email'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Phone')}}</label>
                    <input type="text" id="e_phone" name="edit_phone" class="form-control" placeholder="{{__('Phone')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_phone'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password')}}</label>
                    <input type="password" name="edit_password" class="form-control" placeholder="{{__('Password')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_password'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Countries')}}</label>
                    <select class="form-control" id="e_country_id" name="edit_country_id">
                        <option selected disabled>{{__('Select Action')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_country_id'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}}</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit admin -->

    <!-- Start send notification -->
    @component('admin.includes.modal')
        @slot('modalID')
            notifyModal
        @endslot
        @slot('modalTitle')
            {{__('Send Notification')}}
        @endslot
        @slot('modalButton')
            {{__('Send')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Content')}} <span style="color:red">*</span></label>
                    <textarea name="text" class="form-control" placeholder="{{__('Content')}}"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'text'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End send notification -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/pages/form-fileupload.init.js"></script>

    {!! $dataTable->scripts() !!}
    <script>
        function openModalEdit(user) {
            $('#e_name').val(user.name);
            $('#e_email').val(user.email);
            $('#e_phone').val(user.phone);
            $('#e_country_id').val(user.country_id);

            let imagenUrl = user.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.users.update', '')}}' + '/' + user.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.users.store')}}');
        })

        function openModalNotification(user_id) {
            $('.action_form').attr('action', '{{route('admin.users.sendNotification', '')}}' + '/' + user_id);
            $('#notifyModal').modal('show');
        }

        $('#notifyModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.users.store')}}');
        })
    </script>
@endsection
