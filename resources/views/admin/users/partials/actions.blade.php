@can('update', \App\Models\User::class)
<button type="button" title="{{__('Edit')}}" class="btn btn-warning btn-sm" onclick="openModalEdit({{$data}})">
    <i class="fa fa-edit"></i>
</button>
@endcan
<button title="{{__('Send notification')}}" class='btn btn-success btn-sm btn-condensed' onclick='openModalNotification({{$data->id}})' >
    <i class="fa fa-paper-plane"></i>
</button>
@can('delete', \App\Models\User::class)
<button id="delete" title="{{__('Delete')}}" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->id }}').submit()
    }
    ">
    <i class="fa fa-trash"></i>
    <form id="destroy{{ $data->id }}" class="d-none" action="{{ route('admin.users.destroy', $data->id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
@endcan
