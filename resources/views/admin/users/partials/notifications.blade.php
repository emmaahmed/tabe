@if( $data->company_notification_flag===1)
    <label class="switch">
        <input type="checkbox" id="switchValue" checked=""
               onchange = "window.location.href = '{{route('admin.users.changeNotification', $data->id)}}' " >
        <span class="switch-state"></span>
    </label>
@else
    <label class="switch">
        <input type="checkbox" id="switchValue"
               onchange = "window.location.href = '{{route('admin.users.changeNotification', $data->id)}}' " >
        <span class="switch-state"></span>
    </label>
@endif
