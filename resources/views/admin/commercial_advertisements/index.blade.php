@extends('layouts.admin', ['title' => __('Commercial Advertisements')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('breadcrumb')
    <h3>{{__('Commercial Advertisements')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Commercial Advertisements')}}</li>
    </ol>
@endsection
{{--@section('button')--}}
{{--    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">--}}
{{--        <i class="fa fa-plus"></i>--}}
{{--        {{__('Add')}}--}}
{{--    </button>--}}
{{--@endsection--}}
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table-bordered cell-border', 'id' => 'commercial-advertisements-table']) !!}
            </div>
        </div>
    </div>

    <!-- Start create admin -->
    {{--    @component('admin.includes.modal')--}}
    {{--        @slot('modalID')--}}
    {{--            createModal--}}
    {{--        @endslot--}}
    {{--        @slot('modalTitle')--}}
    {{--            {{__('Add')}}--}}
    {{--        @endslot--}}
    {{--        @slot('modalRoute')--}}
    {{--            {{route('admin.commercial_advertisements.store')}}--}}
    {{--        @endslot--}}
    {{--        @slot('modalContent')--}}
    {{--            <div class="row">--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>--}}
    {{--                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}"--}}
    {{--                           title="{{__('Please fill out this field')}}" required>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'name'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14">{{__('Email')}} </label>--}}
    {{--                    <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="{{__('Email')}}">--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'email'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14">{{__('Phone')}} <span style="color:red">*</span></label>--}}
    {{--                    <input type="text" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('Phone')}}"--}}
    {{--                           title="{{__('Please fill out this field')}}" required>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'phone'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14">{{__('Password')}} <span style="color:red">*</span></label>--}}
    {{--                    <input type="password" name="password" class="form-control" placeholder="{{__('Password')}}">--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'password'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14">{{__('Countries')}}</label>--}}
    {{--                    <select class="form-control" name="country_id">--}}
    {{--                        <option selected disabled>{{__('Select Action')}}</option>--}}
    {{--                        @foreach($countries as $country)--}}
    {{--                            <option value="{{$country->id}}">{{$country->name}}</option>--}}
    {{--                        @endforeach--}}
    {{--                    </select>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'country_id'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14" for="image">{{__('Description')}}</label>--}}
    {{--                    <textarea name="description" rows="5" class="form-control"></textarea>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'description'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-4">--}}
    {{--                    <label class="text-muted font-14">{{__('Images')}} <span style="color:red">*</span></label>--}}
    {{--                    <input type="file" name="images[]" multiple/>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'images'])--}}
    {{--                </div>--}}

    {{--                <div class="col-sm-12 mt-2">--}}
    {{--                    <label class="text-muted font-14" for="image">{{__('Company image')}}</label>--}}
    {{--                    <input type="file" name="image" class="dropify" data-max-file-size="40M" required/>--}}
    {{--                    @include('admin.includes.alerts.error', ['input' => 'image'])--}}
    {{--                </div>--}}

    {{--            </div><!-- row -->--}}
    {{--        @endslot--}}
    {{--    @endcomponent--}}
    <!-- End create admin -->

    <!-- Start edit company -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Description')}}</label>
                    <textarea disabled rows="5" id="description" class="form-control"></textarea>
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Notes')}}</label>
                    <textarea disabled rows="5" id="notes" class="form-control"></textarea>
                </div>
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="period">{{__('Period')}}</label>
                    <input disabled type="text"  id="period" class="form-control">
                </div>


                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_image">{{__('Image')}} ({{__('Width')}}  173 px  * {{__('Height')}} 228 px)</label>
                    <input type="file" id="e_image" name="image"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit company -->
    <!-- Start edit company -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Description')}}</label>
                    <textarea disabled rows="5" id="description" class="form-control"></textarea>
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Notes')}}</label>
                    <textarea disabled rows="5" id="notes" class="form-control"></textarea>
                </div>
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="period">{{__('Period')}}</label>
                    <input disabled type="text"  id="period" class="form-control">
                </div>


                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_image">{{__('Image')}}  ({{__('Width')}}  228 px  * {{__('Height')}} 173 px)</label>
                    <input type="file" id="e_image" name="image"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit company -->

    <!-- Start delete company -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete company -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    {!! $dataTable->scripts() !!}
    <script>
        function openModalEdit(company) {


            let imagenUrl = company.image;
            $('#description').text(company.description);
            $('#notes').text(company.notes);
            $('#period').val(company.period +" "+ "{{__('Month')}}");

            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.commercial_advertisements.update', '')}}' + '/' + company.id);
            $('#editModal').modal('show');
        }

        {{--$('#editModal').on('hide.bs.modal', function() {--}}
        {{--    $('.action_form').attr('action', '{{route('admin.companies.store')}}');--}}
        {{--})--}}

        function openModalDelete(company_id) {
            $('.action_form').attr('action', '{{route('admin.commercial_advertisements.destroy', '')}}' + '/' + company_id);
            $('#deleteModal').modal('show');
        }

        {{--$('#deleteModal').on('hide.bs.modal', function() {--}}
        {{--    $('.action_form').attr('action', '{{route('admin.companies.store')}}');--}}
        {{--})--}}

    </script>

@endsection
