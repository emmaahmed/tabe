@if($data->type=='commercial_banners')
<button type="button" title="{{__('Edit')}}" class="btn btn-warning btn-sm" onclick="openModalEdit({{$data}})">
    <i class="fa fa-plus-square"></i>
</button>
@endif

<button id="delete" title="{{__('Delete')}}" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->id }}').submit()
    }
    ">
    <i class="fa fa-trash"></i>
    <form id="destroy{{ $data->id }}" class="d-none" action="{{ route('admin.commercial_advertisements.destroy', $data->id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
