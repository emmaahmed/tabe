<!-- Page Sidebar Start-->
<div class="page-sidebar light-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper">
            <a href="{{route('admin.dashboard.index')}}" style="margin: auto">
                <img src="/admin_main/images/logo.png" style="height: 90px">
            </a>
        </div>
    </div>
    <div class="sidebar custom-scrollbar">
        <ul class="sidebar-menu">
            @can('viewAny', "Dashboard")
                <li>
                    <a class="sidebar-header" href="{{route('admin.dashboard.index')}}">
                        <i data-feather="home"></i>
                        <span> {{__('Dashboard')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\User::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.users.index')}}">
                        <i data-feather="users"></i>
                        <span> {{__('Users')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', 'Admin')
                <li>
                    <a class="sidebar-header" href="{{route('admin.admins.index')}}">
                        <i data-feather="lock"></i>
                        <span> {{__('Admins')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Category::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.categories.index')}}">
                        <i data-feather="align-justify"></i>
                        <span> {{__('Categories')}} </span>
                    </a>
                </li>
            @endcan
            @can('viewAny', \App\Models\Package::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.packages.index')}}">
                        <i data-feather="align-justify"></i>
                        <span> {{__('Packages')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Advertisement::class)
                <li><a class="sidebar-header" href="#" onclick="event.preventDefault()"><i data-feather="tv"></i>
                        <span>{{__('Advertisements')}}</span><i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="{{route('admin.advertisements.index')}}">
                                <i class="fa fa-circle"></i><span>{{__('Advertisements')}}</span>
                            </a>
                        </li>
                        <li><a href="{{route('admin.comments.index')}}">
                                <i class="fa fa-circle"></i><span>{{__('Comments')}}</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            @can('viewAny', 'Company')
                <li>
                    <a class="sidebar-header" href="{{route('admin.companies.index')}}">
                        <i data-feather="align-justify"></i>
                        <span> {{__('Companies')}} </span>
                    </a>
                </li>
            @endcan
            @can('viewAny', \App\Models\CommercialAdvertisement::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.commercial_advertisements.index')}}">
                        <i data-feather="align-justify"></i>
                        <span> {{__('Commercial Advertisements')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Banner::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.banners.index')}}">
                        <i data-feather="image"></i>
                        <span> {{__('Banners')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Country::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.countries.index')}}">
                        <i data-feather="globe"></i>
                        <span> {{__('Countries')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Notification::class)
                    <li>
                        <a class="sidebar-header" href="{{route('admin.notifications.index')}}">
                            <i data-feather="bell"></i>
                            <span> {{__('Notifications')}} </span>
                        </a>
                    </li>
            @endcan

            @can('viewAny', \App\Models\Welcome::class)
                <li>
                    <a class="sidebar-header" href="{{route('admin.welcomes.index')}}">
                        <i data-feather="layout"></i>
                        <span> {{__('Welcome pages')}} </span>
                    </a>
                </li>
            @endcan

            @can('viewAny', \App\Models\Setting::class)
                <li><a class="sidebar-header" href="#" onclick="event.preventDefault()"><i data-feather="settings"></i><span>{{__('Settings')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        <li><a href="{{route('admin.settings.show', 'premium_cost')}}"><i class="fa fa-circle"></i><span>{{__('Ads Premium cost')}}</span></a></li>
                        <li><a href="{{route('admin.settings.show', 'advertisement_modify_time')}}"><i class="fa fa-circle"></i><span>{{__('Ads modify time')}}</span></a></li>
                        <li><a href="{{route('admin.settings.show', 'terms_condition')}}"><i class="fa fa-circle"></i><span>{{__('Terms & condition')}}</span></a></li>
                        <li><a href="{{route('admin.settings.show', 'privacy_policy')}}"><i class="fa fa-circle"></i><span>{{__('Privacy Policy')}}</span></a></li>
                        <li><a href="{{route('admin.settings.show', 'about_us')}}"><i class="fa fa-circle"></i><span>{{__('About us')}}</span></a></li>
                        <li><a href="{{route('admin.settings.show', 'help')}}"><i class="fa fa-circle"></i><span>{{__('Help')}}</span></a></li>
                        <li><a href="{{route('admin.report_reasons.index')}}"><i class="fa fa-circle"></i><span>{{__('Report reasons')}}</span></a></li>
                        <li><a href="{{route('admin.contact_us.index')}}"><i class="fa fa-circle"></i><span>{{__('Contact us')}}</span></a></li>
                        <li style="padding-bottom: 30px"><a href="{{route('admin.complain_suggestions.index')}}"><i class="fa fa-circle"></i><span>{{__('Complain suggestion')}}</span></a></li>
                    </ul>
                </li>
            @endcan

{{--            @can('viewAny', 'Report')--}}
{{--                <li><a class="sidebar-header" href="#" onclick="event.preventDefault()"><i data-feather="bar-chart"></i><span>{{__('Reports')}}</span><i class="fa fa-angle-right pull-right"></i></a>--}}
{{--                    <ul class="sidebar-submenu">--}}
{{--                        <li><a href="{{route('admin.reports.orders')}}"><i class="fa fa-circle"></i><span> {{__('admin.ordersSideBar')}}</a></li>--}}
{{--                        <li><a href="{{route('admin.reports.orders.finished')}}"><i class="fa fa-circle"></i><span> {{__('admin.deliveredOrdersSideBar')}}</span></a></li>--}}
{{--                        <li><a href="{{route('admin.reports.products')}}"><i class="fa fa-circle"></i><span> {{__('admin.productsHistorySideBar')}}</span></a></li>--}}
{{--                        <li><a href="{{route('admin.reports.products.receipt')}}"><i class="fa fa-circle"></i><span> {{__('admin.productsReceiptSideBar')}}</span></a></li>--}}
{{--                        <li><a href="{{route('admin.reports.delegates')}}"><i class="fa fa-circle"></i><span> {{__('admin.delegatesSideBar')}}</span></a></li>--}}
{{--                        <li><a href="{{route('admin.reports.customers')}}"><i class="fa fa-circle"></i><span> {{__('admin.customersSideBar')}}</span></a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endcan--}}

        </ul>

        <div style="text-align:center;">
            <a href="https://2grand.net/" target="_blank">
                <img src="/admin_main/images/Logo-Grand.png" style="width: 75px">
            </a>
        </div>
    </div>
</div>
<!-- Page Sidebar Ends-->
