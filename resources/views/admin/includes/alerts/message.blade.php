<script>
    $(function() {

        if({{Session::has('success') ?: 0}}){
            $.notify({
                    message:'{{Session::get('success')}}'
                },
                {
                    type:'success',
                    allow_dismiss:true,
                    timer:1000,
                    delay:1000 ,
                });
        }

        if({{Session::has('error') ?: 0}}){
            $.notify({
                    message:'{{Session::get('error')}}'
                },
                {
                    type:'danger',
                    allow_dismiss:true,
                    timer:1000,
                    delay:1000 ,
                });
        }

        if({{$errors->any() ?: 0}}){
            $.notify({
                    message:'{{__('Whoops! Something went wrong.')}}'
                },
                {
                    type:'danger',
                    allow_dismiss:true,
                    timer:1000,
                    delay:1000 ,
                });
        }
    });
</script>
