<!-- Page Header Start-->
<div class="page-main-header" style="background-color: #4B2B6E">
    <div class="main-header-right row">
        <div class="main-header-left d-lg-none">
            <div class="logo-wrapper">
                <a href="{{route('admin.dashboard.index')}}" style="height: 95px">
                    <img src="/admin_main/images/logo.png" alt="">
                </a>
            </div>
        </div>
        <div class="mobile-sidebar d-block">
            <div class="media-body text-right switch-sm">
                <label class="switch"><a href="#"><i id="sidebar-toggle" data-feather="align-left"></i></a></label>
            </div>
        </div>
        <div class="nav-right col p-0">
            <ul class="nav-menus">
                <li>
{{--                    <form class="form-inline search-form" action="#" method="get">--}}
{{--                        <div class="form-group">--}}
{{--                            <div class="Typeahead Typeahead--twitterUsers">--}}
{{--                                <div class="u-posRelative">--}}
{{--                                    <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Search...">--}}
{{--                                    <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>--}}
{{--                                </div>--}}
{{--                                <div class="Typeahead-menu"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
                </li>
{{--                <li class="onhover-dropdown"><a class="txt-dark" href="index.html#">--}}
{{--                        <h6 style="color: #fff">EN</h6></a>--}}
{{--                    <ul class="language-dropdown onhover-show-div p-20">--}}
{{--                        <li><a href="index.html#" data-lng="en"><i class="flag-icon flag-icon-is"></i> English</a></li>--}}
{{--                        <li><a href="index.html#" data-lng="es"><i class="flag-icon flag-icon-um"></i> Spanish</a></li>--}}
{{--                        <li><a href="index.html#" data-lng="pt"><i class="flag-icon flag-icon-uy"></i> Portuguese</a></li>--}}
{{--                        <li><a href="index.html#" data-lng="fr"><i class="flag-icon flag-icon-nz"></i> French</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <li class="onhover-dropdown">
                    <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle"
                                                               src="{{auth()->user()->image ?: '/admin_main/images/user.jpg'}}" alt="header-user">
                    </div>
                    <ul class="profile-dropdown onhover-show-div p-20" style="top:37px">
                        <li><a href="{{route('admin.profile.index')}}"><i data-feather="user"></i>{{__('Edit')}}</a></li>
{{--                        <li><a href="index.html#"><i data-feather="mail"></i>                                    Inbox</a></li>--}}
{{--                        <li><a href="index.html#"><i data-feather="lock"></i>                                    Lock Screen</a></li>--}}
{{--                        <li><a href="index.html#"><i data-feather="settings"></i>                                    Settings</a></li>--}}
                        <li><a href="{{route('admin.logout')}}"><i data-feather="log-out"></i> {{__('Logout')}}</a></li>
                    </ul>
                </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">
                <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                <div class="ProfileCard-details">
                    <div class="ProfileCard-realName"></div>
                </div>
            </div>
        </script>
        <script id="empty-template" type="text/x-handlebars-template">
            <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>

        </script>
    </div>
</div>
<!-- Page Header Ends -->
