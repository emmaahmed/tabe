<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/admin_main/images/logo.png" type="image/x-icon">
    <link rel="shortcut icon" href="/admin_main/images/logo.png" type="image/x-icon">
    <title>Tabe</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=tajwal:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/style.css">
    <link rel="stylesheet" href="/admin_main/css/royal-preload.css" />
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/responsive.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" >
</head>
<body main-theme-layout="rtl">
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <div class="auth-bg">
        <div class="authentication-box" style="width: 560px;">
            <div class="text-center"><img src="/admin_main/images/logo.png" style="height: 90px"></div>
            <div class="card mt-4">
                <div class="card-body" style="padding: 60px">
                    <div class="text-center">
                        <h4>{{__('Log in')}} <span style="color: #4b2b6d; font-weight: 900">Tabe</span></h4>
                        <p style="color: #656565;font-size: 17px;"> {{__('Login to your account to manage app easily')}}</p>
                    </div>
                    @if(Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    <form class="theme-form" action="{{route('admin.authenticate')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label class="col-form-label pt-0" style="font-weight: bold; color: #313131;">
                                <i class="fas fa-user"></i> {{__('Name')}}
                            </label>
                            <input class="form-control input-face" type="text" name="name" placeholder="{{__('Enter your name')}}" required>
                            @include('admin.includes.alerts.error', ['input' => 'name'])
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0" style="font-weight: bold; color: #313131;">
                                <i class="fas fa-lock"></i> {{__('Password')}}
                            </label>
                            <input class="form-control input-face" type="password" name="password" placeholder="{{__('Enter your password')}}" required>
                        </div>
                        <div class="form-group form-row mt-3 mb-0">
                            <button class="btn btn-primary btn-block" type="submit">{{__('Login')}}</button>
                        </div>
                    </form>
                    <div style="text-align:center;">
                        <a href="https://2grand.net/" target="_blank">
                            <img src="/admin_main/images/Logo-Grand.png" style="width: 75px">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- latest jquery-->
<script src="/admin_main/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap js-->
<script src="/admin_main/js/bootstrap/popper.min.js"></script>
<script src="/admin_main/js/bootstrap/bootstrap.js"></script>
<!-- Theme js-->
<script src="/admin_main/js/script.js"></script>
<script src="/admin_main/js/royal_preloader.min.js"></script>
<script type="text/javascript">
    window.jQuery = window.$ = jQuery;
    (function($) { "use strict";
        //Preloader
        Royal_Preloader.config({
            mode           : 'logo',
            logo           : '/admin_main/images/logo.png',
            logo_size      : [145, 75],
            showProgress   : true,
            showPercentage : true,
            text_colour: '#000000',
            background:  '#ffffff'
        });
    })(jQuery);
</script>
</body>
</html>
