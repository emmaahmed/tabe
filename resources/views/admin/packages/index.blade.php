@extends('layouts.admin', ['title' => __('packages')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('packages')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Packages')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Title')}}</th>
                            <th>{{__('Period')}}</th>
                            <th>{{__('Advertisements_count')}}</th>
                            <th>{{(__('Price'))}}</th>
                            <th>{{(__('Description'))}}</th>
                            <th>{{(__('Status'))}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create category -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.packages.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image" >{{__('Image')}}  {{__('Width')}} 66 px  * {{__('Height')}} 66 px</label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M" required/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <input type="text" name="title_ar" class="form-control" value="{{old('title_ar')}}" placeholder="{{__('Title')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'title_ar'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Period')}} ({{__('Invisible')}}) <span style="color:red">*</span></label>
                    <select class="form-control" id="period"  name="period">
                        <option label="{{__('Period')}}"></option>

                        <option value="1">1 {{__('Month')}}</option>
                        <option value="2">2 {{__('Months')}}</option>
                        <option value="3">3 {{__('Months')}}</option>
                        <option value="4">4 {{__('Months')}}</option>
                        <option value="5">5 {{__('Months')}}</option>
                        <option value="6">6 {{__('Months')}}</option>
                        <option value="7">7 {{__('Months')}}</option>
                        <option value="8">8 {{__('Months')}}</option>
                        <option value="9">9 {{__('Months')}}</option>
                        <option value="10">10 {{__('Months')}}</option>
                        <option value="11">11 {{__('Months')}}</option>
                        <option value="12">1 {{__('Year')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'period'])
                </div>
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Advertisements_count')}} ({{__('Invisible')}}) <span style="color:red">*</span></label>
                    <input type="text" name="advertisements_count" class="form-control" value="{{old('advertisements_count')}}" placeholder="{{__('Advertisements_count')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'advertisements_count'])
                </div>
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Price')}} <span style="color:red">*</span></label>
                    <input type="text" name="price" class="form-control" value="{{old('price')}}" placeholder="{{__('Price')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'price'])
                </div>

                <div class="col-sm-12 ">
                    <label class="text-muted font-14">{{__('Description')}} <span style="color:red">*</span></label>
                    <textarea name="desc_ar" id="desc_ar" class="form-control" rows="6" cols="80" placeholder="{{__('Description')}}"  data-emojiable="true" data-emoji-input="unicode"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'desc_ar'])
                </div>

            </div>

        @endslot
    @endcomponent
    <!-- End create category -->

    <!-- Start edit category -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 66 px  * {{__('Height')}} 66 px</label>
                    <input type="file" name="image"  id="e_image" class="dropify" data-max-file-size="40M" />
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <input type="text" name="title_ar" id="title_ar" class="form-control" placeholder="{{__('Title')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'title_ar'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Period')}} ({{__('Invisible')}})</label>
                    <select class="form-control" id="period_edit"  name="period">
                        <option label="{{__('Period')}}"></option>

                        <option value="1">1 {{__('Month')}}</option>
                        <option value="2">2 {{__('Months')}}</option>
                        <option value="3">3 {{__('Months')}}</option>
                        <option value="4">4 {{__('Months')}}</option>
                        <option value="5">5 {{__('Months')}}</option>
                        <option value="6">6 {{__('Months')}}</option>
                        <option value="7">7 {{__('Months')}}</option>
                        <option value="8">8 {{__('Months')}}</option>
                        <option value="9">9 {{__('Months')}}</option>
                        <option value="10">10 {{__('Months')}}</option>
                        <option value="11">11 {{__('Months')}}</option>
                        <option value="12">1 {{__('Year')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'period'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Advertisements_count')}} ({{__('Invisible')}}) <span style="color:red">*</span></label>
                    <input type="text" name="advertisements_count" id="advertisements_count" class="form-control"  placeholder="{{__('Advertisements_count')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'advertisements_count'])
                </div>
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Price')}} <span style="color:red">*</span></label>
                    <input type="text" name="price" id="price" class="form-control"  placeholder="{{__('Price')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'price'])
                </div>
                <div class="col-sm-12 ">
                    <label class="text-muted font-14">{{__('Description')}} <span style="color:red">*</span></label>
                    <textarea name="desc_ar" id="desc_ar_edit" class="form-control" rows="6" cols="80" placeholder="{{__('Description')}}"  data-emojiable="true" data-emoji-input="unicode"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'desc_ar'])
                </div>


            </div>
        @endslot
    @endcomponent
    <!-- End edit category -->

    <!-- Start delete category -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete category -->
@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
    <script src="/admin_main/ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'desc_ar' );
    </script>

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/packages",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: 'image', render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },                    { data: 'title_ar' },
                    { data: 'period' },
                    { data: 'advertisements_count' },
                    { data: 'price' },
                    { data: 'desc_ar' },
                    { data: "status", render: function (data, type, row)
                        {
                            if(row.active === 1) {
                                return  '<label class="switch">'+
                                        '<input type="checkbox" id="switchValue'+row.id+'" checked="" onchange=changeStatus('+row.id+')>' +
                                        '<span class="switch-state"></span>'+
                                        '</label>'
                            } else {
                                return  '<label class="switch">'+
                                        '<input type="checkbox" id="switchValue'+row.id+'" onchange=changeStatus('+row.id+')>' +
                                        '<span class="switch-state"></span>'+
                                        '</label>'
                            }
                        }
                    },
                    { data: "operations", render: function (data, type, row) {

                            return  "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                    '<i class="fa fa-edit"></i>' +
                                    '</button> ' +
                                    '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                                    '<i class="fa fa-trash"></i>' +
                                    '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(category)
        {
            console.log(category);
            $('#title_ar').val(category.title_ar);
            var periodVal=category.period;
            // $('#period').val(category.period);
            $("#period_edit option[value ='"+periodVal+"' ]").prop('selected', true);
            $('#advertisements_count').val(category.advertisements_count);
            $('#price').val(category.price);
            CKEDITOR.replace( 'desc_ar_edit' );
            CKEDITOR.instances['desc_ar_edit'].setData(category.desc_ar)

            // CKEDITOR.instances['desc_ar'].insertHtml(category.desc_ar);

            // $('#desc_ar_edit').text(category.desc_ar);
            let imagenUrl = category.image;

            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();


            $('.action_form').attr('action', '{{route('admin.packages.update', '')}}' + '/' + category.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.packages.store')}}');
        })

        function openModalDelete(category_id)
        {
            $('.action_form').attr('action', '{{route('admin.packages.destroy', '')}}' + '/' + category_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.packages.store')}}');
        })

        function changeStatus(categoryId)
        {
            var isChecked = document.getElementById("switchValue"+categoryId).checked;

            $.get( '{{route('admin.packages.changeStatus', '')}}' + '/' + categoryId,
                { 'status': isChecked },
                function( data ) {
                    $.notify({
                            message:'The action ran successfully!'
                        },
                        {
                            type:'success',
                            allow_dismiss:true,
                            timer:1000,
                            delay:1000 ,
                        });
                });
        }

    </script>

@endsection
