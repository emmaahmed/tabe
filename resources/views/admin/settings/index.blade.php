@extends('layouts.admin', ['title' => __($type)])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__($type)}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__($type)}}</li>
    </ol>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>{{(__('Content'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start edit settings -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Content')}}</label>
                    @if($type == 'premium_cost')
                        <input name="edit_value" id="e_value" class="form-control" placeholder="{{__('Content')}}">
                    @elseif($type == 'advertisement_modify_time')
                        <select class="form-control" id="e_value" name="edit_value">
                            @for($i = 1; $i < 11; $i++)
                                <option value="{{$i}}">{{$i}} - ساعات </option>
                            @endfor
                        </select>
                    @else
                        <textarea name="edit_value" id="e_value" class="form-control" rows="10" placeholder="{{__('Content')}}"></textarea>
                    @endif
                    @include('admin.includes.alerts.error', ['input' => 'edit_value'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit settings -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "/api/datatable/settings?type={{$type}}",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'value' },
                    { data: "operations", render: function (data, type, row) {
                            return "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                '<i class="fa fa-edit"></i>' +
                                '</button> '
                        }
                    },
                ],
            });
        });

        function openModalEdit(setting) {
            $('#e_value').val(setting.value);

            $('.action_form').attr('action', '{{route('admin.settings.update', '')}}' + '/' + setting.key);
            $('#editModal').modal('show');
        }

    </script>
@endsection
