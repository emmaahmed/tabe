@extends('layouts.admin', ['title' => __('Report reasons')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Report reasons')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Report reasons')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{(__('Reason'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.report_reasons.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Reason')}} <span style="color:red">*</span></label>
                    <input type="text" name="reason" class="form-control" value="{{old('reason')}}" placeholder="{{__('Reason')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'reason'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End create admin -->

    <!-- Start edit admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Reason')}} </label>
                    <input type="text" id="e_reason" name="edit_reason" class="form-control" value="{{old('edit_reason')}}" placeholder="{{__('Reason')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_reason'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit admin -->

    <!-- Start delete admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete admin -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "/api/datatable/report_reasons",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: 'reason' },
                    { data: "operations", render: function (data, type, row) {
                            return "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                '<i class="fa fa-edit"></i>' +
                                '</button> ' +
                                '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                                '<i class="fa fa-trash"></i>' +
                                '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(data) {
            $('#e_reason').val(data.reason);

            $('.action_form').attr('action', '{{route('admin.report_reasons.update', '')}}' + '/' + data.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.report_reasons.store')}}');
        })

        function openModalDelete(data_id) {
            $('.action_form').attr('action', '{{route('admin.report_reasons.destroy', '')}}' + '/' + data_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.report_reasons.store')}}');
        })
    </script>
@endsection
