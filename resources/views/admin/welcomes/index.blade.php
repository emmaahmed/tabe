@extends('layouts.admin', ['title' => __('Welcome pages')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Welcome pages')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Welcome pages')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{(__('Image'))}}</th>
                            <th>{{(__('Title'))}}</th>
                            <th>{{(__('Content'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start edit welcome -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.welcomes.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <input type="text" name="title" class="form-control" placeholder="{{__('Title')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'title'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Content')}} <span style="color:red">*</span></label>
                    <textarea type="text" name="text" class="form-control" required></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'text'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 375 px  * {{__('Height')}} 368 px <span style="color:red">*</span></label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M" required/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit welcome -->

    <!-- Start edit welcome -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}}</label>
                    <input type="text" id="e_title" name="edit_title" class="form-control" placeholder="{{__('Title')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_title'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Content')}}</label>
                    <textarea type="text" id="e_content" name="edit_content" class="form-control" required></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'edit_content'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 375 px  * {{__('Height')}} 368 px</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit welcome -->

    <!-- Start delete welcome -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete welcome -->
@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>

        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "/api/datatable/welcomes",
                    "dataSrc": "data.data",
                    "data" : {'parent_id': '{{isset($welcome->id) ? $welcome->id : NULL}}' },
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: "image", render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },
                    { data: 'title' },
                    { data: 'content' },
                    { data: "operations", render: function (data, type, row) {

                        return "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                            '<i class="fa fa-edit"></i>' +
                            '</button> ' +
                            '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                            '<i class="fa fa-trash"></i>' +
                            '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(welcome)
        {
            $('#e_title').val(welcome.title);
            $('#e_content').val(welcome.content);
            let imagenUrl = welcome.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.welcomes.update', '')}}' + '/' + welcome.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.welcomes.store')}}');
        })


        function openModalDelete(category_id)
        {
            $('.action_form').attr('action', '{{route('admin.welcomes.destroy', '')}}' + '/' + category_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.welcomes.store')}}');
        })

    </script>
@endsection
