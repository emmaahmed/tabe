<a href="{{route('admin.admins.show', $data->id)}}"  title="{{__('View')}}" class="btn btn-primary btn-sm a_button">
    <i class="fa fa-eye"></i>
</a>
<a href="{{route('admin.admins.history', $data->id)}}" target="_blank" title="{{__('View')}}" class="btn btn-primary btn-sm a_button">
    <i class="fa fa-file"></i>
</a>
@can('update', 'Admin')
<button type="button" title="{{__('Edit')}}" class="btn btn-warning btn-sm" onclick="openModalEdit({{$data}})">
    <i class="fa fa-edit"></i>
</button>
@endcan
@can('delete', 'Admin')
<button id="delete" title="{{__('Delete')}}" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->id }}').submit()
    }
    ">
    <i class="fa fa-trash"></i>
    <form id="destroy{{ $data->id }}" class="d-none" action="{{ route('admin.admins.destroy', $data->id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
@endcan
