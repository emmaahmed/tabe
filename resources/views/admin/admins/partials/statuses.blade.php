@if($data->active === 1)
    <label class="switch">
        <input type="checkbox" id="switchValue" checked=""
               onchange = "window.location.href = '{{route('admin.admins.changeStatus', $data->id)}}' " >
        <span class="switch-state"></span>
    </label>
@else
    <label class="switch">'+
        <input type="checkbox" id="switchValue"
               onchange = "window.location.href = '{{route('admin.admins.changeStatus', $data->id)}}' " >
        <span class="switch-state"></span>
    </label>
@endif
