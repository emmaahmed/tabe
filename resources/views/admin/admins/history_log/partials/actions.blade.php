@if($data->description == 'created' || $data->description == 'updated')
    <button type="button" title="{{__('View details')}}" class="btn btn-primary btn-sm"
            onclick="openModalViewDetails({{$data}})">
        <i class="fa fa-eye"></i>
    </button>
@endif
@can('delete', 'Admin')
<button id="delete" class="btn btn-danger btn-sm mt-1" title="{{__('Delete')}}" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->id }}').submit()
    }
    ">
    <i class="fa fa-trash"></i>
    <form id="destroy{{ $data->id }}" class="d-none" action="{{ route('admin.admins.destroyHistoryLog', $data->id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
@endcan
