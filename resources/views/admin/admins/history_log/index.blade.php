@extends('layouts.admin', ['title' => __('Admins')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('History log')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item">{{__('Admins')}}</li>
        <li class="breadcrumb-item active">{{__('History log')}}</li>
    </ol>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table-bordered cell-border', 'id' => 'admins-table']) !!}
            </div>
        </div>
    </div>

    <!-- Start view history log -->
    @component('admin.includes.modal')
        @slot('modalID')
            viewModal
        @endslot
        @slot('modalHistory')
            history
        @endslot
        @slot('modalTitle')
            {{__('View details')}}
        @endslot
        @slot('modalContent')
            <table class="table" id="tableViewDetails">
                <tbody></tbody>
            </table>
        @endslot
    @endcomponent
    <!-- End view history log -->
@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <script src="/admin_main/js/calendar/moment.min.js"></script>

    <script src="/admin_main/js/main/history_log.js"></script>

    {!! $dataTable->scripts() !!}
@endsection
