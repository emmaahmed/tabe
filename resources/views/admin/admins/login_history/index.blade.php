@extends('layouts.admin', ['title' => __('History')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('History')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('History')}}</li>
    </ol>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('Logged_in_at')}}</th>
                            <th>{{__('Logged_out_at')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/histories",
                    "dataSrc": "data.data",
                    "data" : {'admin_id': '{{isset($admin_id) ? $admin_id : NULL}}' },
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: 'logged_in_at' },
                    { data: 'logged_out_at' },
                ],
            });
        });


    </script>
@endsection
