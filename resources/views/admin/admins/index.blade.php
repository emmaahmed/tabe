@extends('layouts.admin', ['title' => __('Admins')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Admins')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Admins')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table-bordered cell-border', 'id' => 'admins-table']) !!}
            </div>
        </div>
    </div>

    <!-- Start create admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.admins.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Email')}}</label>
                    <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="{{__('Email')}}">
                    @include('admin.includes.alerts.error', ['input' => 'email'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Phone')}} <span style="color:red">*</span></label>
                    <input type="text" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('Phone')}}"
                           title="{{__('Please fill out this field')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'phone'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password')}} <span style="color:red">*</span></label>
                    <input type="password" name="password" class="form-control" placeholder="{{__('Password')}}">
                    @include('admin.includes.alerts.error', ['input' => 'password'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password confirmation')}} <span style="color:red">*</span></label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="{{__('Password confirmation')}}">
                    @include('admin.includes.alerts.error', ['input' => 'password_confirmation'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}}</label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

                <div class="col-sm-12 mt-2">

                    <label class="text-muted font-14" for="image">{{__('Permissions')}}</label>

                    @php
                        $models = ['dashboard', 'admins', 'users', 'categories', 'advertisements', 'banners',
                                    'countries', 'welcomes', 'settings', 'notifications', 'companies', 'reports'];
                        $maps = ['create', 'read', 'update', 'delete'];
                    @endphp

                    <table class="table">
                        <thead class="thead-light">
                        </thead>
                        <tbody>
                        @foreach ($models as $index=>$model)
                            <tr>
                                <th scope="row">{{__($model)}}</th>
                                @if($model == 'dashboard' || $model == 'reports')
                                    <td>
                                        <input type="checkbox" class="check" name="permissions[]" value="{{ $maps[1] . '_' . $model }}">
                                        {{__($maps[1])}}
                                    </td>
                                @else
                                    @foreach ($maps as $map)
                                        <td>
                                            <input type="checkbox" class="check" name="permissions[]" value="{{ $map . '_' . $model }}"> {{__($map)}}
                                        </td>
                                    @endforeach
                                        <td>
                                            <input type="checkbox" class="checkAll"> *
                                        </td>
                                @endif
                            </tr>
                        @endforeach
                        @include('admin.includes.alerts.error', ['input' => 'permissions'])
                        </tbody>
                    </table>
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End create admin -->

    <!-- Start edit admin -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_name" name="edit_name" class="form-control" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Email')}}</label>
                    <input type="email" id="e_email" name="edit_email" class="form-control" placeholder="{{__('Email')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_email'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Phone')}}</label>
                    <input type="text" id="e_phone" name="edit_phone" class="form-control" placeholder="{{__('Phone')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_phone'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password')}}</label>
                    <input type="password" name="edit_password" class="form-control" placeholder="{{__('Password')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_password'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Password confirmation')}}</label>
                    <input type="password" name="edit_password_confirmation" class="form-control" placeholder="{{__('Password confirmation')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_password_confirmation'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}}</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Permissions')}}</label>

                    <table class="table">
                        <thead class="thead-light">
                        </thead>
                        <tbody class="permissionCheckBox">
                        @foreach ($models as $index=>$model)
                            <tr>
                                <th scope="row">{{__($model)}}</th>
                                @if($model == 'dashboard' || $model == 'reports')
                                    <td>
                                        <input type="checkbox" class="check" id="e_permission_{{ $maps[1] . '_' . $model }}" name="permissions[]" value="{{ $maps[1] . '_' . $model }}">
                                        {{__($maps[1])}}
                                    </td>
                                @else
                                    @foreach ($maps as $map)
                                        <td>
                                            <input type="checkbox" class="check" id="e_permission_{{ $map . '_' . $model }}" name="permissions[]" value="{{ $map . '_' . $model }}"> {{__($map)}}
                                        </td>
                                    @endforeach
                                    <td>
                                        <input type="checkbox" class="checkAll"> *
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit admin -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>

    {!! $dataTable->scripts() !!}
    <script>
        function openModalEdit(admin) {
            $('#e_type').val(admin.type);
            $('#e_name').val(admin.name);
            $('#e_email').val(admin.email);
            $('#e_phone').val(admin.phone);

            let imagenUrl = admin.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            if(admin.permissions != null) {
                $('.permissionCheckBox input:checkbox').prop('checked', false);
                $.each(admin.permissions, function( index, value ) {
                    $( "#e_permission_"+value.slug ).prop( "checked", true );
                });
            }

            $('.action_form').attr('action', '{{route('admin.admins.update', '')}}' + '/' + admin.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.admins.store')}}');
        })

        $(".checkAll").click(function () {
            $(this).parent().parent().find('.check').prop('checked', this.checked);
        });

    </script>
@endsection
