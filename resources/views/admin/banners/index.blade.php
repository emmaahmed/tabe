@extends('layouts.admin', ['title' => __('Banners')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Banners')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Banners')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{(__('Image'))}}</th>
                            <th>{{(__('Type'))}}</th>
                            <th>{{(__('Title'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create banner -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.banners.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Type')}}</label>
                    <select class="form-control" name="link_type" id="link_type">
                        <option value="public">{{__('Public')}}</option>
                        <option value="company">{{__('Company')}}</option>
                        <option value="website">{{__('Website')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'link_type'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="country">{{__('Countries')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="country_id" id="country">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="all">{{__('All countries')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'country_id'])
                </div>

                <div class="col-sm-12 mt-2" id="div_companies" style="display: none">
                    <label class="text-muted font-14" for="company">{{__('Companies')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="company_id">
                        <option disabled selected>{{__('Select Action')}}</option>
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'company_id'])
                </div>

                <div class="col-sm-12 mt-2" id="div_website" style="display: none">
                    <label class="text-muted font-14" for="website_link">{{__('Website')}} <span style="color:red">*</span></label>
                    <input type="text" id="website_link" name="website_link" class="form-control" placeholder="{{__('Put the link')}}">
                    @include('admin.includes.alerts.error', ['input' => 'website_link'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}}  {{__('Width')}} 356 px  * {{__('Height')}} 140 px <span style="color:red">*</span> </label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Onclick image')}}  {{__('Width')}} 540 px  * {{__('Height')}} 960 px<span style="color:red">*</span></label>
                    <input type="file" name="large_image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'large_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End create banner -->

    <!-- Start edit banner -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Type')}}</label>
                    <select class="form-control" name="edit_link_type" id="e_link_type">
                        <option value="public">{{__('Public')}}</option>
                        <option value="company">{{__('Company')}}</option>
                        <option value="website">{{__('Website')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_link_type'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="country">{{__('Countries')}}</label>
                    <select class="form-control" name="edit_country_id" id="e_country_id">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="all">{{__('All countries')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_country_id'])
                </div>

                <div class="col-sm-12 mt-2" id="e_div_companies">
                    <label class="text-muted font-14" for="company">{{__('Companies')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="edit_company_id" id="e_company">
                        <option disabled selected>{{__('Select Action')}}</option>
                        @foreach($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_company_id'])
                </div>

                <div class="col-sm-12 mt-2" id="e_div_website">
                    <label class="text-muted font-14" for="e_website_link">{{__('Website')}} <span style="color:red">*</span></label>
                    <input type="text" id="e_website_link" name="edit_website_link" class="form-control" placeholder="{{__('Put the link')}}">
                    @include('admin.includes.alerts.error', ['input' => 'edit_website_link'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 356 px  * {{__('Height')}} 140 px </label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Onclick image')}} {{__('Width')}} 540 px  * {{__('Height')}} 960 px</label>
                    <input type="file" id="e_large_image" name="edit_large_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_large_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit banner -->

    <!-- Start delete banner -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete banner -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        $(document).ready(function() {
            $('#link_type').on('change', function () {
                if ($(this).val() === 'company') {
                    $('#div_website').hide('fade in');
                    $('#div_companies').show('fade in');
                }else if ($(this).val() === 'website'){
                    $('#div_companies').hide('fade in');
                    $('#div_website').show('fade in');
                }else {
                    $('#div_companies').hide('fade in');
                    $('#div_website').hide('fade in');
                }
            })

            $('#e_link_type').on('change', function () {
                if ($(this).val() === 'company') {
                    $('#e_div_website').hide('fade in');
                    $('#e_div_companies').show('fade in');
                }else if ($(this).val() === 'website'){
                    $('#e_div_companies').hide('fade in');
                    $('#e_div_website').show('fade in');
                }else {
                    $('#e_div_companies').hide('fade in');
                    $('#e_div_website').hide('fade in');
                }
            })

            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/banners",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: "image", render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },
                    { data: "link_type", render: function (data, type, row)
                        {
                            if (row.link_type === 'public') {
                                return '<span>عام</span>'
                            }

                            if (row.link_type === 'company') {
                                return '<span>شركة</span>'
                            }

                            return '<span>ويب سايت</span>'
                        }
                    },
                    { data: 'title' },
                    { data: "operations", render: function (data, type, row) {
                        return "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                            '<i class="fa fa-edit"></i>' +
                            '</button> '+
                            '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                            '<i class="fa fa-trash"></i>' +
                            '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(banner) {
            if (banner.link_type === 'company') {
                $('#e_div_website').hide();
                $('#e_div_companies').show();
                $('#e_company').val(banner.link_id);
            }else if (banner.link_type === 'website') {
                $('#e_div_website').show();
                $('#e_div_companies').hide();
                $('#e_website_link').val(banner.website_link);
            }else {
                $('#e_div_companies').hide();
                $('#e_div_website').hide();
            }

            banner.country_id == null ? $('#e_country_id').val('all') : $('#e_country_id').val(banner.country_id);
            $('#e_link_type').val(banner.link_type);
            $('#e_is_slider').prop('checked', banner.is_slider)

            let imagenUrl = banner.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            let largeImageUrl = banner.large_image;
            let drEventLarge = $('#e_large_image').dropify(
                {
                    defaultFile: largeImageUrl
                });
            drEventLarge = drEventLarge.data('dropify');
            drEventLarge.resetPreview();
            drEventLarge.clearElement();
            drEventLarge.settings.defaultFile = largeImageUrl;
            drEventLarge.destroy();
            drEventLarge.init();

            $('.action_form').attr('action', '{{route('admin.banners.update', '')}}' + '/' + banner.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.banners.store')}}');
        })

        function openModalDelete(admin_id) {
            $('.action_form').attr('action', '{{route('admin.banners.destroy', '')}}' + '/' + admin_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.banners.store')}}');
        })

    </script>
@endsection
