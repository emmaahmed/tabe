@extends('layouts.admin', ['title' => __('Properties details')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Properties details')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}">{{__('Categories')}}</a></li>
        <li class="breadcrumb-item">
            <a href="{{route('admin.category_fields.index')}}?category_id={{$category_id}}">
                {{__('Fields')}}
            </a>
        </li>
        <li class="breadcrumb-item active">{{__('Properties details')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#excelModal">
        <i class="fa fa-file-excel-o"></i>
        {{__('Attributes excel')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $key => $attribute)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $attribute->value }}</td>
                            <td>
                                <button class='btn btn-warning btn-sm btn-condensed' title="{{__('Edit')}}" onclick='openModalEdit({{$attribute}})' >
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class='btn btn-danger btn-sm btn-condensed' title="{{__('Delete')}}" onclick='openModalDelete({{$attribute->id}})' >
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create attributes -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.category_fields.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="parent_id" value="{{$id}}">

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="value" class="form-control" value="{{old('value')}}" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'value'])
                </div>

            </div>

        @endslot
    @endcomponent
    <!-- End create attributes -->

    <!-- Start create excel -->
    @component('admin.includes.modal')
        @slot('modalID')
            excelModal
        @endslot
        @slot('modalTitle')
            {{__('Attributes excel')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.category_fields.excel')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="parent_id" value="{{$id}}">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="file">{{__('File')}}</label>
                    <input type="file" id="file" name="file"/>
                    @include('admin.includes.alerts.error', ['input' => 'file'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create excel -->

    <!-- Start edit attributes -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_value" name="edit_value" class="form-control" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_value'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit attributes -->

    <!-- Start delete attributes -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete attributes -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
    <script>
        function openModalEdit(attribute)
        {
            $('#e_value').val(attribute.value);

            $('.action_form').attr('action', '{{route('admin.category_fields.update', '')}}' + '/' + attribute.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.category_fields.store')}}');
        })

        function openModalDelete(attribute_id)
        {
            $('.action_form').attr('action', '{{route('admin.category_fields.destroy', '')}}' + '/' + attribute_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.category_fields.store')}}');
        })
    </script>
@endsection
