@extends('layouts.admin', ['title' => __('Properties')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Properties')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Properties')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#excelModal">
        <i class="fa fa-file-excel-o"></i>
        {{__('Attributes excel')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Attributes type') }}</th>
                            <th scope="col">{{ __('Fields type') }}</th>
                            <th scope="col">{{ __('Image') }}</th>
                            <th scope="col">{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $key => $attribute)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $attribute->key }}</td>
                            <td>{{ $attribute->attribute_type == 1 ? __('Specifications') : __('Features') }}</td>
                            <td>{{ __($attribute->graphical_control_element) }}</td>
                            <td>
                                <img src="{{ $attribute->image }}" width="70px" >
                            </td>
                            <td>
                                <a href='{{route('admin.category_fields.show', $attribute->id)}}' title='{{__('View')}}' class='btn btn-primary btn-sm btn-condensed'>
                                    <i class="fa fa-eye"></i>
                                </a>
                                <button class='btn btn-warning btn-sm btn-condensed' title="{{__('Edit')}}" onclick='openModalEdit({{$attribute}})' >
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class='btn btn-danger btn-sm btn-condensed' title="{{__('Delete')}}" onclick='openModalDelete({{$attribute->id}})' >
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create fileds -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.category_fields.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="category_id" value="{{request()->category_id}}">

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="key" class="form-control" value="{{old('key')}}" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'key'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Attributes type')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="attribute_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="1">{{__('Specifications')}}</option>
                        <option value="2">{{__('Features')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'attribute_type'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Fields type')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="graphical_control_element">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="radio">{{__('radio')}}</option>
                        <option value="switch">{{__('switch')}}</option>
                        <option value="horizontal_view">{{__('horizontal view')}}</option>
                        <option value="text">{{__('text')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'graphical_control_element'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="used_tag">{{__('Used tag')}}</label>
                    <input type="checkbox" id="used_tag" name="used_tag" class="check">
                    ( {{__('Take first sign')}} )
                    @include('admin.includes.alerts.error', ['input' => 'used_tag'])
                </div>

                <div class="col-sm-12 mt-4">
                    <label class="text-muted font-14">{{__('Image')}} </label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>
            </div>
        @endslot
    @endcomponent
    <!-- End create fileds -->

    <!-- Start create excel -->
    @component('admin.includes.modal')
        @slot('modalID')
            excelModal
        @endslot
        @slot('modalTitle')
            {{__('Attributes excel')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.category_fields.excel')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="category_id" value="{{request()->category_id}}">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="file">{{__('File')}}</label>
                    <input type="file" id="file" name="file"/>
                    @include('admin.includes.alerts.error', ['input' => 'file'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create excel -->

    <!-- Start edit fileds -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_key" name="edit_key" class="form-control" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_key'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Attributes type')}} </label>
                    <select class="form-control" id="e_attribute_type" name="edit_attribute_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="1">{{__('Specifications')}}</option>
                        <option value="2">{{__('Features')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_attribute_type'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Fields type')}} </label>
                    <select class="form-control" id="e_graphical_control_element" name="edit_graphical_control_element">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="radio">{{__('radio')}}</option>
                        <option value="switch">{{__('switch')}}</option>
                        <option value="horizontal_view">{{__('horizontal view')}}</option>
                        <option value="text">{{__('text')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_graphical_control_element'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_used_tag">{{__('Used tag')}}</label>
                    <input type="checkbox" id="e_used_tag" name="edit_used_tag" class="check">
                    ( {{__('Take first sign')}} )
                    @include('admin.includes.alerts.error', ['input' => 'edit_used_tag'])
                </div>

                <div class="col-sm-12 mt-4">
                    <label class="text-muted font-14">{{__('Image')}}</label>
                    <input type="file" id="e_image" name="edit_image"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>
            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit fileds -->

    <!-- Start delete fileds -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete fileds -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        function openModalEdit(attribute)
        {
            $('#e_key').val(attribute.key);
            $('#e_attribute_type').val(attribute.attribute_type);
            $('#e_graphical_control_element').val(attribute.graphical_control_element);

            attribute.used_tag === 1 ?
            $('#e_used_tag').prop('checked', true)
            :
            $('#e_used_tag').prop('checked', false);

            let imagenUrl = attribute.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.category_fields.update', '')}}' + '/' + attribute.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.category_fields.store')}}');
        })

        function openModalDelete(fileds_id)
        {
            $('.action_form').attr('action', '{{route('admin.category_fields.destroy', '')}}' + '/' + fileds_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.category_fields.store')}}');
        })
    </script>
@endsection
