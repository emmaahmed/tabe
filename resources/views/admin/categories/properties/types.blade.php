@extends('layouts.admin', ['title' => __('Fields')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Fields')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Fields')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="datatable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tags as $key=>$tag)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $tag->name }}</td>
                            <td>
                                <button class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit({{$tag}})' >
                                    <i class="fa fa-edit"></i> {{__('Edit')}}
                                </button>
                                <button class='btn btn-danger btn-sm btn-condensed' onclick='openModalDelete({{$tag->id}})' >
                                    <i class="fa fa-trash"></i>{{__('Delete')}}
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalFormId')
            form_id
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.categories.add-advertisement-types')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="category_id" value="{{request()->category_id}}">

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                    @include('admin.includes.alerts.error', ['input' => 'value_type'])
                </div>
        @endslot
    @endcomponent
    <!-- End create category_fields -->

    <!-- Start edit category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalFormId')
            edit_form_id
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} </label>
                    <input type="text" id="e_key_ar" name="name" class="form-control" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End edit category_fields -->

    <!-- Start delete category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalFormId')
            delete_form_id
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete category_fields -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
{{--    <script src="/admin_main/js/ajax-normal-file.js"></script>--}}

    <script>
        function openModalEdit(attribute)
        {
            $('#e_key_ar').val(attribute.name);



            $('.action_form').attr('action', '{{route('admin.categories.update-advertisement-types', '')}}' + '/' + attribute.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.categories.add-advertisement-types')}}');
        })

        function openModalDelete(category_fields_id)
        {
            $('.action_form').attr('action', '{{route('admin.categories.delete-advertisement-types', '')}}' + '/' + category_fields_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.property_fields.store')}}');
        })
    </script>
@endsection
