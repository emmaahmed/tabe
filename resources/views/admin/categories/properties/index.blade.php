@extends('layouts.admin', ['title' => __('Fields')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Fields')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Fields')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#excelModal">
        <i class="fa fa-file-excel-o"></i>
        {{__('Attributes excel')}}
    </button>

@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="datatable">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>

                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Attributes type') }}</th>
                            <th scope="col">{{ __('Fields type') }}</th>
                            <th scope="col">{{ __('Image') }}</th>
                            <th scope="col">{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attributes as $key => $attribute)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>

                            <td>{{ $attribute->name }}</td>
                            <td>{{ __($attribute->property_type) }}</td>
                            <td>{{ __($attribute->type) }}</td>
                            <td> <img src="{{ $attribute->image }}" width="70px"></td>
                            <td>
                                @if($attribute->type!='text' && $attribute->type!='switch')
                                <a href='{{route('admin.property_fields.show', $attribute->id)}}' title='{{__('View')}}' class='btn btn-primary btn-sm btn-condensed'>
                                    <i class="fa fa-eye"></i> {{__('View')}}
                                </a>
                                @endif
                                <button class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit({{$attribute}})' >
                                    <i class="fa fa-edit"></i> {{__('Edit')}}
                                </button>
                                <button class='btn btn-danger btn-sm btn-condensed' onclick='openModalDelete({{$attribute->id}})' >
                                    <i class="fa fa-trash"></i> {{__('Delete')}}
                                </button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalFormId')
            form_id
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.property_fields.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="category_id" value="{{request()->category_id}}">

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>


                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Type')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="value_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="radio">{{__('radio')}}</option>
                        <option value="switch">{{__('switch')}}</option>
                        <option value="horizontal_view">{{__('horizontal view')}}</option>
                        <option value="text">{{__('text')}}</option>
                        <option value="integer">{{__('integer')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'value_type'])
                </div>
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Property Type')}} <span style="color:red">*</span></label>
                    <select class="form-control" name="property_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="specification">{{__('Specifications')}}</option>
                        <option value="feature">{{__('Features')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'property_type'])
                </div>
                <div class="col-sm-12 mt-4">
                    <label class="text-muted font-14">{{__('Image')}} </label>
                    <input type="file" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>

            </div>
        @endslot
    @endcomponent
    <!-- End create category_fields -->

    <!-- Start edit category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalFormId')
            edit_form_id
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} </label>
                    <input type="text" id="e_key_ar" name="name" class="form-control" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>


                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Type')}}</label>
                    <select class="form-control" name="edit_value_type" id="e_value_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="radio">{{__('radio')}}</option>
                        <option value="switch">{{__('switch')}}</option>
                        <option value="horizontal_view">{{__('horizontal view')}}</option>
                        <option value="text">{{__('text')}}</option>
                        <option value="integer">{{__('integer')}}</option>

                    </select>

                    @include('admin.includes.alerts.error', ['input' => 'edit_value_type'])
                </div>
                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="key_type">{{__('Property Type')}} <span style="color:red">*</span></label>
                    <select id="property_type_edit" class="form-control" name="property_type">
                        <option disabled selected>{{__('Select Action')}}</option>
                        <option value="specification">{{__('Specifications')}}</option>
                        <option value="feature">{{__('Features')}}</option>
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'property_type'])
                </div>
                <div class="col-sm-12 mt-4">
                    <label class="text-muted font-14">{{__('Image')}}</label>
                    <input type="file" id="e_image" name="edit_image"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div>

        @endslot
    @endcomponent
    <!-- End edit category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            excelModal
        @endslot
        @slot('modalTitle')
            {{__('Attributes excel')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.category_fields.excel')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="category_id" value="{{request()->category_id}}">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="file">{{__('File')}}</label>
                    <input type="file" id="file" name="file"/>
                    @include('admin.includes.alerts.error', ['input' => 'file'])
                </div>
            </div>

        @endslot
    @endcomponent

    <!-- Start delete category_fields -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalFormId')
            delete_form_id
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete category_fields -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
{{--    <script src="/admin_main/js/ajax-normal-file.js"></script>--}}

    <script>
        function openModalEdit(attribute)
        {
            console.log(attribute);
            $('#e_key_ar').val(attribute.name);
            $("#e_value_type option[value ='"+attribute.type+"' ]").prop('selected', true);
            $("#property_type_edit option[value ='"+attribute.property_type+"' ]").prop('selected', true);

            // $('#e_value_type').val(attribute.value_type);

            $('.action_form').attr('action', '{{route('admin.property_fields.update', '')}}' + '/' + attribute.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.property_fields.store')}}');
        })

        function openModalDelete(category_fields_id)
        {
            $('.action_form').attr('action', '{{route('admin.property_fields.destroy', '')}}' + '/' + category_fields_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.property_fields.store')}}');
        })
    </script>
@endsection
