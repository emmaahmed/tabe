@extends('layouts.admin', ['title' => __('Sub categories')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Sub categories')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item">{{__('Categories')}}</li>
        <li class="breadcrumb-item active">{{__('Sub categories')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#excelModal">
        <i class="fa fa-file-excel-o"></i>
        {{__('Category excel')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <form id="frm-example" action="#" method="post">
                        @csrf
                        <table id="datatable">
                            <thead>
                            <tr>
                                <th style="text-align: left; width: 32px">
                                    <input type="checkbox" name="select_all" value="1" id="example-select-all">
                                </th>
                                <th>#</th>
                                <th>{{(__('Image'))}}</th>
                                <th>{{(__('Name'))}}</th>
                                <th>{{(__('Title sub category'))}}</th>
                                <th>{{(__('Actions'))}}</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <hr>

                        <button class="btn btn-danger">{{__('Delete')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create category -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.categories.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <input type="text" name="title_name" class="form-control" value="{{old('title_name')}}" placeholder="{{__('Title sub category')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'title_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="is_company">{{__('Is company')}}</label>
                    <input type="checkbox" id="is_company" name="is_company" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'is_company'])
                </div>

                <input type="hidden" name="parent_id" value="{{isset($category->id) ? $category->id : NULL}}">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 174 px  * {{__('Height')}} 131 px <span style="color:red">*</span></label>
                    <input type="file" id="image" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create category -->

    <!-- Start create category -->
    @component('admin.includes.modal')
        @slot('modalID')
            excelModal
        @endslot
        @slot('modalTitle')
            {{__('Category excel')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.categories.excel')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <input type="hidden" name="parent_id" value="{{isset($category->id) ? $category->id : NULL}}">

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="file">{{__('File')}}</label>
                    <input type="file" id="file" name="file"/>
                    @include('admin.includes.alerts.error', ['input' => 'file'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create category -->

    <!-- Start edit category -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_name" name="edit_name" class="form-control" placeholder="{{__('Name arabic')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} </label>
                    <input type="text" id="e_title_name" name="edit_title_name" class="form-control" placeholder="{{__('Title sub category')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_title_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_is_company">{{__('Is company')}}</label>
                    <input type="checkbox" id="e_is_company" name="edit_is_company" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'edit_is_company'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 174 px  * {{__('Height')}} 131 px</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit category -->

    <!-- Start delete category -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete category -->
@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        $(document).ready(function() {
            var table = $("#datatable").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [1, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/categories",
                    "dataSrc": "data.data",
                    "data" : {'parent_id': '{{isset($category->id) ? $category->id : NULL}}' },
                },
                'columnDefs': [{
                    'targets': 0,
                    'searchable':false,
                    'orderable':false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    }
                }],
                "columns": [
                    { data: null },
                    { data: 'id' },
                    { data: "image", render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },
                    { data: 'name' },
                    { data: 'title_name' },
                    { data: "operations", render: function (data, type, row) {
                            return (row.is_company === 1 ?
                                    "<a href='{{route('admin.category_fields.index')}}?category_id=" + row.id +"' title='{{__('Attributes')}}' class='btn btn-dark btn-sm btn-condensed a_button' >" +
                                    '<i class="fa fa-pencil"></i>' + '</a> '+
                                "<a href='{{route('admin.categories.get-advertisement-types')}}?category_id=" + row.id +"' class='btn btn-dark btn-sm btn-condensed a_button' title='{{__('Advertisements Types')}}' >" +
                                '<i class="fa fa-window-restore"></i> ' +
                                '</a> '

                                    :
                                    " <a href='{{route('admin.categories.show', '')}}"+ '/' + row.id +" ' class='btn btn-primary btn-sm btn-condensed a_button' >" +
                                    '<i class="fa fa-eye"></i>' +
                                    '</a> ')
                                    +"<button title='{{__('Edit')}}' type='button' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                    '<i class="fa fa-edit"></i>' +
                                    '</button> ' +
                                    '<button title="{{__('Delete')}}" type="button" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                                    '<i class="fa fa-trash"></i>' +
                                    '</button>'
                        }
                    },
                ],
            });

            $('#example-select-all').on('click', function(){
                var rows = table.rows({ 'search': 'applied' }).nodes();
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });

            $('#datatable tbody').on('change', 'input[type="checkbox"]', function(){
                if(!this.checked){
                    var el = $('#example-select-all').get(0);
                    if(el && el.checked && ('indeterminate' in el)){
                        el.indeterminate = true;
                    }
                }
            });

            $('#frm-example').submit(function(e){
                var form = this;

                table.$('input[type="checkbox"]').each(function(){
                    if(!$.contains(document, this)){
                        if(this.checked){
                            $(form).append(
                                $('<input>')
                                    .attr('type', 'hidden')
                                    .attr('name', this.name)
                                    .val(this.value)
                            );
                        }
                    }
                });

                $('#example-console').text($(form));
                console.log("Form submission", $(form));
                console.log(table.rows( { selected: true } ).data());


            });

        });

        function openModalEdit(category)
        {
            $('#e_name').val(category.name);
            $('#e_title_name').val(category.title_name);
            category.is_company === 1 ?
                $('#e_is_company').prop('checked', true)
                :
                $('#e_is_company').prop('checked', false);

            let imagenUrl = category.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.categories.update', '')}}' + '/' + category.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.categories.store')}}');
        })

        function openModalDelete(category_id)
        {
            $('.action_form').attr('action', '{{route('admin.categories.destroy', '')}}' + '/' + category_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.categories.store')}}');
        })

    </script>
@endsection
