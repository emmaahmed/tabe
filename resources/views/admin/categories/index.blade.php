@extends('layouts.admin', ['title' => __('Categories')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Categories')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Categories')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Order')}}</th>
                            <th>{{__('Title sub category')}}</th>
                            <th>{{(__('Status'))}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create category -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.categories.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} <span style="color:red">*</span></label>
                    <input type="text" name="title_name" class="form-control" value="{{old('title_name')}}" placeholder="{{__('Title sub category')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'title_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="is_company">{{__('Is company')}}</label>
                    <input type="checkbox" id="is_company" name="is_company" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'is_company'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 174 px  * {{__('Height')}} 131 px<span style="color:red">*</span></label>
                    <input type="file" id="image" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create category -->

    <!-- Start edit category -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_name" name="edit_name" class="form-control" placeholder="{{__('Name arabic')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Title')}} </label>
                    <input type="text" id="e_title_name" name="edit_title_name" class="form-control" placeholder="{{__('Title sub category')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_title_name'])
                </div>

                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Order')}} </label>
                    <input type="number" id="e_order" name="order" class="form-control" placeholder="{{__('Order')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'order'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_is_company">{{__('Is company')}}</label>
                    <input type="checkbox" id="e_is_company" name="edit_is_company" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'edit_is_company'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image" id="sizeLabel">{{__('Image')}} {{__('Width')}}  174 px  * {{__('Height')}} 131 px</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit category -->

    <!-- Start delete category -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete category -->
@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/categories",
                    "dataSrc": "data.data",
                    "data" : {'parent_id': '{{isset($category->id) ? $category->id : NULL}}' },
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: "image", render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },
                    { data: 'name' },
                    { data: 'order' },
                    { data: 'title_name' },
                    { data: "status", render: function (data, type, row)
                        {
                            if(row.active === 1) {
                                return  '<label class="switch">'+
                                        '<input type="checkbox" id="switchValue'+row.id+'" checked="" onchange=changeStatus('+row.id+')>' +
                                        '<span class="switch-state"></span>'+
                                        '</label>'
                            } else {
                                return  '<label class="switch">'+
                                        '<input type="checkbox" id="switchValue'+row.id+'" onchange=changeStatus('+row.id+')>' +
                                        '<span class="switch-state"></span>'+
                                        '</label>'
                            }
                        }
                    },
                    { data: "operations", render: function (data, type, row) {
                            return  " <a href='{{route('admin.categories.show', '')}}"+ '/' + row.id +" ' title='{{__('View')}}' class='btn btn-primary btn-sm btn-condensed a_button' >" +
                                    '<i class="fa fa-eye"></i>' +
                                    '</a> ' +
                                (row.is_company != 1 && !row.name.includes("VIP") && row.id!=1 && row.id!=3010?
                                    {{--"<a href='{{route('admin.category_fields.index')}}?category_id=" + row.id +"' title='{{__('Attributes')}}' class='btn btn-dark btn-sm btn-condensed a_button' >" +--}}
                                    {{--'<i class="fa fa-pencil"></i>' + '</a> '+--}}
                                    "<a href='{{route('admin.property_fields.index')}}?category_id=" + row.id +"' title='{{__('Attributes')}}' class='btn btn-dark btn-sm btn-condensed a_button' >" +
                                    '<i class="fa fa-pencil"></i>' + '</a> '                                    +
                                    "<a href='{{route('admin.categories.get-advertisement-types')}}?category_id=" + row.id +"' class='btn btn-dark btn-sm btn-condensed a_button' title='{{__('Advertisements Types')}}' >" +
                                    '<i class="fa fa-window-restore"></i> ' +
                                    '</a> '

                                    : "")
                                    +"<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                    '<i class="fa fa-edit"></i>' +
                                    '</button> ' +
                                    '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                                    '<i class="fa fa-trash"></i>' +
                                    '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(category)
        {
            $('#e_name').val(category.name);
            $('#e_title_name').val(category.title_name);
            $('#e_order').val(category.order);
            if(category.id==3010){
                $('#sizeLabel').text("{{__('Image')}} {{__('Width')}}  353 px  * {{__('Height')}} 131 px");
            }
            else{
                $('#sizeLabel').text("{{__('Image')}} {{__('Width')}}  174 px  * {{__('Height')}} 131 px");

            }

            category.is_company === 1 ?
                $('#e_is_company').prop('checked', true)
                :
                $('#e_is_company').prop('checked', false);

                let imagenUrl = category.image;
                let drEvent = $('#e_image').dropify(
                    {
                        defaultFile: imagenUrl
                    });
                drEvent = drEvent.data('dropify');
                drEvent.resetPreview();
                drEvent.clearElement();
                drEvent.settings.defaultFile = imagenUrl;
                drEvent.destroy();
                drEvent.init();

            $('.action_form').attr('action', '{{route('admin.categories.update', '')}}' + '/' + category.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.categories.store')}}');
        })

        function openModalDelete(category_id)
        {
            $('.action_form').attr('action', '{{route('admin.categories.destroy', '')}}' + '/' + category_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.categories.store')}}');
        })

        function changeStatus(categoryId)
        {
            var isChecked = document.getElementById("switchValue"+categoryId).checked;

            $.get( '{{route('admin.categories.changeStatus', '')}}' + '/' + categoryId,
                { 'status': isChecked },
                function( data ) {
                    $.notify({
                            message:'The action ran successfully!'
                        },
                        {
                            type:'success',
                            allow_dismiss:true,
                            timer:1000,
                            delay:1000 ,
                        });
                });
        }

    </script>
@endsection
