@extends('layouts.admin', ['title' => __('Countries')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')

    <!-- dropify -->
    <link href="/admin_main/libs/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
    <h3>{{__('Countries')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Countries')}}</li>
    </ol>
@endsection
@section('button')
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#createModal">
        <i class="fa fa-plus"></i>
        {{__('Add')}}
    </button>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{(__('Image'))}}</th>
                            <th>{{(__('Name'))}}</th>
                            <th>{{(__('Currency'))}}</th>
                            <th>{{(__('Actions'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start create countries -->
    @component('admin.includes.modal')
        @slot('modalID')
            createModal
        @endslot
        @slot('modalTitle')
            {{__('Add')}}
        @endslot
        @slot('modalRoute')
            {{route('admin.countries.store')}}
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}} <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('Name')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Currency')}} <span style="color:red">*</span></label>
                    <input type="text" name="currency" class="form-control" value="{{old('currency')}}" placeholder="{{__('Currency')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'currency'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 37 px  * {{__('Height')}} 25 px <span style="color:red">*</span></label>
                    <input type="file" id="image" name="image" class="dropify" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'image'])
                </div>
            </div>

        @endslot
    @endcomponent
    <!-- End create countries -->

    <!-- Start edit countries -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14">{{__('Name')}}</label>
                    <input type="text" id="e_name" name="edit_name" class="form-control" placeholder="{{__('Name arabic')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_name'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Currency')}}</label>
                    <input type="text" id="e_currency" name="edit_currency" class="form-control" placeholder="{{__('Currency')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_currency'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="image">{{__('Image')}} {{__('Width')}} 37 px  * {{__('Height')}} 25 px</label>
                    <input type="file" id="e_image" name="edit_image" data-max-file-size="40M"/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_image'])
                </div>

            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit countries -->

    <!-- Start delete countries -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete countries -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <!-- dropify js -->
    <script src="/admin_main/libs/dropify/dropify.min.js"></script>

    <!-- form-upload init -->
    <script src="/admin_main/js/form-fileupload.init.js"></script>
    <script>

        $(document).ready(function() {
            $("#datatable").DataTable({
                stateSave: true,
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/countries",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'id' },
                    { data: "image", render: function (data, type, row)
                        { return '<img src="'+row.image+'" style="width: 70px">' }
                    },
                    { data: 'name' },
                    { data: 'currency' },
                    { data: "operations", render: function (data, type, row) {

                        return  "<button title='{{__('Edit')}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                            '<i class="fa fa-edit"></i>' +
                            '</button> ' +
                            '<button title="{{__('Delete')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+row.id+') >' +
                            '<i class="fa fa-trash"></i>' +
                            '</button>'
                        }
                    },
                ],
            });
        });

        function openModalEdit(countries)
        {
            $('#e_name').val(countries.name);
            $('#e_currency').val(countries.currency);

            let imagenUrl = countries.image;
            let drEvent = $('#e_image').dropify(
                {
                    defaultFile: imagenUrl
                });
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.settings.defaultFile = imagenUrl;
            drEvent.destroy();
            drEvent.init();

            $('.action_form').attr('action', '{{route('admin.countries.update', '')}}' + '/' + countries.id);
            $('#editModal').modal('show');
        }

        $('#editModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.countries.store')}}');
        })

        function openModalDelete(countries_id)
        {
            $('.action_form').attr('action', '{{route('admin.countries.destroy', '')}}' + '/' + countries_id);
            $('#deleteModal').modal('show');
        }

        $('#deleteModal').on('hide.bs.modal', function() {
            $('.action_form').attr('action', '{{route('admin.countries.store')}}');
        })
    </script>
@endsection
