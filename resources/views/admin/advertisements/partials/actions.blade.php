<button type="button" title="{{__('View details')}}" class="btn btn-primary btn-sm" onclick="openModalViewDetails({{$data}})">
    <i class="fa fa-eye"></i>
</button>
<button type="button" title="{{__('View images')}}" class="btn btn-dark btn-sm" onclick="openModalViewImages({{$data->images}})">
    <i class="fa fa-picture-o"></i>
</button>
@can('update', \App\Models\Advertisement::class)
<button type="button" title="{{__('Edit')}}" class="btn btn-warning btn-sm" onclick="openModalEdit({{$data}})">
    <i class="fa fa-edit"></i>
</button>
@endcan
<button type="button" title="{{__('Reports ads')}}" class="btn btn-info btn-sm" onclick="openModalReports({{$data}})">
    <span class="badge badge-danger" id="reportCount">{{$data->userReports()->where('admin_seen', 0)->count()}}</span>
    <i class="fa fa-bug"></i>
</button>
@can('delete', \App\Models\Advertisement::class)
<button id="delete" title="{{__('Delete')}}" class="btn btn-danger btn-sm" onclick="
    event.preventDefault();
    if (confirm('Are you sure? It will delete the data permanently!')) {
        document.getElementById('destroy{{ $data->id }}').submit()
    }
    ">
    <i class="fa fa-trash"></i>
    <form id="destroy{{ $data->id }}" class="d-none" action="{{ route('admin.advertisements.destroy', $data->id) }}" method="POST">
        @csrf
        @method('delete')
    </form>
</button>
@endcan
