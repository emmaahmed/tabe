@extends('layouts.admin', ['title' => __('Advertisement Reports')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Advertisement Reports')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item">{{__('Advertisements')}}</li>
        <li class="breadcrumb-item active">{{__('Advertisement Reports')}}</li>
    </ol>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable">
                        <thead>
                        <tr>
                            <th>{{(__('Advertisement id'))}}</th>
                            <th>{{(__('Advertisement title'))}}</th>
                            <th>{{(__('Reported user'))}}</th>
                            <th>{{(__('Reason'))}}</th>
                            <th>{{(__('Reporter user'))}}</th>
                            <th>{{(__('Reporter phone'))}}</th>
                            <th>{{(__('Created at'))}}</th>
                            <th>{{(__('Action'))}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Start delete advertisement -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete advertisement -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')
    <script>

        $(document).ready(function() {
            $("#datatable").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "processing": true, "serverSide": true, "order": [ [0, 'desc'] ],
                "ajax": {
                    url: "/api/datatable/advertisements/reports",
                    "dataSrc": "data.data",
                },
                language: {
                    url: '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json',
                },
                "columns": [
                    { data: 'advertisement_id' },
                    { data: 'title' },
                    { data: 'reported_name' },
                    { data: 'reason' },
                    { data: 'reporter_name' },
                    { data: 'reporter_phone' },
                    { data: 'date' },
                    { data: "operations", render: function (data, type, row) {

                            return  '<button class="btn btn-danger btn-sm btn-condensed" title="{{'Delete'}}" onclick=openModalDelete('+row.id+') >' +
                                        '<i class="fa fa-trash"></i>' +
                                    '</button>'
                        }
                    },
                ],
            });
        });

        function openModalDelete(report_id)
        {
            $('.action_form').attr('action', '{{route('admin.advertisements.reports.destroy', '')}}' + '/' + report_id);
            $('#deleteModal').modal('show');
        }
    </script>
@endsection
