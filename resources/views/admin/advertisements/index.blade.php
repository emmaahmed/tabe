@extends('layouts.admin', ['title' => __('Advertisements')])
@section('css')
    <!-- datatable style -->
    @include('admin.includes.datatable.style')
@endsection
@section('breadcrumb')
    <h3>{{__('Advertisements')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
        <li class="breadcrumb-item active">{{__('Advertisements')}}</li>
    </ol>
@endsection
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table-bordered cell-border', 'id' => 'advertisements-table']) !!}
            </div>
        </div>
    </div>

    <!-- Start edit advertisement -->
    @component('admin.includes.modal')
        @slot('modalID')
            editModal
        @endslot
        @slot('modalTitle')
            {{__('Edit')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('put')
        @endslot
        @slot('modalContent')
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-muted font-14" for="category_id">{{__('Categories')}}</label>
                    <select class="form-control" name="edit_category_id" id="e_category_id">
                        <option disabled selected>{{__('Select Action')}}</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_category_id'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Title')}}</label>
                    <input type="text" id="e_title" name="edit_title" class="form-control" placeholder="{{__('Title')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_title'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Price')}}</label>
                    <input type="number" step="0.01" id="e_price" name="edit_price" class="form-control" placeholder="{{__('Price')}}" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_price'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_country_id">{{__('Countries')}}</label>
                    <select class="form-control" id="e_country_id" name="edit_country_id">
                        <option disabled selected>{{__('Select Action')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                    @include('admin.includes.alerts.error', ['input' => 'edit_country_id'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_negotiable">{{__('Negotiable')}}</label>
                    <input type="checkbox" id="e_negotiable" name="edit_negotiable" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'edit_negotiable'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="e_premium">{{__('Premium')}}</label>
                    <input type="checkbox" id="e_premium" name="edit_premium" class="check">
                    @include('admin.includes.alerts.error', ['input' => 'edit_premium'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Description')}}</label>
                    <textarea id="e_description" name="edit_description" rows="5" class="form-control"></textarea>
                    @include('admin.includes.alerts.error', ['input' => 'edit_description'])
                </div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14">{{__('Ended at')}}</label>
                    <input type="date" id="e_ended_at" name="edit_ended_at" class="form-control" required>
                    @include('admin.includes.alerts.error', ['input' => 'edit_ended_at'])
                </div>

                <div class="col-sm-12 mt-2" id="e_images"></div>

                <div class="col-sm-12 mt-2">
                    <label class="text-muted font-14" for="example-name">{{__('Images')}}</label>
                    <input type="file" id="e_image" name="edit_images[]" multiple/>
                    @include('admin.includes.alerts.error', ['input' => 'edit_images'])
                </div>
            </div><!-- row -->
        @endslot
    @endcomponent
    <!-- End edit advertisement -->

    <!-- Start delete advertisement -->
    @component('admin.includes.modal')
        @slot('modalID')
            deleteModal
        @endslot
        @slot('modalTitle')
            {{__('Delete')}}
        @endslot
        @slot('modalMethodPutOrDelete')
            @method('delete')
        @endslot
        @slot('modalContent')
            <div class="text-center">
                <span class="text-danger font-16">
                    {{__('Are you sure you want to run this action?')}}
                </span>
            </div>
        @endslot
    @endcomponent
    <!-- End delete advertisement -->

    <!-- Start view advertisement -->
    <div class="modal fade" id="viewModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> {{__('View details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table" id="tableViewDetails">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End view advertisement -->

    <!-- Start view images advertisement -->
    <div class="modal fade" id="viewImagesModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> {{__('View Images')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center id="viewImages">
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End view images advertisement -->

    <!-- Start reports advertisement -->
    <div class="modal fade" id="reportModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> {{__('View details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table" id="tableReports">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End reports advertisement -->

@endsection

@section('script')
    @include('admin.includes.alerts.message')
    <!-- datatable scripts -->
    @include('admin.includes.datatable.scripts')

    <script src="/admin_main/js/calendar/moment.min.js"></script>

    {!! $dataTable->scripts() !!}

    <script>
        function openModalEdit(advertisement) {
            $('#e_category_id').val(advertisement.category_id);
            $('#e_title').val(advertisement.title);
            $('#e_price').val(advertisement.price);
            $('#e_country_id').val(advertisement.country_id);
            $('#e_latitude').val(advertisement.latitude);
            $('#e_longitude').val(advertisement.longitude);
            $('#e_negotiable').prop('checked', advertisement.negotiable)
            $('#e_premium').prop('checked', advertisement.premium)
            $('#e_is_new').prop('checked', advertisement.is_new)
            $('#e_description').val(advertisement.description);
            $('#e_ended_at').val(moment(advertisement.ended_at).format('YYYY-MM-DD'));

            $('.action_form').attr('action', '{{route('admin.advertisements.update', '')}}' + '/' + advertisement.id);
            $('#editModal').modal('show');
        }

        function openModalDelete(advertisement_id) {
            $('.action_form').attr('action', '{{route('admin.advertisements.destroy', '')}}' + '/' + advertisement_id);
            $('#deleteModal').modal('show');
        }

        function openModalReports(advertisement) {
            $('#tableReports tbody').empty();

            $.each(advertisement.user_reports, function(index, value) {
                $('#tableReports tbody').append(
                    `<tr class="bg-light">
                        <th>{{__('Reporter user')}}</th>
                        <td>${value.name}</td>
                    </tr>
                    <tr class="bg-light">
                        <th>{{__('Reporter phone')}}</th>
                        <td>${value.phone}</td>
                    </tr>`
                );
            })

            $.each(advertisement.reports, function(index, value) {
                $('#tableReports tbody').append(
                    `<tr class="bg-light">
                        <th>{{__('Reason')}}</th>
                        <td>${value.reason}</td>
                    </tr>
                    <tr class="bg-light">
                        <th>{{__('Created at')}}</th>
                        <td>${moment(value.pivot.created_at).format('YYYY-MM-DD')}</td>
                    </tr>`
                );
            })

            $.get("{{route('admin.advertisements.reports.update', '')}}" + '/' + advertisement.id, function(data, status){

            });

            $("#reportCount").text(0);

            $('#reportModal').modal('show');
        }

        function openModalViewDetails(advertisement) {
            $('#tableViewDetails tbody').empty();

            $('#tableViewDetails tbody').append(
                `<tr class="bg-light">
                    <th>{{__('ID')}}</th>
                    <td>${advertisement.id}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Name')}}</th>
                    <td>${advertisement.title}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('User name')}}</th>
                    <td>${advertisement.user.name}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('User phone')}}</th>
                    <td>${advertisement.user.phone}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Category')}}</th>
                    <td>${advertisement.category?advertisement.category.name:'-'}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Price')}}</th>
                    <td>${advertisement.price}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Premium')}}</th>
                    <td>${ (advertisement.premium == 1) ? "true" : "false"}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Negotiable')}}</th>
                    <td>${ (advertisement.negotiable == 1) ? "true" : "false"}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('View count')}}</th>
                    <td>${advertisement.view_count}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Address')}}</th>
                    <td>${advertisement.address}</td>
                </tr>`
            );

            $.each(advertisement.attributes, function(index, value) {
                $('#tableViewDetails tbody').append(
                    `<tr class="bg-light">
                        <th>${value.parent.key}</th>
                        <td>${value.value}</td>
                    </tr>`
                );
            })

            $.each(advertisement.sub_category_child, function(index, value) {
                $('#tableViewDetails tbody').append(
                    `<tr class="bg-light">
                        <th>${value.category.title_name}</th>
                        <td>${value.name}</td>
                    </tr>`
                );
            })

            $('#tableViewDetails tbody').append(
                `<tr class="bg-light">
                    <th>{{__('Description')}}</th>
                    <td>${advertisement.description}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Created at')}}</th>
                    <td>${moment(advertisement.created_at).format('YYYY-MM-DD')}</td>
                </tr>
                <tr class="bg-light">
                    <th>{{__('Ended at')}}</th>
                    <td>${moment(advertisement.ended_at).format('YYYY-MM-DD')}</td>
                </tr>`
            );

            $('#viewModal').modal('show');
        }

        function openModalViewImages(images) {
            $('#viewImages').empty();

            $.each(images, function(index, value) {
                console.log(value)
                $('#viewImages').append(
                    `
                    <div class="image-area mb-3" >
                        <img src="${value.image}" alt="Preview">
                        <a class="remove-image" href="{{route('admin.advertisements.deleteImage', '')}}/${value.id}"
                         style="display: inline;">&#215;</a>
                    </div>
                    `
                );
            })

            $('#viewImagesModal').modal('show');
        }

    </script>
@endsection
