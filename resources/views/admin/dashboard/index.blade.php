@extends('layouts.admin', ['title' => __('Dashboard')])
@section('breadcrumb')
    <h3>{{__('Dashboard')}}</h3>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard.index')}}"><i data-feather="home"></i></a></li>
    </ol>
@endsection
@section('content')
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <div class="card o-hidden">
            <div class="bg-app-first-color b-r-4 card-body">
                <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="navigation"></i></div>
                    <div class="media-body"><span class="m-0">{{__('Clients')}}</span>
                        <h4 class="mb-0 counter">{{$clients_count}}</h4><i class="icon-bg" data-feather="navigation"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <div class="card o-hidden">
            <div class="bg-app-second-color b-r-4 card-body">
                <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="box"></i></div>
                    <div class="media-body"><span class="m-0">{{__('Advertisements')}}</span>
                        <h4 class="mb-0 counter">{{$advertisements_count}}</h4><i class="icon-bg" data-feather="box"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <div class="card o-hidden">
            <div class="bg-app-first-color b-r-4 card-body">
                <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="message-square"></i></div>
                    <div class="media-body"><span class="m-0">{{__('Average ads')}}</span>
                        <h4 class="mb-0 counter">{{$advertisements_avg}}</h4><i class="icon-bg" data-feather="message-square"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3 col-lg-6">
        <div class="card o-hidden">
            <div class="bg-app-second-color b-r-4 card-body">
                <div class="media static-top-widget">
                    <div class="align-self-center text-center"><i data-feather="users"></i></div>
                    <div class="media-body"><span class="m-0">{{__('Profit')}}</span>
                        <h4 class="mb-0 counter">{{$advertisements_profit}}</h4><i class="icon-bg" data-feather="users"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xl-6">
        <div class="card">
            <div class="card-header">
                <h5>{{__('Advertisements')}} <span class="digits">{{__('Status')}}</span></h5>
            </div>
            <div class="card-body chart-block">
                <div class="chart-overflow" id="pie-users"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xl-6">
        <div class="card">
            <div class="card-header">
                <h5>{{__('Advertisements')}} <span class="digits">{{__('Profit')}}</span></h5>
            </div>
            <div class="card-body chart-block">
                <div class="chart-overflow" id="pie-advertisements"></div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script src="/admin_main/js/chart/google/google-chart-loader.js"></script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.load('current', {'packages':['line']});
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {
        if ($("#pie-users").length > 0) {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['{{__('Current')}}', {{$advertisement_current_month}}],
                ['{{__('Last')}}', {{$advertisement_last_month}}],
            ]);
            var options = {
                title: '{{__('Advertisements by month')}}',
                is3D: true,
                width: '100%',
                height: 300,
                colors: ["#4b2b6d", "#2674ab"]
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie-users'));
            chart.draw(data, options);
        }

        if ($("#pie-advertisements").length > 0) {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['{{__('Current')}}', {{$current_profit_month_advertisement}}],
                ['{{__('Last')}}', {{$last_profit_month_advertisement}}]
            ]);
            var options = {
                title: '{{__('Profit by month')}}',
                is3D: true,
                width: '100%',
                height: 300,
                colors: ["#4b2b6d", "#2674ab"]
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie-advertisements'));
            chart.draw(data, options);
        }
    }
</script>
@endsection
