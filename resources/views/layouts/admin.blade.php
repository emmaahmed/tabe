<!DOCTYPE html>
<html lang="en" dir="rtl">
{{--dir="rtl"--}}
<head>
    <meta charset="utf-8" />
    <title> {{$title}} </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/admin_main/images/logo.png" type="image/x-icon">
    <link rel="shortcut icon" href="/admin_main/images/logo.png" type="image/x-icon">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/feather-icon.css">
    @yield('css')
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/style.css">
    <link id="color" rel="stylesheet" href="/admin_main/css/light-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="/admin_main/css/responsive.css">

    <link rel="stylesheet" href="/admin_main/css/royal-preload.css" />
    <link href="/admin_main/css/emoji.css" rel="stylesheet">
</head>

<body class="royal_preloader" main-theme-layout="rtl">
{{--main-theme-layout="rtl"--}}

<div class="page-wrapper">

    @include('admin.includes.topbar')

    <!-- Page Body Start-->
    <div class="page-body-wrapper">

        @include('admin.includes.left_sidemenu')

        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col">
                            <div class="page-header-left">
                                @yield('breadcrumb')
                            </div>
                            <!-- Bookmark Start-->
                            <div class="col">
                                <div class="bookmark pull-right">
                                    @yield('button')
                                </div>
                            </div>
                            <!-- Bookmark Ends-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            <div class="container-fluid">
                <div class="row">
                    @yield('content')
                </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>

        @include('admin.includes.footer')
    </div>
</div>

<!-- latest jquery-->
<script src="/admin_main/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap js-->
<script src="/admin_main/js/bootstrap/popper.min.js"></script>
<script src="/admin_main/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="/admin_main/js/icons/feather-icon/feather.min.js"></script>
<script src="/admin_main/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="/admin_main/js/sidebar-menu.js"></script>
<script src="/admin_main/js/config.js"></script>
<!-- Theme js-->
<script src="/admin_main/js/script.js"></script>
<!-- notify js -->
<script src="/admin_main/js/notify/bootstrap-notify.min.js"></script>
<script src="/admin_main/js/notify/notify-script.js"></script>

<script src="/admin_main/js/royal_preloader.min.js"></script>
<script type="text/javascript">
    window.jQuery = window.$ = jQuery;
    (function($) { "use strict";
        //Preloader
        Royal_Preloader.config({
            mode           : 'logo',
            logo           : '/admin_main/images/logo.png',
            logo_size      : [145, 75],
            showProgress   : true,
            showPercentage : true,
            text_colour: '#000000',
            background:  '#ffffff'
        });
    })(jQuery);
</script>
{{--<script src="/admin_main/js/jquery-3.2.1.min.js"></script>--}}

<script src="/admin_main/js/emoji/config.js"></script>
<script src="/admin_main/js/emoji/util.js"></script>
<script src="/admin_main/js/emoji/jquery.emojiarea.js"></script>
<script src="/admin_main/js/emoji/emoji-picker.js"></script>

@yield('script')

</body>
</html>
