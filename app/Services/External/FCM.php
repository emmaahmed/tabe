<?php

namespace App\Services\External;

class FCM
{
    public static function send($tokens, $data)
    {
         $server_key = config('services.fcm.server_key');

         $fields = [
             "registration_ids" => $tokens,
             "priority"         => 10,
             'data'             => $data,
             'notification'     => $data,
         ];

         $headers = [
             'accept: application/json',
             'Content-Type: application/json',
             'Authorization: key=' . $server_key
         ];

         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
         curl_setopt($ch, CURLOPT_POST, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
         $result = curl_exec($ch);
         if ($result === FALSE) {
             die('Curl failed: ' . curl_error($ch));
         }
         curl_close($ch);
         return $result;
    }
}
