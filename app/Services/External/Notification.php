<?php

namespace App\Services\External;

use App\Models\PersonalDeviceToken;
use App\Models\Notification as NotificationModel;
use App\Models\User;

class Notification
{
    public static function notify($type, $user_id, $action_by_id, $advertisement_id, $title, $body, $comment_id = null, $message_id = null)
    {
        User::whereId($user_id)->increment('notification_count');
        NotificationModel::create([
            'type'              =>  $type,
            'user_id'           =>  $user_id,
            'action_by_id'      =>  $action_by_id,
            'advertisement_id'  =>  $advertisement_id,
            'comment_id'        =>  $comment_id,
            'message_id'        =>  $message_id,
            'text'              =>  $body
        ]);

        $device_token = PersonalDeviceToken::where('user_id', $user_id)->pluck('device_token')->toArray();

        $data = [
            'type'              => $type,
            'user_id'           => $user_id,
            'action_by_id'      => $action_by_id,
            'advertisement_id'  => $advertisement_id,
            'comment_id'        => $comment_id,
            'message_id'        => $message_id,
            'title'             => $title,
            'body'              => $body,
            'sound'             => 'default',
        ];

        FCM::send($device_token, $data);
    }
}
