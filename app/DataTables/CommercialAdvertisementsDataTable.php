<?php

namespace App\DataTables;

use App\Models\CommercialAdvertisement;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CommercialAdvertisementsDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->with( 'country','category')
            ->addColumn('type', function ($data) {
                if($data->type=='commercial_banners')
                    return __('Commercial Advertisements');

                else

                return __('Home Banners');
            })
            ->addColumn('phone', function ($data) {
                return str_replace(' ', '', $data->country_code.$data->phone);
            })
            ->addColumn('image', function ($data) {
                return '<a  href="'.$data->image.'" target="_blank"><img src="'.$data->image.'" border="0" width="50" class="img-thumbnail" align="center"/></a>';
            })
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('Y-m-d');
            })
            ->addColumn('action', function ($data) {
                    return view('admin.commercial_advertisements.actions', compact('data'));

            })
            ->addColumn('period',function ($data){
                return $data->period . " " . __('Month');
            })
            ->addColumn('start_date',function ($data){
                return $data->start_date? Carbon::parse($data->start_date)->format('Y-m-d'):'-';
            })
            ->addColumn('end_date',function ($data){
                return $data->end_date?Carbon::parse($data->end_date)->format('Y-m-d'):'-';
            })
            ->addColumn('status', function ($data) {
                return view('admin.commercial_advertisements.statuses', compact('data'));
            })

            ->rawColumns(['image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CommercialAdvertisement $model)
    {
        return
            $model->newQuery()->orderBy('created_at','desc')
                ->with( 'country','category');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('commercial-advertisements-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->language('//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json')
            ->parameters([
                'scrollX' => true,
                'stateSave' => true,
                'order' => [
                    0, // here is the column number
                    'desc'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('type')
                ->title(__('Type'))
                ->className('text-center align-middle'),
            Column::make('image')
                ->title(__('Image'))
                ->className('text-center align-middle'),

            Column::make('company_name')
                ->title(__('Company Name'))
                ->className('text-center align-middle'),
            Column::make('email')
                ->title(__('Email'))
                ->className('text-center align-middle'),
            Column::make('phone')
                ->title(__('Phone'))
                ->className('text-center align-middle width-50'),


            Column::make('period')
                ->title(__('Period'))
                ->className('text-center align-middle'),

            Column::make('start_date')
                ->title(__('Start Date'))
                ->className('text-center align-middle'),

            Column::make('end_date')
                ->title(__('End Date'))
                ->className('text-center align-middle'),



            Column::make('created_at')
                ->title(__('Created at'))
                ->className('text-center align-middle'),

            Column::computed('status')
                ->title(__('Approved'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Action'))
                ->className('text-center align-middle'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'commercial-advertisements_' . date('YmdHis');
    }
}
