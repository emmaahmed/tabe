<?php

namespace App\DataTables;

use App\Models\Advertisement;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AdvertisementsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                return view('admin.advertisements.partials.actions', compact('data'));
            })
            ->addColumn('image', function ($data) {
                return '<img src="'.optional($data->images->first())->image.'" border="0" width="150" class="img-thumbnail" align="center"/>';
            })
            ->editColumn('created_at', function ($request) {
                if ($request->special == 1) {
                    return '-';
                }
                return $request->created_at->format('Y-m-d'); // human readable format
            })
            ->editColumn('ended_at', function ($request) {
                if ($request->special == 1) {
                    return '-';
                }
                return $request->ended_at?$request->ended_at->format('Y-m-d'):'-'; // human readable format
            })
            ->rawColumns(['image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Advertisement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Advertisement $model)
    {
        return $model->newQuery()
            ->groupBy('advertisements.id')
            ->with('user', 'category', 'images', 'attributes.parent',
                'sub_category_child.category', 'reports', 'userReports')->orderBy('created_at','desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('advertisements-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->language('//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json')
            ->parameters([
                'scrollX' => true,
                'stateSave' => true,
                'order' => [
                    0, // here is the column number
                    'desc'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')
                ->title('#')
                ->className('text-center align-middle'),

            Column::computed('image')
                ->title(__('Image'))
                ->className('text-center align-middle'),

            Column::make('user.name')
                ->title(__('User name'))
                ->className('text-center align-middle'),

//            Column::make('category.name')
//                ->title(__('Category'))
//                ->className('text-center align-middle'),

            Column::make('title')
                ->title(__('Title'))
                ->className('text-center align-middle'),

            Column::make('price')
                ->title(__('Price'))
                ->className('text-center align-middle'),

            Column::make('created_at')
                ->title(__('Created at'))
                ->className('text-center align-middle'),

            Column::make('ended_at')
                ->title(__('Ended at'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Actions'))
                ->className('text-center align-middle'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Advertisements_' . date('YmdHis');
    }
}
