<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->with('advertisements', 'country')
            ->addColumn('action', function ($data) {
                return view('admin.users.partials.actions', compact('data'));
            })
            ->addColumn('status', function ($data) {
                return view('admin.users.partials.statuses', compact('data'));
            })
            ->addColumn('advertisements_count', function ($data) {
                return $data->advertisements->count();
            })
            ->addColumn('image', function ($data) {
                return '<img src="'.$data->image.'" border="0" width="50" class="img-thumbnail" align="center"/>';
            })
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('Y-m-d'); // human readable format
            })
            ->rawColumns(['image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return
        $model->newQuery()
        ->where('account_type', 'client')
        ->with('advertisements', 'country')->orderBy('created_at','desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->language('//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json')
                    ->parameters([
                        'scrollX' => true,
                        'stateSave' => true,
                        'order' => [
                            0, // here is the column number
                            'desc'
                        ]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('image')
                ->title(__('Image'))
                ->className('text-center align-middle'),

            Column::make('name')
                ->title(__('Name'))
                ->className('text-center align-middle'),

            Column::make('email')
                ->title(__('Email'))
                ->className('text-center align-middle'),

            Column::make('phone')
                ->title(__('Phone'))
                ->className('text-center align-middle'),

            Column::make('country.name')
                ->title(__('Country'))
                ->className('text-center align-middle'),

            Column::computed('advertisements_count')
                ->title(__('Ads count'))
                ->className('text-center align-middle'),

            Column::make('created_at')
                ->title(__('Created at'))
                ->className('text-center align-middle'),

            Column::computed('status')
                ->title(__('Status'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Action'))
                ->className('text-center align-middle'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
