<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CompaniesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->with('advertisements', 'country')
            ->addColumn('action', function ($data) {
                return view('admin.users.partials.actions', compact('data'));
            })
            ->addColumn('status', function ($data) {
                return view('admin.users.partials.statuses', compact('data'));
            })
            ->addColumn('company_notification_flag', function ($data) {
                return view('admin.users.partials.notifications', compact('data'));
            })
            ->addColumn('category', function ($data) {
                return $data->category->name;
            })
            ->addColumn('advertisements_count', function ($data) {
                return $data->advertisements->count();
            })
            ->addColumn('company_license', function ($data) {
                return '<a target="_blank" href="'.$data->company_license.'"><img src="'.$data->company_license.'" border="0" width="50" class="img-thumbnail" align="center"/></a>';
            })
            ->addColumn('company_license', function ($data) {
                return '<img src="'.$data->company_license.'" border="0" width="50" class="img-thumbnail" align="center"/>';
            })
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('Y-m-d');
            })
            ->rawColumns(['image','company_license']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return
        $model->newQuery()
        ->where('account_type', 'company')
        ->with('advertisements', 'country', 'images','package')->orderBy('created_at','desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('companies-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->language('//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json')
                    ->parameters([
                        'scrollX' => true,
                        'stateSave' => true,
                        'order' => [
                            0, // here is the column number
                            'desc'
                        ]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('name')
                ->title(__('Name'))
                ->className('text-center align-middle'),


            Column::computed('country.name')
                ->title(__('Country'))
                ->className('text-center align-middle'),

            Column::make('category')
                ->title(__('Category'))
                ->className('text-center align-middle'),

            Column::computed('advertisements_count')
                ->title(__('Ads count'))
                ->className('text-center align-middle'),

            Column::computed('company_license')
                ->title(__('company_license'))
                ->className('text-center align-middle'),

            Column::make('created_at')
                ->title(__('Created at'))
                ->className('text-center align-middle'),

            Column::computed('status')
                ->title(__('Status'))
                ->className('text-center align-middle'),

            Column::computed('company_notification_flag')
                ->title(__('Notification'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Action'))
                ->className('text-center align-middle'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
