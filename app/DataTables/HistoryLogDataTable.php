<?php

namespace App\DataTables;

use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HistoryLogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                return view('admin.admins.history_log.partials.actions', compact('data'));
            })
            ->editColumn('log_name', function ($request) {
                return __($request->log_name);
            })
            ->editColumn('description', function ($request) {
                return __($request->description);
            })
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('Y-m-d H:i');
            })
            ->addColumn('causer_name', function ($data) {
                return optional($data->causer)->name;
            })
            ->rawColumns(['image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Activity $model)
    {
        return $model->newQuery()->with('causer', 'subject')
            ->where('causer_id', request()->route('admin')->id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admins-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->language('//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json')
                    ->orderBy(1)
                    ->parameters([
                        'scrollX'   => true,
                        'stateSave' => true,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')
                ->title(__('#'))
                ->className('text-center align-middle'),

            Column::make('log_name')
                ->title(__('Log name'))
                ->className('text-center align-middle'),

            Column::make('description')
                ->title(__('Action taken'))
                ->className('text-center align-middle'),

            Column::make('causer_name')
                ->title(__('Causer name'))
                ->className('text-center align-middle'),

            Column::make('created_at')
                ->title(__('Created at'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Action'))
                ->className('text-center align-middle'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admins_' . date('YmdHis');
    }
}
