<?php

namespace App\Imports;

use App\Models\AttributeCategory;
use Maatwebsite\Excel\Concerns\ToModel;

class AttributesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (request()->category_id) {
            $nameKey = 'key';
            $key = 'category_id';
            $value = request()->category_id;
        }else {
            $nameKey = 'value';
            $key = 'parent_id';
            $value = request()->parent_id;
        }

        $name=$row[0];
        if($name!=null) {
            return new AttributeCategory([
                $nameKey=> $name,
                $key    => $value
            ]);
        }
    }
}
