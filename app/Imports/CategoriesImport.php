<?php

namespace App\Imports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\ToModel;

class CategoriesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $name=$row[0];
        if($name!=null) {
            return new Category([
                'name' => $name,
                'parent_id' => request()->parent_id
            ]);
        }
    }
}
