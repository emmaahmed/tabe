<?php

namespace App\Providers;

use App\Models\Advertisement;
use App\Models\Banner;
use App\Models\Category;
use App\Models\CommercialAdvertisement;
use App\Models\ComplainSuggestion;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Notification;
use App\Models\Package;
use App\Models\ReportReason;
use App\Models\Setting;
use App\Models\User;
use App\Models\Welcome;
use App\Policies\AdvertisementPolicy;
use App\Policies\BannerPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\CommercialAdvertisementPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\ComplainSuggestionPolicy;
use App\Policies\ContactUsPolicy;
use App\Policies\CountryPolicy;
use App\Policies\NotificationPolicy;
use App\Policies\PackagePolicy;
use App\Policies\ReportReasonPolicy;
use App\Policies\SettingPolicy;
use App\Policies\UserPolicy;
use App\Policies\WelcomePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Admin'                     => 'App\Policies\AdminPolicy',
        'Dashboard'                 => 'App\Policies\DashboardPolicy',
        'Report'                    => 'App\Policies\ReportPolicy',
        User::class                 => UserPolicy::class,
        'Company'                   => CompanyPolicy::class,
        Category::class             => CategoryPolicy::class,
        Advertisement::class        => AdvertisementPolicy::class,
        Banner::class               => BannerPolicy::class,
        Country::class              => CountryPolicy::class,
        ComplainSuggestion::class   => ComplainSuggestionPolicy::class,
        ContactUs::class            => ContactUsPolicy::class,
        ReportReason::class         => ReportReasonPolicy::class,
        Notification::class         => NotificationPolicy::class,
        Welcome::class              => WelcomePolicy::class,
        Setting::class              => SettingPolicy::class,
        Package::class              => PackagePolicy::class,
        CommercialAdvertisement::class              => CommercialAdvertisementPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
