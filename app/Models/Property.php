<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
//    use HasFactory;


    protected $fillable = [
        'type',
        'parent_id',
        'property_type',
        'is_category_type',
        'name',
        'image'
    ];

    public function parent()
    {
        return $this->belongsTo(Property::class, 'parent_id');
    }

    public function values()
    {
        return $this->hasMany(Property::class, 'parent_id','id');
    }
    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('categories/attribute', 'public');
            $this->attributes['image'] = $name;
        }
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_property');
    }
    public function advertisements()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisement_property');
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
    public function scopePropertyType($query,$property_type)
    {
        return $query->where('property_type',$property_type);
    }

}
