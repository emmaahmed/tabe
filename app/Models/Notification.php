<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Notification extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'notification';

    protected static $logAttributes = ['title','text'];

    protected $dates = ['created_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'action_by_id');
    }
}
