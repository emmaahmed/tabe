<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $guarded = [];

    public function setFileAttribute($value)
    {
        if($value){
            $name = $value->store('users/stories', 'public');
            $this->attributes['file'] = $name;
        }
    }

    public function getFileAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
