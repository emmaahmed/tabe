<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AttributeCategory extends Model
{
    use LogsActivity;

    protected $guarded = [];

    const TYPE_SPECIFICATIONS = 1;
    const TYPE_FEATURES = 2;

    protected static $logName = 'attribute_category';

    protected static $logAttributes = [
        'key',
        'value',
        'attribute_type',
        'graphical_control_element',
        'image',
    ];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('categories/attribute', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function values()
    {
        return $this->hasMany(AttributeCategory::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(AttributeCategory::class, 'parent_id');
    }

    public function advertisements()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisement_attributes')
            ->withTimestamps();
    }
}
