<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Advertisement extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = [
        'title',
        'price',
        'negotiable',
        'address',
        'premium',
        'description',
        'ended_at',
    ];
    protected static $logName = 'advertisement';
    protected $guarded = [];
    protected $dates = ['created_at', 'ended_at'];

    public static function parent($id)
    {
        return AttributeCategory::find($id)->parent;
    }

    public function scopeVisible($query)
    {
        if($this->user()->first() && $this->user()->first()->account_type=='client') {
            return $query->whereDate('ended_at', '>', now()->format('Y-m-d'));
        }
    }



    public function companyAttributes()
    {
        return $this->hasMany(CompanyAttributes::class, 'advertise_id', 'id')->select('key', 'value', 'id', 'advertise_id', 'company_id');
    }

    public function sub_category_child()
    {
        return $this->belongsToMany(Category::class, 'advertisement_category',
            'advertisement_id', 'sub_category_child_id')
            ->withTimestamps();
    }

    public function getValueKeyText()
    {
        return $this->belongsToMany(AttributeCategory::class, 'advertisement_attributes')
            ->withPivot('text')
            ->where(function ($query) {
                $query->where(['advertisement_attributes.attribute_type' => 'value',
                    'advertisement_attributes.used_tag' => 0]);
            })
            ->orWhere(function ($query) {
                $query->where('advertisement_attributes.text', '!=', null)
                    ->where('advertisement_attributes.text', '!=', 'null');
            });
    }

    public function used_tag_value()
    {
//        return $this->attributes(1);
        return $this->properties(1);
    }
    public function tag(){
        return $this->belongsTo(AdvertisementType::class,'type_id','id');
    }

    public function attributes($used_tag = null)
    {
        return $this->belongsToMany(AttributeCategory::class, 'advertisement_attributes')
            ->withTimestamps()
            ->withPivot('used_tag', 'text')
            ->when(is_numeric($used_tag), function ($query) use ($used_tag) {
                $query->where('advertisement_attributes.used_tag', $used_tag);
            });
    }
    public function properties($used_tag = null)
    {
        return $this->belongsToMany(Property::class)
            ->withPivot('text')
            ->withTimestamps()
            ->when(is_numeric($used_tag), function ($query) use ($used_tag) {
                $query->where('is_category_type', $used_tag);
            });

    }

    public function images()
    {
        return $this->hasMany(AdvertisementImage::class);
    }

    public function wishlist()
    {
        return $this->belongsToMany(User::class, 'wishlists');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPriceAttribute($value)
    {
        return number_format((float)$value, 2, '.', '');
    }

    public function reports()
    {
        return $this->belongsToMany(ReportReason::class, 'reports', 'target_ads_id', 'reason_id')
            ->withPivot('reporter_id', 'admin_seen')
            ->withTimestamps();
    }

    public function userReports()
    {
        return $this->belongsToMany(User::class, 'reports', 'target_ads_id', 'reporter_id')
            ->withPivot('reporter_id')
            ->withTimestamps();
    }


}
