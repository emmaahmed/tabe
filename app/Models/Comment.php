<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Comment extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'comment';

    protected static $logAttributes = ['content'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
