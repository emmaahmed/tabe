<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ContactUs extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'contact_us';

    protected static $logAttributes = ['message'];
}
