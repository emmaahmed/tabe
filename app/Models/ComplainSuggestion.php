<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ComplainSuggestion extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'complaint_suggestion';

    protected static $logAttributes = ['message'];
}
