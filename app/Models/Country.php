<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Country extends Model
{
    use SoftDeletes, LogsActivity;

    protected $guarded = [];

    protected static $logName = 'country';

    protected static $logAttributes = [
        'name',
        'currency',
        'image',
    ];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('countries', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
