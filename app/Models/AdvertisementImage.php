<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AdvertisementImage extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'advertisement images';

    protected static $logAttributes = [ 'image' ];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('advertisements', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
