<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CommercialAdvertisement extends Model
{
    use  LogsActivity;

    protected static $logAttributes = [
        'approved',
        'category_id',
        'country_id',
        'type',
        'image',
        'period',
        'start_date',
        'end_date',
        'company_name',
        'email',
        'country_code',
        'phone',
        'description',
        'notes'
    ];
    protected static $logName = 'commercial_advertisement';
    protected $guarded = [];
    protected $dates = ['created_at', 'start_date','end_date'];


    public function scopeVisible($query)
    {
        return $query->whereDate('end_date', '>', now()->format('Y-m-d'));
    }


    public function scopeApproved($query)
    {
        return $query->where('approved',1);
    }



    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('commercial_advertisements', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

}
