<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Message extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected $dates = ['created_at'];

    protected static $logName = 'message';

    protected static $logAttributes = ['message'];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('messages', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function other_node()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function unread_count()
    {

        return $this->where(['receiver_id' => $this->receiver_id, 'read' => 0])->count();
    }
}
