<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CompanyImage extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'company_images';

    protected static $logAttributes = ['image'];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('companies/images', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
