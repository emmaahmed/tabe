<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminHistory extends Model
{
    protected $fillable = [
        'logged_in_at',
        'logged_out_at',
        'admin_id'
    ];

}
