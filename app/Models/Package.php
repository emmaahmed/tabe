<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Package extends Model
{
    use  LogsActivity;

    protected $guarded = [];

    protected static $logName = 'package';


    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('packages', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
//    public function setImageAttribute($value)
//    {
//        if($value){
//            $name = $value->store('categories', 'public');
//            $this->attributes['image'] = $name;
//        }
//    }
//
//    public function getImageAttribute($value)
//    {
//        if($value){
//            return asset('storage/' . $value);
//        }
//    }

    public function scopeActive($query)
    {
        $query->where('active', 1);
    }


}
