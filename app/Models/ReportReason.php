<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ReportReason extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'report_reason';

    protected static $logAttributes = ['reason'];
}
