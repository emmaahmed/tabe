<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Setting extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'setting';

    protected static $logAttributes = ['key', 'value', 'image'];

    const ABOUT_US = 'about_us';
    const PRIVACY_POLICY = 'privacy_policy';
    const TERMS_CONDITION = 'terms_condition';
    const HELP = 'help';
    const PREMIUM_COST = 'premium_cost';
    const ADVERTISEMENT_MODIFY_TIME = 'advertisement_modify_time';

//    public function setImageAttribute($value)
//    {
//        if($value){
//            $name = $value->store('settings', 'public');
//            $this->attributes['image'] = $name;
//        }
//    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function translations()
    {
        return $this->hasMany(Setting::class, 'translation_id');
    }

    public function translation()
    {
        return $this->hasOne(Setting::class, 'translation_id');
    }

    public function scopeGetValueByKey($query, $key)
    {
        return $query->where('key', $key);
    }
}
