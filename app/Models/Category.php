<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use SoftDeletes, LogsActivity;

    protected $guarded = [];

    protected static $logName = 'category';

    protected static $logAttributes = [
        'name',
        'title_name',
        'is_company',
        'order',
        'image',
    ];

    public function setImageAttribute($value)
    {
        if($value){
//            dd('el if');

            $name = $value->store('categories', 'public');
            $this->attributes['image'] = $name;
        }
        else {
//            dd('el else');

            $this->attributes['image'] = null;
        }


    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function scopeActive($query)
    {
        $query->where('active', 1);
    }

    public function scopeMainCategory($query)
    {
        return $query->whereNull(['parent_id', 'translation_id'])->active();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function subcategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function translations()
    {
        return $this->hasMany(Category::class, 'translation_id');
    }

    public function translation()
    {
        return $this->hasOne(Category::class, 'translation_id');
    }

    public function attributes()
    {
        return $this->hasMany(AttributeCategory::class);
    }

    public function tags()
    {
        return $this->hasMany(AttributeCategory::class)->where('used_tag', 1);
    }
    public function types()
    {
        return $this->hasMany(AdvertisementType::class);
    }

    public static function values($category_id)
    {
        return Category::join('attribute_categories', 'categories.id', '=', 'attribute_categories.category_id', 'inner')
            ->join('attribute_categories as child', 'attribute_categories.id', '=', 'child.parent_id', 'inner')
            ->where('child.used_tag', 1)
            ->where('attribute_categories.category_id', $category_id)
            ->select('child.*')
            ->get();
    }

    public function title()
    {
        return $this->belongsTo(Title::class);
    }
    public function properties()
    {
        return $this->belongsToMany(Property::class, 'category_property', 'category_id')
            ->withTimestamps();
    }

}
