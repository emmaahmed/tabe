<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function translations()
    {
        return $this->hasMany(Area::class, 'translation_id');
    }

    public function translation()
    {
        return $this->hasOne(Area::class, 'translation_id');
    }
}
