<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    public function other_node()
    {
        if ($this->user_one_id == auth()->user()->id) {
            return $this->belongsTo(User::class, 'user_two_id');
        }else{
            return $this->belongsTo(User::class, 'user_one_id');
        }
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->latest('id');
    }

    public function message()
    {
        return $this->hasOne(Message::class)->latest('id');
    }
    public function unread_count()
    {
//        dd($this->messages()->where(['receiver_id' => $this->message->receiver_id, 'read' => 0])->get());
        return $this->messages()->where(['receiver_id' => $this->message->receiver_id, 'read' => 0])->count();
    }

}
