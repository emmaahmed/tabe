<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes, LogsActivity;

    protected $guarded = [];

    protected static $logName = 'user';

    protected static $logAttributes = ['name', 'email', 'phone', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('users', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }

        return asset('storage/users/default.jpg');
    }
    public function setCompanyLicenseAttribute($value)
    {
//        dd($value);
        if($value){
            $name = $value->store('companies/company_license', 'public');
//            dd($name);
            $this->attributes['company_license'] = $name;
        }
    }

    public function getCompanyLicenseAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }

        return '';
    }

    public function scopeClient($query)
    {
        return $query->where('account_type', 'client');
    }
    public function last_device_token()
    {
        return $this->hasOne(PersonalDeviceToken::class)->latest();
    }
    public function firebaseToken()
    {

        return $this->hasMany(PersonalFirebaseToken::class,'tokenable_id','id');
    }
    public function scopeAdmin($query)
    {
        return $query->where('account_type', 'admin');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function getTypeAttribute()
    {
        return 'user';
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function hasPermission($permission)
    {
        return (bool) $this->permissions->where('slug', $permission)->count();
    }

    public function checkDeviceToken($device_token)
    {
        if($this->device_token()->where('device_token', $device_token)->first()) {
            $this->device_token()->update([
                'device_token'  => $device_token,
            ]);
        }else {
            $this->device_token()->create([
                'device_token'  => $device_token,
            ]);
        }
    }

    public function wishlist()
    {
        return $this->belongsToMany(Advertisement::class, 'wishlists');
    }

    public function device_token()
    {
        return $this->hasMany(PersonalDeviceToken::class);
    }

    // users that are followed by this user
    public function following()
    {
        return $this->belongsToMany(User::class, 'followings',
            'follower_id', 'following_id')
            ->withPivot('type');
    }

    // users that follow this user
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followings',
            'following_id', 'follower_id');
    }

    public function advertisements()
    {
        return $this->hasMany(Advertisement::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function report()
    {
        return $this->belongsToMany(ReportReason::class, 'reports', 'reporter_id', 'reason_id')
            ->withPivot('target_ads_id')
            ->withTimestamps();
    }

    public function messages()
    {
        return $this->belongsToMany(User::class, 'messages', 'sender_id', 'receiver_id');
    }

    public function advertisement_premium()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisement_premium')
            ->withPivot('cost')
            ->withTimestamps();
    }

    public function country()
    {
        return $this->belongsTo(Country::class)->withDefault(['name' => '']);
    }
    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault(['name' => '']);
    }

    public function images()
    {
        return $this->hasMany(CompanyImage::class, 'company_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
    public function history(){
        return $this->hasMany(AdminHistory::class,'admin_id','id');
    }

}
