<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Governorate extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function translations()
    {
        return $this->hasMany(Governorate::class, 'translation_id');
    }

    public function translation()
    {
        return $this->hasOne(Governorate::class, 'translation_id');
    }
}
