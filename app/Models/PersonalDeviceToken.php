<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalDeviceToken extends Model
{
    protected $guarded = [];
}
