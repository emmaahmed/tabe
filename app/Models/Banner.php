<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Banner extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logAttributes = [
        'link_type',
        'link_id',
        'country_id',
        'website_link',
        'image',
    ];

    protected static $logName = 'banner';

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('banners', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function setLargeImageAttribute($value)
    {
        if($value){
            $name = $value->store('banners', 'public');
            $this->attributes['large_image'] = $name;
        }
        else{
            $this->attributes['large_image']=null;
        }
    }

    public function getLargeImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
