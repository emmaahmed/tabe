<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function translations()
    {
        return $this->hasMany(City::class, 'translation_id');
    }

    public function translation()
    {
        return $this->hasOne(City::class, 'translation_id');
    }
}
