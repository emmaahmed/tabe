<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalFirebaseToken extends Model
{
    protected $table='personal_access_tokens';
    protected $hidden=['created_at','updated_at'];

}
