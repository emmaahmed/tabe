<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('companies', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function images()
    {
        return $this->hasMany(CompanyImage::class);
    }
}
