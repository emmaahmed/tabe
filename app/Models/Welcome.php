<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Welcome extends Model
{
    use LogsActivity;

    protected $guarded = [];

    protected static $logName = 'welcome';

    protected static $logAttributes = ['title', 'content', 'image'];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('welcome', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
