<?php

namespace App\Http\Resources\Categories;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributesCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return [
//            'id'                => $this->id,
//            'name'              => $this->key,
//            'attribute_type'    => $this->attribute_type,
//            'type'              => $this->graphical_control_element,
//            'used_tag'          => $this->used_tag,
//            'image'             => $this->image,
//            'values'            => ValuesAttributeCategoryResource::collection($this->values)
//        ];
        return [
            'id'                => $this->id,
            'key_name'              => $this->name,
            'type'              => $this->type,
//            'used_tag'          => $this->is_category_type,
            'image'             => $this->image,
            'values'            => ValuesAttributeCategoryResource::collection($this->values)
        ];
    }
}
