<?php

namespace App\Http\Resources\Categories;

use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'        => $this->id,
                'title'     => optional($this->category)->title_name,
                'name'      => $this->name,
                'image'     => $this->image,
                'attributes'    => $this->attributes->count()
//                $this->mergeWhen($request->tag == 1, [
//                    'tags'  => ValuesAttributeCategoryResource::collection(Category::values())
//                ]),
            ];
    }
}
