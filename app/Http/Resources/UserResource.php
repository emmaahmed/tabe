<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'account_type'  => $this->account_type,
            'category_id'  => $this->category_id!=null?(int)$this->category_id:0,
            'country_id'  => $this->country_id!=null?(int)$this->country_id:0,
            'name'          => $this->name,
            'email'         => $this->email,
            'phone'         => $this->phone,
            'hide_phone'    => $this->hide_phone==1?true:false,
            'hide_messages' => $this->hide_messages==1?true:false,
            'image'         => $this->image,
            'token'         => $this->token,
            'address'       => $this->address,
            'description'   => $this->description,
            'landline_phone'       => $this->landline_phone,
            'company_license'   => $this->company_license,
            'images'        => $this->images()->select('id', 'image')->get(),
            'is_follow'     => auth('sanctum')->user() ? auth('sanctum')->user()->following->contains($this->id) : false,
        ];
    }
}
