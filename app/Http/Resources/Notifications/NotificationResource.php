<?php

namespace App\Http\Resources\Notifications;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'              => $this->title,
            'text'              => $this->text,
            'type'              => $this->type,
            'user_id'           => $this->user_id,
            'action_by_id'      => $this->action_by_id,
            'advertisement_id'  => $this->advertisement_id,
            'comment_id'        => $this->comment_id,
            'message_id'        => $this->message_id,
            'created_at'        => $this->created_at->format('d F Y H:i'),
            'user'              => new UserResource($this->user)
        ];
    }
}
