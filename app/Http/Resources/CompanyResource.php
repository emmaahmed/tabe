<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'description'   => $this->description,
            'image'         => $this->image,
            'images'        => $this->images()->select('id', 'image')->get(),
            'is_follow'     => auth('sanctum')->user() ? auth('sanctum')->user()->following_company->contains($this->id) : false,
            'type'          => 'company'
        ];
    }
}
