<?php

namespace App\Http\Resources\Companies;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompaniesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'image'         => $this->image,
            'description'   => $this->description,
        ];
    }
}
