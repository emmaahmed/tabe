<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->sender_id != $request->user()->id) {
            $this->update([
                'read' => 1
            ]);
        }

        return [
            'id'            => $this->id,
            'message'       => $this->message,
            'image'         => $this->image,
            'time'          => $this->created_at->format('H:i'),
            'unread_count'  => $this->unread_count(),
            'user'          => new UserResource($this->other_node)
        ];
    }
}
