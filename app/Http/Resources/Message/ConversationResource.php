<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'message'       => $this->message->message != null ? $this->message->message : __('Photo sent.'),
            'receiver_id'       => $this->message->receiver_id,
            'sender_id'       => $this->message->sender_id,
            'conversation_id'       => $this->message->conversation_id,
            'user'          => new UserResource($this->other_node),
            'time'          => $this->message->created_at->format('H:i'),
            'unread_count'       => auth()->user()->id!=$this->message->sender_id?$this->unread_count():0,
        ];
    }
}
