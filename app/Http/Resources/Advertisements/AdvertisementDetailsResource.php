<?php

namespace App\Http\Resources\Advertisements;

use App\Http\Resources\Categories\SubCategoryResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\Comments\CommentsResource;
use App\Http\Resources\UserResource;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $modify_time = Setting::getValueByKey(Setting::ADVERTISEMENT_MODIFY_TIME)->first()->value;
        return [
            'id'                => $this->id,

            'images'            => $this->images,
            'title'             => $this->title,
            'view_count'        => $this->view_count,
            'created_at'        => Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans(),
            'description'       => $this->description,
            'price'             => $this->price,
            'vip'               => $this->premium,
            'negotiable'        => $this->negotiable,
            'is_new'            => $this->is_new,
            'contact_on'        => $this->contact_on,
            'category_id'       => $this->category_id,
            'category_name'     => $this->category?$this->category->name:'',
            'is_favorite'       => auth('sanctum')->user() ? $this->wishlist->contains(auth('sanctum')->user()) : false,
            'sub_categories'    => SubCategoryResource::collection($this->sub_category_child),
            'latitude'          => $this->latitude,
            'longitude'         => $this->longitude,
            'address'           => $this->address,
            'user_id'           => $this->user_id,
            'tag'               => new ValuesAttributeCategoryResource($this->tag),
            'user'              => new UserResource($this->user),
            'comments_count'    => $this->comments()->count(),
            'comments'          => CommentsResource::collection($this->comments->take(3)),
            'can_modify'    => $this->created_at->addHours($modify_time) > now() ? true : false,

            'companyAttributes'     => $this->whenLoaded('companyAttributes'),
            'specifications'            => ValuesAttributeAdvertisementResource::collection($this->properties()->PropertyType('specification')->get()),
            'features'            => ValuesAttributeAdvertisementResource::collection($this->properties()->PropertyType('feature')->get()),

        ];
    }
}
