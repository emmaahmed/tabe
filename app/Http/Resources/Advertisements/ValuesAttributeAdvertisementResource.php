<?php

namespace App\Http\Resources\Advertisements;

use App\Models\Advertisement;
use Illuminate\Http\Resources\Json\JsonResource;

class ValuesAttributeAdvertisementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        dd($this->id);
        return [
            'id'            => $this->id,
            'key_name'      => $this->parent_id==null ? $this->name : $this->parent->name,
            'text'          => $this->parent_id!=null ? $this->name  :$this->pivot->text,
//            'used_tag'      => optional($this->parent)->used_tag,
            'type'          => $this->parent_id==null ? $this->type:$this->parent->type,
//            'attribute_type'=> optional($this->parent)->attribute_type,
            'image'          => $this->parent_id==null ? $this->image:$this->parent->image,

//            'image'         => $this->image,
            'is_category_type'         => $this->is_category_type
        ];
    }
}
