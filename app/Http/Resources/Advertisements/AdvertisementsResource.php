<?php

namespace App\Http\Resources\Advertisements;

use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $modify_time = Setting::getValueByKey(Setting::ADVERTISEMENT_MODIFY_TIME)->first()->value;
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'price'         => $this->price,
            'vip'           => $this->premium,
            'is_favorite'   => $this->wishlist->contains(auth('sanctum')->user()),
            'image'         => count($this->images) ? $this->images->first()->image : '',
            'tag'           => new ValuesAttributeCategoryResource($this->tag),
            'created_at'    => $this->created_at->format('Y-m-d H:i'),
            'ended_at'      => $this->ended_at?$this->ended_at->format('Y-m-d H:i'):'',
            'modify_time'   => $modify_time,
            'can_modify'    => $this->created_at->addHours($modify_time) > now() ? true : false,
            'contact_on'    => $this->contact_on,
            'company'       => $this->company_id != null ? 1 : 0,
            'user'          => new UserResource($this->user)
        ];
    }
}
