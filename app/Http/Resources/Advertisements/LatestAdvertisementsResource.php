<?php

namespace App\Http\Resources\Advertisements;

use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;

class LatestAdvertisementsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'price'         => $this->price,
            'user_id'         => $this->user_id,
            'image'         => count($this->images) ? $this->images->first()->image : '',
            'vip'           => $this->premium,

        ];
    }
}
