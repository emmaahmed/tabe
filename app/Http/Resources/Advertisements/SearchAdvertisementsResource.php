<?php

namespace App\Http\Resources\Advertisements;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchAdvertisementsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'category_id'   => $this->category_id
        ];
    }
}
