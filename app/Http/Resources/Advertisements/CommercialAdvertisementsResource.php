<?php

namespace App\Http\Resources\Advertisements;
use Illuminate\Http\Resources\Json\JsonResource;

class CommercialAdvertisementsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'image'         => $this->image,
        ];
    }
}
