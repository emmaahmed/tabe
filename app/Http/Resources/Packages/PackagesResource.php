<?php

namespace App\Http\Resources\Packages;

use Illuminate\Http\Resources\Json\JsonResource;

class PackagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'title'     => $this->title,
            'desc'     => $this->desc,
            'period'     => $this->period,
            'advertisements_count'     => $this->advertisements_count,
            'price'     => $this->price,
            'image'     => $this->image,
        ];
    }
}
