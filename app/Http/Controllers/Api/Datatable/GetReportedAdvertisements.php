<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetReportedAdvertisements extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $advertisements =
        DB::table('reports')
        ->select([
            'reports.id',
            'advertisements.id as advertisement_id',
            'advertisements.title',
            'users.name as reported_name',
            'report_reasons.reason',
            'ReporterUser.name as reporter_name',
            'ReporterUser.phone as reporter_phone',
            'reports.created_at as date',
        ])
        ->leftJoin('advertisements', 'reports.target_ads_id', '=', 'advertisements.id')
        ->leftJoin('users', 'advertisements.user_id', '=', 'users.id')
        ->leftJoin('report_reasons', 'reports.reason_id', '=', 'report_reasons.id')
        ->leftJoin('users as ReporterUser', 'reports.reporter_id', '=', 'ReporterUser.id')
        ->whereNull('advertisements.deleted_at')
        ->where(function ($q) use ($search) {
            $q->where('advertisements.title', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('report_reasons.reason', 'like', '%'.$search.'%');
            $q->orWhere('ReporterUser.name', 'like', '%'.$search.'%');
            $q->orWhere('ReporterUser.phone', 'like', '%'.$search.'%');
            $q->orWhere('reports.created_at', 'like', '%'.$search.'%');
        })
        ->groupBy('reports.id')
        ->orderBy('advertisements.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $advertisements->total(),
            'recordsFiltered' => $advertisements->total(),
            'data' => $advertisements,
        );

        return $data;
    }
}
