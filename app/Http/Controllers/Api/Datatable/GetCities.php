<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCities extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $cities =
        DB::table('cities')
        ->select([
            'cities.id',
            'cities.name',
            'governorates.name as governorate_name',
            'governorates.id as governorate_id',
        ])
        ->leftJoin('governorates', 'cities.governorate_id', '=', 'governorates.id')
        ->whereNull(['cities.translation_id', 'cities.deleted_at'])
        ->where(function ($q) use ($search) {
            $q->where('cities.id', 'like', '%'.$search.'%');
            $q->orWhere('cities.name', 'like', '%'.$search.'%');
            $q->orWhere('governorates.name', 'like', '%'.$search.'%');
        })
        ->orderBy('cities.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $cities->total(),
            'recordsFiltered' => $cities->total(),
            'data' => $cities,
        );

        return $data;
    }
}
