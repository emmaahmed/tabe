<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetPackages extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $packages =
        DB::table('packages')
        ->select([
            'packages.id',
            'packages.title_ar',
            'packages.active',
            'packages.period',
            'packages.advertisements_count',
            'packages.price',
            'packages.desc_ar',
            DB::raw("concat('$server/storage/', packages.image) as image"),
        ])
        ->groupBy('packages.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $packages->total(),
            'recordsFiltered' => $packages->total(),
            'data' => $packages,
        );

        return $data;
    }
}
