<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetGovernorates extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $governorates =
        DB::table('governorates')
        ->select([
            'governorates.id',
            'governorates.name',
            'countries.id as country_id',
            'countries.name as country_name',
        ])
        ->leftJoin('countries', 'governorates.country_id', '=', 'countries.id')
        ->whereNull(['governorates.translation_id', 'governorates.deleted_at'])
        ->where(function ($q) use ($search) {
            $q->where('governorates.id', 'like', '%'.$search.'%');
            $q->orWhere('governorates.name', 'like', '%'.$search.'%');
            $q->orWhere('countries.name', 'like', '%'.$search.'%');
        })
        ->orderBy('governorates.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $governorates->total(),
            'recordsFiltered' => $governorates->total(),
            'data' => $governorates,
        );

        return $data;
    }
}
