<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCompanies extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $companies =
        DB::table('companies')
        ->select([
            'companies.id',
            'companies.name',
            'companies.latitude',
            'companies.longitude',
            'companies.address',
            'companies.description',
            DB::raw("concat('$server/storage/', companies.image) as image"),
            DB::raw("GROUP_CONCAT(company_images.image) as images"),
        ])
        ->leftJoin('company_images', 'companies.id', '=', 'company_images.company_id')
        ->where(function ($q) use ($search) {
            $q->where('companies.id', 'like', '%'.$search.'%');
            $q->orWhere('companies.name', 'like', '%'.$search.'%');
            $q->orWhere('companies.address', 'like', '%'.$search.'%');
        })
        ->orderBy('companies.id', $order[0]['dir'])
        ->groupBy('companies.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $companies->total(),
            'recordsFiltered' => $companies->total(),
            'data' => $companies,
        );

        return $data;
    }
}
