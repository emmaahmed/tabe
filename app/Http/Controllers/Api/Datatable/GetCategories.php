<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCategories extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $categories =
        DB::table('categories')
        ->select([
            'categories.id',
            'categories.name',
            'categories.active',
            'categories.title_name',
            'categories.is_company',
            'categories.order',
            DB::raw("concat('$server/storage/', categories.image) as image"),
        ])
        ->when($request->parent_id, function ($query) use ($request){
            $query->where('categories.parent_id', $request->parent_id);
        })
        ->when($request->parent_id == null, function ($query) use ($request){
            $query->whereNull('categories.parent_id');
        })
        ->whereNull(['categories.translation_id', 'categories.deleted_at'])
        ->where(function ($q) use ($search) {
            $q->where('categories.id', 'like', '%'.$search.'%');
            $q->orWhere('categories.name', 'like', '%'.$search.'%');
        })
        ->orderBy('categories.id', $order[0]['dir'])
        ->groupBy('categories.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $categories->total(),
            'recordsFiltered' => $categories->total(),
            'data' => $categories,
        );

        return $data;
    }
}
