<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCountries extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $countries =
        DB::table('countries')
        ->select([
            'countries.id',
            'countries.name',
            'countries.currency',
            DB::raw("concat('$server/storage/', countries.image) as image"),
        ])
        ->whereNull(['countries.translation_id', 'countries.deleted_at'])
        ->where(function ($q) use ($search) {
            $q->where('countries.id', 'like', '%'.$search.'%');
            $q->orWhere('countries.name', 'like', '%'.$search.'%');
            $q->orWhere('countries.currency', 'like', '%'.$search.'%');
        })
        ->orderBy('countries.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $countries->total(),
            'recordsFiltered' => $countries->total(),
            'data' => $countries,
        );

        return $data;
    }
}
