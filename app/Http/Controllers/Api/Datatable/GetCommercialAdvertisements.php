<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use App\Models\CommercialAdvertisement;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class GetCommercialAdvertisements extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $companies =
        DB::table('commercial_advertisements')
        ->select([
            'commercial_advertisements.id',
            'commercial_advertisements.type',
            'commercial_advertisements.company_name',
            'commercial_advertisements.email',
            'commercial_advertisements.country_code',
            'commercial_advertisements.phone',
            'commercial_advertisements.description',
            DB::raw("concat('$server/storage/', commercial_advertisements.image) as image"),
        ])
            ->leftJoin('categories', 'commercial_advertisements.category_id', '=', 'categories.id')
            ->leftJoin('countries', 'commercial_advertisements.country_id', '=', 'countries.id')
        ->where(function ($q) use ($search) {
            $q->where('commercial_advertisements.id', 'like', '%'.$search.'%');
            $q->orWhere('commercial_advertisements.name', 'like', '%'.$search.'%');
        })
        ->orderBy('commercial_advertisements.id', $order[0]['dir'])
        ->groupBy('commercial_advertisements.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $companies->total(),
            'recordsFiltered' => $companies->total(),
            'data' => $companies,
        );

        return $data;
    }
}

