<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetAdmins extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $admins =
        DB::table('users')
        ->select([
            'users.id',
            'users.name',
            'users.email',
            'users.phone',
            DB::raw('GROUP_CONCAT(permissions.slug) as permissions'),
            DB::raw("concat('$server/storage/', users.image) as image"),
        ])
        ->leftJoin('permission_user', 'users.id', '=', 'permission_user.user_id')
        ->leftJoin('permissions', 'permission_user.permission_id', '=', 'permissions.id')
        ->where('users.account_type', 'admin')
        ->whereNull('users.deleted_at')
        ->where('users.id', '!=', 1)
        ->where(function ($q) use ($search) {
            $q->where('users.id', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
        })
        ->orderBy('users.id', $order[0]['dir'])
        ->groupBy('users.id')
            ->orderBy('created_at','desc')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $admins->total(),
            'recordsFiltered' => $admins->total(),
            'data' => $admins,
        );

        return $data;
    }
}
