<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetSettings extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length);
        });

        $settings =
        DB::table('settings')
        ->select([
            'id',
            'key',
            'value',
        ])
        ->whereNull('translation_id')
        ->where('key', $request->type)
        ->where(function ($q) use ($search) {
            $q->where('value', 'like', '%'.$search.'%');
        })
            ->orderBy('created_at','desc')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $settings->total(),
            'recordsFiltered' => $settings->total(),
            'data' => $settings,
        );

        return $data;
    }
}
