<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetContactUs extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length);
        });

        $contact_us =
        DB::table('contact_us')
        ->select([
            'id',
            'name',
            'email',
            'phone',
            'message',
            'created_at',
        ])
        ->where(function ($q) use ($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
        })
        ->orderBy('id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $contact_us->total(),
            'recordsFiltered' => $contact_us->total(),
            'data' => $contact_us,
        );

        return $data;
    }
}
