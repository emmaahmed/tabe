<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetUsers extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $active = $search == __('Active') ? 1 :
            ( $search == __('Suspended') ? 2 : 0 );

        $server = url('');
        $users =
        DB::table('users')
        ->select([
            'users.id',
            'users.name',
            'users.email',
            'users.phone',
            'users.active',
            DB::raw("concat('$server/storage/', users.image) as image"),
        ])
        ->where('users.account_type', 'client')
        ->whereNull('users.deleted_at')
        ->where(function ($q) use ($search, $active) {
            $q->where('users.id', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            $q->orWhere('users.active', $active);
        })
        ->orderBy('users.id', $order[0]['dir'])
        ->groupBy('users.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $users->total(),
            'recordsFiltered' => $users->total(),
            'data' => $users,
        );

        return $data;
    }
}
