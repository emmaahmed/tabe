<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetWelcomes extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $welcomes =
        DB::table('welcomes')
        ->select([
            'id',
            'title',
            'content',
            DB::raw("concat('$server/storage/', welcomes.image) as image"),
        ])
        ->whereNull('welcomes.translation_id')
        ->where(function ($q) use ($search) {
            $q->where('id', 'like', '%'.$search.'%');
            $q->orWhere('title', 'like', '%'.$search.'%');
            $q->orWhere('content', 'like', '%'.$search.'%');
        })
        ->orderBy('id', 'desc')
        ->groupBy('id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $welcomes->total(),
            'recordsFiltered' => $welcomes->total(),
            'data' => $welcomes,
        );

        return $data;
    }
}
