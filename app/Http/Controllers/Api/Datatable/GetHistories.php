<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetHistories extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $histories =
        DB::table('admin_histories')
        ->select([
            'admin_histories.id',
            'admin_histories.logged_in_at',
            'admin_histories.logged_out_at'])
        ->when($request->admin_id, function ($query) use ($request){
            $query->where('admin_histories.admin_id', $request->admin_id);
        })
        ->paginate();

        $data = array(
            'recordsTotal' => $histories->total(),
            'recordsFiltered' => $histories->total(),
            'data' => $histories,
        );

        return $data;
    }
}
