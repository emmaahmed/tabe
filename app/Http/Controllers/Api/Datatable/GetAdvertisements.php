<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetAdvertisements extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $advertisements =
        DB::table('advertisements')
        ->select([
            'advertisements.id',
            'advertisements.category_id',
            'advertisements.title',
            'advertisements.price',
            'advertisements.negotiable',
            'advertisements.latitude',
            'advertisements.longitude',
            'advertisements.is_new',
            'advertisements.premium',
            'advertisements.description',
            'advertisements.ended_at',
            'categories.name as category_name',
            'users.name as user_name',
            DB::raw("concat('$server/storage/', advertisement_images.image) as image"),
            DB::raw("GROUP_CONCAT(advertisement_images.image) as images"),
        ])
        ->leftJoin('categories', 'advertisements.category_id', '=', 'categories.id')
        ->leftJoin('users', 'advertisements.user_id', '=', 'users.id')
        ->leftJoin('advertisement_images', 'advertisements.id', '=', 'advertisement_images.advertisement_id')
        ->whereNull('advertisements.deleted_at')
        ->where(function ($q) use ($search) {
            $q->where('advertisements.id', 'like', '%'.$search.'%');
            $q->orWhere('advertisements.title', 'like', '%'.$search.'%');
            $q->orWhere('advertisements.price', 'like', '%'.$search.'%');
            $q->orWhere('categories.name', 'like', '%'.$search.'%');
        })
        ->groupBy('advertisements.id')
        ->orderBy('advertisements.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $advertisements->total(),
            'recordsFiltered' => $advertisements->total(),
            'data' => $advertisements,
        );

        return $data;
    }
}
