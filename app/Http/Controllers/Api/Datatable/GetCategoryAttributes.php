<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCategoryAttributes extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $attributes =
        DB::table('attribute_categories')
        ->select([
            'ChildAttributes.id',
            'attribute_categories.id as parent_id',
            'attribute_categories.key',
            'attribute_categories.used_tag',
            'attribute_categories.attribute_type',
            'attribute_categories.graphical_control_element',
            'ChildAttributes.value',
            DB::raw("concat('$server/storage/', attribute_categories.image) as image"),
        ])
        ->leftJoin('attribute_categories as ChildAttributes', 'attribute_categories.id', 'ChildAttributes.parent_id')
        ->where('attribute_categories.category_id', $request->category_id)
        ->whereNull(['attribute_categories.translation_id', 'attribute_categories.parent_id'])
        ->where(function ($q) use ($search) {
            $q->where('attribute_categories.id', 'like', '%'.$search.'%');
            $q->orWhere('attribute_categories.key', 'like', '%'.$search.'%');
            $q->orWhere('attribute_categories.value', 'like', '%'.$search.'%');
        })
        ->orderBy('attribute_categories.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $attributes->total(),
            'recordsFiltered' => $attributes->total(),
            'data' => $attributes,
        );

        return $data;
    }
}
