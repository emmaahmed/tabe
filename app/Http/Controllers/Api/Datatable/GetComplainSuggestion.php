<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetComplainSuggestion extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length);
        });

        $complain_suggestions =
        DB::table('complain_suggestions')
        ->select([
            'id',
            'name',
            'email',
            'phone',
            'message',
            'created_at',
        ])
        ->where(function ($q) use ($search) {
            $q->where('name', 'like', '%'.$search.'%');
            $q->orWhere('email', 'like', '%'.$search.'%');
            $q->orWhere('phone', 'like', '%'.$search.'%');
        })
            ->orderBy('created_at','desc')

            ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $complain_suggestions->total(),
            'recordsFiltered' => $complain_suggestions->total(),
            'data' => $complain_suggestions,
        );

        return $data;
    }
}
