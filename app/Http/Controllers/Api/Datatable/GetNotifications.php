<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetNotifications extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length);
        });

        $notifications =
        DB::table('notifications')
        ->select([
            'notifications.id',
            'notifications.title',
            'notifications.text',
            DB::raw('(CASE WHEN notifications.is_all = 1 THEN "'.__('For Users').'" WHEN notifications.is_all = 2 THEN "'.__('For Companies').'" ELSE "'.__('For Country').'" END ) AS is_all'),


            'users.name',
            DB::raw('DATE_FORMAT(notifications.created_at, "%Y-%m-%d") as created_at'),
        ])
        ->leftJoin('users', 'notifications.user_id', '=', 'users.id')
        ->where('notifications.type', 'admin')
        ->where(function ($q) use ($search) {
            $q->where('notifications.text', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
        })
        ->orderBy('notifications.id', $order[0]['dir'])
        ->groupBy('notifications.id')
        ->paginate();


        $data = array(
            'draw' => $draw,
            'recordsTotal' => $notifications->total(),
            'recordsFiltered' => $notifications->total(),
            'data' => $notifications,
        );

        return $data;
    }
}
