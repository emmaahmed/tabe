<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetAdvertisementsComments extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $comments =
        DB::table('comments')
        ->select([
            'comments.id',
            'advertisements.id as advertisement_id',
            'advertisements.title',
            'users.name as user_name',
            'users.phone as user_phone',
            'comments.content',
            'comments.created_at',
        ])
        ->leftJoin('advertisements', 'comments.advertisement_id', '=', 'advertisements.id')
        ->leftJoin('users', 'advertisements.user_id', '=', 'users.id')
        ->whereNull('advertisements.deleted_at')
        ->where(function ($q) use ($search) {
            $q->where('advertisements.id', 'like', '%'.$search.'%');
            $q->where('advertisements.title', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
            $q->orWhere('comments.content', 'like', '%'.$search.'%');
            $q->orWhere('comments.created_at', 'like', '%'.$search.'%');
        })
        ->groupBy('comments.id')
//        ->orderBy('advertisements.id', $order[0]['dir'])
            ->orderBy('created_at','desc')

            ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $comments->total(),
            'recordsFiltered' => $comments->total(),
            'data' => $comments,
        );

        return $data;
    }
}
