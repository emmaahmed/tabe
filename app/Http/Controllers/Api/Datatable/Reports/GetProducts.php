<?php

namespace App\Http\Controllers\Api\Datatable\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetProducts extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $locale = app()->getLocale();
        $products =
        DB::table('products')
        ->select([
            'products.id',
            DB::raw('products.name_'.$locale.' as name'),
            DB::raw("concat('$server/storage/', products.image) as image"),
            'products.created_at',
            DB::raw('count(order_lines.id) as count_orders'),
            DB::raw('sum(order_lines.quantity) as quantity_products'),
            DB::raw('sum(order_lines.total_line_amount) as total_products'),
        ])
        ->leftJoin('order_lines', 'products.id', '=', 'order_lines.product_id')
        ->whereBetween('products.created_at', array($request->from_date." 00:00:00", $request->to_date." 23:59:59"))
        ->where(function ($q) use ($search) {
            $q->where('products.id', 'like', '%'.$search.'%');
            $q->orWhere('products.name_en', 'like', '%'.$search.'%');
            $q->orWhere('products.name_ar', 'like', '%'.$search.'%');
        })
        ->orderBy('products.id', 'desc')
        ->groupBy('products.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $products->total(),
            'recordsFiltered' => $products->total(),
            'data' => $products,
        );

        return $data;
    }
}
