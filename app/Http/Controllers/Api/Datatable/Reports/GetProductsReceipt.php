<?php

namespace App\Http\Controllers\Api\Datatable\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetProductsReceipt extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $locale = app()->getLocale();
        $products =
        DB::table('products')
        ->select([
            'products.id',
            DB::raw('products.name_'.$locale.' as name'),
            DB::raw("concat('$server/storage/', products.image) as image"),
            DB::raw('count(order_lines.id) as count_orders'),
            DB::raw('sum(order_lines.quantity) as quantity_products'),
            DB::raw('sum(order_lines.total_line_amount) as total_products'),
            'orders.receive_date',
        ])
        ->leftJoin('order_lines', 'products.id', '=', 'order_lines.product_id')
        ->leftJoin('orders', 'order_lines.order_id', '=', 'orders.id')
        ->whereBetween('orders.receive_date', array($request->from_date, $request->to_date))
        ->where(function ($q) use ($search) {
            $q->where('products.id', 'like', '%'.$search.'%');
            $q->orWhere('products.name_en', 'like', '%'.$search.'%');
            $q->orWhere('products.name_ar', 'like', '%'.$search.'%');
        })
        ->orderBy('products.id', 'desc')
        ->groupBy('products.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $products->total(),
            'recordsFiltered' => $products->total(),
            'data' => $products,
        );

        return $data;
    }
}
