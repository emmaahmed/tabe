<?php

namespace App\Http\Controllers\Api\Datatable\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetOrders extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $orders =
        DB::table('orders')
        ->select([
            'orders.id',
            DB::raw('concat("EC-",orders.id) as orderID'),
            'orders.receive_order',
            'orders.sub_total',
            'orders.shipping_cost',
            'orders.voucher_discount',
            'orders.total',
            'orders.created_at',
            'users.name as user_name',
            'users.phone as user_phone',
            'shipping_addresses.address1 as address',
            'delegate_orders.finished_at',
        ])
        ->leftJoin('users', 'orders.user_id', '=', 'users.id')
        ->leftJoin('shipping_addresses', 'orders.shipping_address_id', '=', 'shipping_addresses.id')
        ->leftJoin('delegate_orders', 'orders.id', '=', 'delegate_orders.order_id')
        ->leftJoin('transactions', 'orders.id', '=', 'transactions.order_id')
        ->where('transactions.status', 1)
        ->whereBetween('orders.created_at', array($request->from_date." 00:00:00", $request->to_date." 23:59:59"))
        ->when($request->is_delivered == 1, function ($query) use($request) {
            $query->where('is_delivered', $request->is_delivered);
        })
        ->where(function ($q) use ($search) {
            $q->where('orders.id', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
        })
        ->orderBy('orders.id', 'desc')
        ->groupBy('orders.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $orders->total(),
            'recordsFiltered' => $orders->total(),
            'data' => $orders,
        );

        return $data;
    }
}
