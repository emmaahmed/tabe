<?php

namespace App\Http\Controllers\Api\Datatable\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCustomers extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $customers =
        DB::table('users')
        ->select([
            'users.id',
            'users.name',
            'users.email',
            'users.phone',
            'users.address',
            'users.created_at',
            DB::raw('count(orders.id) as count_orders'),
            DB::raw('sum(orders.total) as total_payments'),
        ])
        ->leftJoin('orders', function($join) {
            $join->on('orders.user_id', '=', 'users.id')
                ->where('transactions.status', 1);
            $join->leftJoin('transactions', function($join) {
                $join->on('transactions.order_id', '=', 'orders.id');
            });
        })
        ->whereBetween('users.created_at', array($request->from_date." 00:00:00", $request->to_date." 23:59:59"))
        ->where(function ($q) use ($search) {
            $q->where('users.id', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
            $q->orWhere('users.email', 'like', '%'.$search.'%');
            $q->orWhere('users.phone', 'like', '%'.$search.'%');
        })
        ->orderBy('users.id', 'desc')
        ->groupBy('users.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $customers->total(),
            'recordsFiltered' => $customers->total(),
            'data' => $customers,
        );

        return $data;
    }
}
