<?php

namespace App\Http\Controllers\Api\Datatable\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetDelegates extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $delegates =
        DB::table('admins')
        ->select([
            'admins.id',
            'admins.name',
            DB::raw('count(delegate_orders.id) as count_orders'),
            'delegate_orders.created_at',
        ])
        ->leftJoin('delegate_orders', 'admins.id', '=', 'delegate_orders.owner_id')
        ->whereBetween('delegate_orders.finished_at', array($request->from_date." 00:00:00", $request->to_date." 23:59:59"))
        ->where('admins.type', 'delegate')
        ->where('delegate_orders.is_delivered', 1)
        ->where(function ($q) use ($search) {
            $q->where('admins.id', 'like', '%'.$search.'%');
            $q->orWhere('admins.name', 'like', '%'.$search.'%');
        })
        ->orderBy('admins.id', 'desc')
        ->groupBy('admins.id')
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $delegates->total(),
            'recordsFiltered' => $delegates->total(),
            'data' => $delegates,
        );

        return $data;
    }
}
