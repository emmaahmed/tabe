<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetAreas extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $areas =
        DB::table('areas')
        ->select([
            'areas.id',
            'areas.name',
            'cities.name as city_name',
            'cities.id as city_id',
        ])
        ->leftJoin('cities', 'areas.city_id', '=', 'cities.id')
        ->whereNull(['areas.translation_id', 'areas.deleted_at'])
        ->where(function ($q) use ($search) {
            $q->where('areas.id', 'like', '%'.$search.'%');
            $q->orWhere('areas.name', 'like', '%'.$search.'%');
            $q->orWhere('cities.name', 'like', '%'.$search.'%');
        })
        ->orderBy('areas.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $areas->total(),
            'recordsFiltered' => $areas->total(),
            'data' => $areas,
        );

        return $data;
    }
}
