<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetBanners extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];
        $order  = $request->get('order');

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $banners =
        DB::table('banners')
        ->select([
            'banners.id',
            'banners.link_type',
            'banners.link_id',
            'banners.country_id',
            'banners.website_link',
            DB::raw('CASE WHEN link_type = "company" THEN users.name
             ELSE website_link END as title'),
            DB::raw("concat('$server/storage/', banners.image) as image"),
            DB::raw("concat('$server/storage/', banners.large_image) as large_image"),
        ])
        ->leftJoin('users', 'banners.link_id', '=', 'users.id')
        ->where(function ($q) use ($search) {
            $q->where('banners.id', 'like', '%'.$search.'%');
            $q->orWhere('banners.link_type', 'like', '%'.$search.'%');
            $q->orWhere('users.name', 'like', '%'.$search.'%');
        })
        ->groupBy('banners.id')
        ->orderBy('banners.id', $order[0]['dir'])
        ->paginate();

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $banners->total(),
            'recordsFiltered' => $banners->total(),
            'data' => $banners,
        );

        return $data;
    }
}
