<?php

namespace App\Http\Controllers\Api\V100\Categories;

use App\Http\Controllers\Controller;
use App\Models\AdvertisementType;
use App\Services\RespondActive;
use \App\Http\Requests\Api\V100\Categories\GetAdvertisementTypes as GetAdvertisementTypesRequest;
class GetAdvertisementTypes extends Controller
{
    public function __invoke(GetAdvertisementTypesRequest $request)
    {
        $types=AdvertisementType::where('category_id',$request->category_id)->select('id','name as key_name')->get();



        return RespondActive::success('The action ran successfully!', $types);
    }

}
