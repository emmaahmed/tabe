<?php

namespace App\Http\Controllers\Api\V100\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Categories\GetCategoryRequest;
use App\Http\Resources\Categories\CategoriesResource;
use App\Models\Category;
use App\Services\RespondActive;

class GetCategories extends Controller
{
    public function __invoke(GetCategoryRequest $request)
    {
        switch($request->company){
            case 0:
                if($request->filter) {
                    $categories =
                        Category::mainCategory()
                            ->where('id', '!=', 1)
                            ->orderBy('is_company', 'asc')
                            ->get();
                }else{
                    $categories =
                        Category::mainCategory()
                            ->where('id', '!=', 1)
                            ->where('is_company', 0)
                            ->get();

                }
                break;
            case 1:
                $categories = Category::whereNotNull('parent_id')->where('is_company', 1)->get();
                break;
        }

        // if (optional(auth('sanctum')->user())->account_type == 'company') {
        //     $categories = Category::whereNull('translation_id')->where('is_company', 1)->get();
        // }

        $categories = CategoriesResource::collection($categories);

        return RespondActive::success('The action ran successfully!', $categories);
    }
}
