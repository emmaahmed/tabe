<?php

namespace App\Http\Controllers\Api\V100\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Categories\GetAttributesCategoryRequest;
use App\Http\Resources\Categories\AttributesCategoryResource;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Services\RespondActive;

class GetAttributesCategory extends Controller
{
    public function __invoke(GetAttributesCategoryRequest $request)
    {
//        $attributes =
//        AttributeCategory::where('category_id', $request->category_id)
//        ->with('values')
//        ->get();
        $category=Category::whereId($request->category_id)->first();

            $attributes=$category->properties()->with('values')->get();

        $attributes = AttributesCategoryResource::collection($attributes);



        return RespondActive::success('The action ran successfully!', $attributes);
    }
}
