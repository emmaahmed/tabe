<?php

namespace App\Http\Controllers\Api\V100\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Categories\GetSubCategoryRequest;
use App\Http\Resources\Categories\SubCategoryResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Models\Category;
use App\Services\RespondActive;

class GetSubCategory extends Controller
{
    public function __invoke(GetSubCategoryRequest $request)
    {
        $categories = Category::where('parent_id', $request->category_id)->active()->get();

        $data['title'] = Category::find($request->category_id)->title_name;

        $data['sub_categories'] = SubCategoryResource::collection($categories);

        return RespondActive::success('The action ran successfully!', $data);
    }
}
