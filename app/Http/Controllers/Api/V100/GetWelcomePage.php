<?php

namespace App\Http\Controllers\Api\V100;

use App\Http\Controllers\Controller;
use App\Models\Welcome;
use App\Services\RespondActive;

class GetWelcomePage extends Controller
{
    public function __invoke()
    {
        $welcome = Welcome::select('title', 'content', 'image')->get();

        return RespondActive::success('The action ran successfully!', $welcome);
    }
}
