<?php

namespace App\Http\Controllers\Api\V100\Messages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Messages\SetMessageRequest;
use App\Http\Resources\Message\MessageResource;
use App\Models\Conversation;
use App\Models\Message;
use App\Services\External\Notification;
use App\Services\RespondActive;

class SetMessage extends Controller
{
    public function __invoke(SetMessageRequest $request)
    {
        $conversationForUser =
            Conversation::where(['user_one_id' => auth()->id(), 'user_two_id' => $request->user_id])
                ->first();
        $conversationForCompany =
            Conversation::where(['user_one_id' => $request->user_id, 'user_two_id' => auth()->id()])
                ->first();

        if (!$conversationForUser && !$conversationForCompany) {
            $conversationForUser = Conversation::create([
                'user_one_id' => auth()->id(),
                'user_two_id' => $request->user_id
            ]);
        }

        $message = Message::create([
            'conversation_id'   => $conversationForUser?$conversationForUser->id:$conversationForCompany->id,
            'sender_id'         => $request->user()->id,
            'receiver_id'       => $request->user_id,
            'message'           => $request->message,
            'image'             => $request->image,
        ]);

        $message = new MessageResource($message);

        Notification::notify('message', $request->user_id, auth()->id(), null,
            __('Tabe'), __('New message') . $request->user()->name,
            null, $message->id);

        return RespondActive::success('The action ran successfully!', $message);
    }
}
