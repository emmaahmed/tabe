<?php

namespace App\Http\Controllers\Api\V100\Messages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Profile\GetProfileRequest;
use App\Http\Resources\Message\MessageResource;
use App\Models\Message;
use App\Services\RespondActive;

class GetChatMessages extends Controller
{
    public function __invoke(GetProfileRequest $request)
    {
//        $chat = Chat::where(['sender_id' => $request->user()->id, 'receiver_id' => $request->user_id])
//                    ->orWhere(['sender_id' => $request->user_id, 'receiver_id' => $request->user()->id])
//                    ->first();
        $messages = Message::where(function ($query) use ($request) {
            $query->where( ['sender_id' => $request->user()->id, 'receiver_id' => $request->user_id] );
        })
        ->orWhere(function ($query) use ($request) {
            $query->where( ['sender_id' => $request->user_id, 'receiver_id' => $request->user()->id] );
        });
//        $messages->update(['read'=>1]);


        $message = MessageResource::collection($messages->paginate())->response()->getData();

        return RespondActive::success('The action ran successfully!', $message);
    }
}
