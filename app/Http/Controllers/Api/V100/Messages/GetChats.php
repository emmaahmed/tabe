<?php

namespace App\Http\Controllers\Api\V100\Messages;

use App\Http\Controllers\Controller;
use App\Http\Resources\Message\ConversationResource;
use App\Http\Resources\Message\MessageResource;
use App\Models\Conversation;
use App\Models\Message;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class GetChats extends Controller
{
    public function __invoke(Request $request)
    {
      $conversations =
            Conversation::where(function ($query) use ($request) {
                $query->where( 'user_one_id', auth()->id())->orWhere( 'user_two_id', auth()->id() );
            })
                ->with('message')
                ->latest()
                ->paginate();

        $messages = ConversationResource::collection($conversations)->response()->getData();
//        dd(__('*'));

        return RespondActive::success('The action ran successfully!', $messages);
    }
}
