<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Helpers\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Auth\RegisterUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\Auth\VerificationService;
use App\Services\External\SMS;
use App\Services\RespondActive;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RegisterUser extends Controller
{
    public function __invoke(RegisterUserRequest $request)
    {
        $user = User::create([
            'account_type'  => $request->account_type,
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'image'         => $request->image,
            'country_id'    => $request->account_type=='user'?request()->header('country'):$request->country_id,
            'landline_phone'    => $request->landline_phone?$request->landline_phone:null,
            'company_license'    =>$request->company_license?$request->company_license:null,
            'category_id'    =>$request->category_id?$request->category_id:null,
            'address'    =>$request->address?$request->address:null,
            'description'    =>$request->description?$request->description:null,
        ]);
        if ($request->company_images) {
            foreach ($request->company_images as $image) {
                $user->images()->create([
                    'image' => $image
                ]);
            }
        }

        $user->checkDeviceToken($request->device_token);
        $this->verificationCode($user->id, 'verify', 'phone', $request->phone);

        return RespondActive::success('Registered Successfully,please activate.');
    }

    public static function verificationCode($user_id, $objective, $information_type, $information)
    {
        $activation_code = 1111;
//        $activation_code = rand(1000, 9999);

        $information_type=is_numeric($information)?'phone':'email';


        $data=[];
        $data['code']=$activation_code;
        $data['phone']=is_numeric($information)?$information:User::whereId($user_id)->first()->phone;

        try {

            SMS::send($data);

        } catch (\Exception $exception) {

//            return RespondActive::clientError(__('Wrong phone number!'));
        }



        DB::table('verification_codes')->updateOrInsert(
            [
                'user_id'           => $user_id,
                'objective'         => $objective,
                'information_type'  => $information_type,
                'information'       => $information,
            ],
            [
                'code'              => $activation_code,
                'expired_at'        => Carbon::now()->addHour()->toDateTimeString(),
                'created_at'        => now(),
            ]
        );
    }
}
