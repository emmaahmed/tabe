<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Http\Controllers\Controller;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class UserLogout extends Controller
{
    public function __invoke(Request $request)
    {
        //UPDATE DEVICE TOKEN
        $request->user()->currentAccessToken()->delete();
        $request->user()->device_token()->delete();
//        $request->user()->update([
//            'active' => 0
//        ]);

        return RespondActive::success('Logged out successfully.');
    }
}
