<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Http\Controllers\Controller;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class DeleteUser extends Controller
{
    public function __invoke(Request $request)
    {
        //UPDATE DEVICE TOKEN
       auth('sanctum')->user()->delete();

        return RespondActive::success('Deleted successfully.');
    }
}
