<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Auth\VerifyUserConfirmationCodeRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\VerificationCode;
use App\Services\RespondActive;

class VerifyUserConfirmationCode extends Controller
{
    public function __invoke(VerifyUserConfirmationCodeRequest $request)
    {
        $check =
        VerificationCode::where('information', $request->phone)
        ->where('code', $request->code)
        ->where('objective', $request->type)
        ->where('expired_at', '>', now())
        ->first();

        if (!$check) {
            return RespondActive::clientError('Invalid code.');
        }

        $user =
            $check->user;

        //CHECK USER EXISTENCE
        if($request->type == 'verify'){
            $user->update(['active' => 1]);
        }
        if($request->type == 'update'){
            if($request->email) {
                $user->update(
                    ['phone' => $request->phone,
                        'email'=>$request->email]
                );
            }
            else{
                $user->update(
                    ['phone' => $request->phone]
                );

            }

        }

        $user['token'] = $user->createToken('token')->plainTextToken;

        return RespondActive::success('Verified successfully.', new UserResource($user));
    }
}
