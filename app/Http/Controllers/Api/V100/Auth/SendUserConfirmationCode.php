<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Auth\SendUserConfirmationCodeRequest;
use App\Models\User;
use App\Services\RespondActive;

class SendUserConfirmationCode extends Controller
{
    public function __invoke(SendUserConfirmationCodeRequest $request)
    {
        $user = User::where(['phone' => $request->phone])->select('id','active')->first();
        if($user->active==2){
            return RespondActive::clientError(__('Your Account Has Been Suspended!'));

        }

        RegisterUser::verificationCode($user->id, $request->type, 'phone', $request->phone);

        return RespondActive::success('Code sent successfully.');
    }
}
