<?php

namespace App\Http\Controllers\Api\V100\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Auth\UserLoginRequest;
use App\Http\Resources\UserResource;
use App\Models\PersonalDeviceToken;
use App\Models\User;
use App\Services\External\FCM;
use App\Services\External\Notification;
use App\Services\RespondActive;
use Illuminate\Support\Facades\Hash;

class UserLogin extends Controller
{
    public function __invoke(UserLoginRequest $request)
    {
        //CHECK USER EXISTENCE
        $user = User::where('account_type', '!=', 'admin')
//            ->select('id','account_type' ,'active', 'name', 'email', 'phone', 'image', 'password','category_id','landline_phone','company_license','')
            ->when(is_numeric($request->phone_email), function ($query) use($request) {
                $query->where('phone', $request->phone_email);
            })
            ->when(filter_var($request->phone_email, FILTER_VALIDATE_EMAIL), function ($query) use($request) {
                $query->where('email', $request->phone_email);
            })
            ->first();

        //CHECK USER CREDENTIALS
        if (!$user || !Hash::check($request->password, $user->password)) {
            is_numeric($request->phone_email) ? $message = 'Wrong phone or password.' :
                $message = 'Wrong email or password.';

            return RespondActive::clientError($message);
        }
        $device_token = PersonalDeviceToken::where('user_id', $user->id)->pluck('device_token')->toArray();

        $data = [
            'type'  => 'deactivated',
            'title' => __('Tabe') ,
            'body'  =>  __('You logged in from another device'),
            'sound' => 'default',
            'user_id'           => $user->id,
            'action_by_id'      => 0,
            'advertisement_id'  => 0,
            'comment_id'        => 0,
            'message_id'        => 0,

        ];

        FCM::send($device_token, $data);


        $user->checkDeviceToken($request->device_token);


        //CHECK USER ACTIVATION
        $information=is_numeric($request->phone_email)?$user->phone:$user->email;
        if ($user->active == 0) {
            RegisterUser::verificationCode($user->id, 'verify', 'phone', $information);

            return RespondActive::clientNotActivated('Code sent to your phone, activate the account.');
        }

        //CHECK USER SUSPENDED
        if ($user->active == 2 ) {
            return RespondActive::clientError(__('Your Account Has Been Suspended!'));
        }

        $user['token'] = $user->createToken('token')->plainTextToken;

        return RespondActive::success('Logged in successfully.', new UserResource($user));
    }
}
