<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Settings\SetCallUsRequest;
use App\Models\ContactUs;
use App\Services\RespondActive;

class SetContactUs extends Controller
{
    public function __invoke(SetCallUsRequest $request)
    {
        $contact_us = ContactUs::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'message'   => $request->message,
        ]);

        return RespondActive::success('The action ran successfully!', $contact_us);
    }
}
