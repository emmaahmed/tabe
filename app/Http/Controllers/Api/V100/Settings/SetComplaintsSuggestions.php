<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Settings\SetComplainSuggestionRequest;
use App\Models\ComplainSuggestion;
use App\Services\RespondActive;

class SetComplaintsSuggestions extends Controller
{
    public function __invoke(SetComplainSuggestionRequest $request)
    {
        ComplainSuggestion::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'message'   => $request->message,
        ]);

        return RespondActive::success('The action ran successfully!');
    }
}
