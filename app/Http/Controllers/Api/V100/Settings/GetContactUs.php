<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Setting;
use App\Services\RespondActive;

class GetContactUs extends Controller
{
    public function __invoke()
    {
        $contact_us = Setting::where('key', 'email')->select('id', 'value as email')->first();

        $contact_us['whatsapp'] = Setting::where('key', 'whatsapp')->select('id', 'value as whatsapp')->first()->whatsapp;

        return RespondActive::success('The action ran successfully!', $contact_us);
    }
}
