<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Setting;
use App\Services\RespondActive;

class GetPrivacyPolicy extends Controller
{
    public function __invoke()
    {
        $privacy_policy = Setting::where('key', 'privacy_policy')->first();

        $privacy_policy = new SettingsResource($privacy_policy);

        return RespondActive::success('The action ran successfully!', $privacy_policy);
    }
}
