<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Setting;
use App\Services\RespondActive;

class GetAboutUs extends Controller
{
    public function __invoke()
    {
        $about_us = Setting::where('key', 'about_us')->first();

        $about_us = new SettingsResource($about_us);

        return RespondActive::success('The action ran successfully!', $about_us);
    }
}
