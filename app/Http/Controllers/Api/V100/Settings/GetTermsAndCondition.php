<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Setting;
use App\Services\RespondActive;

class GetTermsAndCondition extends Controller
{
    public function __invoke()
    {
        $terms_condition = Setting::where('key', 'terms_condition')->first();

        $terms_condition = new SettingsResource($terms_condition);

        return RespondActive::success('The action ran successfully!', $terms_condition);
    }
}
