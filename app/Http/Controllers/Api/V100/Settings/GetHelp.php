<?php

namespace App\Http\Controllers\Api\V100\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Setting;
use App\Services\RespondActive;

class GetHelp extends Controller
{
    public function __invoke()
    {
        $help = Setting::where('key', 'help')->first();

        $help = new SettingsResource($help);

        return RespondActive::success('The action ran successfully!', $help);
    }
}
