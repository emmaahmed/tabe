<?php

namespace App\Http\Controllers\Api\V100\Favorites;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetFavorite extends Controller
{
    public function __invoke(Request $request)
    {
        $advertisements = $request->user()->wishlist()->paginate();

        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
