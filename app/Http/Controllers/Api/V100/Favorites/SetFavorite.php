<?php

namespace App\Http\Controllers\Api\V100\Favorites;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Favorites\SetFavoriteRequest;
use App\Models\Advertisement;
use App\Services\RespondActive;

class SetFavorite extends Controller
{
    public function __invoke(SetFavoriteRequest $request)
    {
        $product = Advertisement::find($request->advertisement_id);

        if ($product->wishlist->contains($request->user())) {

            $product->wishlist()->detach($request->user());
            $data['is_favorite'] = false;
        }else {
            $product->wishlist()->attach($request->user());
            $data['is_favorite'] = true;
        }

        return RespondActive::success('The action ran successfully!', $data);
    }
}
