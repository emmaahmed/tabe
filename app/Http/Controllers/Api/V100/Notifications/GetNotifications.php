<?php

namespace App\Http\Controllers\Api\V100\Notifications;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notifications\NotificationResource;
use App\Models\Notification;
use App\Services\RespondActive;

class GetNotifications extends Controller
{
    public function __invoke()
    {
        $notifications = Notification::  where(function($query){
            $query->where('user_id', auth()->user()->id)
             ->when(auth()->user()->account_type=='client',function ($query){
              $query->orWhere('is_all', 1);

                                })
             ->when(auth()->user()->account_type=='company',function ($query){
                    $query->orWhere('is_all', 2);

                })
              ->orWhere('is_all', 3)->where('country_id',auth()->user()->country_id);


        })->latest('id')->where('created_at','>',auth()->user()->created_at)->paginate();

        auth()->user()->update(['notification_count'=>0]);


        $notifications = NotificationResource::collection($notifications)->response()->getData();

        return RespondActive::success('The action ran successfully!', $notifications);
    }
}
