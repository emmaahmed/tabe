<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetProfileFavorite extends Controller
{
    public function __invoke(Request $request)
    {
        $favorites = $request->user()->wishlist()->paginate();

        $advertisements = AdvertisementsResource::collection($favorites);

        return RespondActive::success('The action ran successfully!', $advertisements);
    }
}
