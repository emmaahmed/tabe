<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Companies\CompaniesResource;
use App\Http\Resources\UserResource;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetFollowingsAndFollowersCount extends Controller
{
    public function __invoke(Request $request)
    {
        $data['followings_count']=  $request->user()->following() ->count();
        $data['followers_count']=  $request->user()->followers()->count();;


        return RespondActive::success('The action ran successfully!', $data);
    }
}
