<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Companies\CompaniesResource;
use App\Http\Resources\UserResource;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetProfileFollowing extends Controller
{
    public function __invoke(Request $request)
    {
        $followings = $request->user()->following()->paginate();

        $followings = UserResource::collection($followings)->response()->getData();

        return RespondActive::success('The action ran successfully!', $followings);
    }
}
