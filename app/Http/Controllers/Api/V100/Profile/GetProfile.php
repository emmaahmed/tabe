<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Profile\GetProfileRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\Companies\CompaniesAdvertisement;
use App\Http\Resources\UserResource;
use App\Models\AttributeCategory;
use App\Models\User;
use App\Services\RespondActive;

class GetProfile extends Controller
{
//    public function __invoke(GetProfileRequest $request)
//    {
//        $user = User::with('advertisements')->find($request->user_id);
//
//        $data['user'] = new UserResource($user);
//
//        $advertisements = $user->advertisements()->paginate();
//
//        if ($advertisements->first() != null) {
//            $attribute =
//            AttributeCategory::where('category_id', $advertisements->first()->category_id)
//            ->where('used_tag', 1)
//            ->with('values')
//            ->first();
//
//            $data['tags'] = ValuesAttributeCategoryResource::collection($attribute->values);
//
//            $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();
//        }
//
//        return RespondActive::success('The action ran successfully!', $data);
//    }
    public function __invoke(GetProfileRequest $request)
    {
        $data['user'] = new UserResource(
            $user = User::with('advertisements')->find($request->user_id)
        );

        $data['tags'] = ValuesAttributeCategoryResource::collection(
            AttributeCategory::whereHas('parent', function ($query) use($user) {
                $query->where('category_id', $user->category_id);
            })->get()
        );

        $data['advertisements'] =
        AdvertisementsResource::collection(
            $user->advertisements()
            ->when($request->tag_id , function ($query) use ($request) {
                $query->whereHas('used_tag_value', function ($query) use ($request){
                    $query->where('attribute_category_id', $request->tag_id);
                });
            })
            ->paginate()
        )
        ->response()
        ->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
