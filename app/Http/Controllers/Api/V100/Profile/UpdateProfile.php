<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Api\V100\Auth\RegisterUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Profile\UpdateProfileRequest;
use App\Http\Resources\UserResource;
use App\Services\RespondActive;

class UpdateProfile extends Controller
{
    public function __invoke(UpdateProfileRequest $request)
    {
        $user=$request->user();
        if($user->phone != $request->phone || $user->email != $request->email){

            RegisterUser::verificationCode($user->id, 'update', 'phone', $request->phone);


        }
        $request->user()->update([
                'country_id' => $request->country_id
            ]);

            $request->user()->update([
                'name'          => $request->name,
//                'email'         => $request->email,
//                'phone'         => $request->phone,
                'hide_phone'    => $request->hide_phone ,
                'hide_messages' => $request->hide_messages ,
                'description'   => $request->description,
                'image'         => $request->image,
                'category_id'         => $request->category_id,
                'landline_phone'         => $request->landline_phone?$request->landline_phone:null,
                'address'         => $request->address?$request->address:null,

            ]);

        if($request->company_license){
            $request->user()->company_license=$request->company_license;
            $request->user()->save();
        }

        if ($request->company_images) {
            $request->user()->images()->delete();
            foreach ($request->company_images as $image) {
                $request->user()->images()->create([
                    'image' => $image
                ]);
            }
        }
        if ($request->delete_images && $request->delete_images==1) {
            $request->user()->images()->delete();
        }
       // dd($request->user());

        $request->user()['token'] = $request->user()->createToken($request->user()->device_token()->latest()->first())->plainTextToken;

        return RespondActive::success('The action ran successfully!', new UserResource($request->user()));
    }
}
