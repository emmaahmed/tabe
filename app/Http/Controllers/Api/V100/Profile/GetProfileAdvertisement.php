<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Message\MessageResource;
use App\Models\Advertisement;
use App\Models\Message;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetProfileAdvertisement extends Controller
{
    public function __invoke(Request $request, Message $message)
    {
        $advertisements =
        Advertisement::where([
            'user_id' => $request->user()->id,
        ])
        ->when($request->vip==0, function ($query) use ($request) {
            $query->where('premium', 0);
        })
        ->when($request->vip==1, function ($query) use ($request) {
            $query->where('premium', 1);
        })
        ->where('title', 'like', '%'.$request->search.'%')
        ->with('images')
        ->paginate();

        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
