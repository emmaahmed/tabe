<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Companies\CompaniesResource;
use App\Http\Resources\UserResource;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class GetProfileFollower extends Controller
{
    public function __invoke(Request $request)
    {
        $type = $request->company == 1 ? 'company' : 'client';

        $followers = $request->user()->followers()->where('account_type', $type)->paginate();

        $followers = UserResource::collection($followers)->response()->getData();

        return RespondActive::success('The action ran successfully!', $followers);
    }
}
