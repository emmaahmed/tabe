<?php

namespace App\Http\Controllers\Api\V100\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Profile\SetProfilePasswordRequest;
use App\Http\Resources\UserResource;
use App\Services\RespondActive;
use Illuminate\Support\Facades\Hash;

class SetProfilePassword extends Controller
{
    public function __invoke(SetProfilePasswordRequest $request)
    {
        if($request->old_password) {
            if(!Hash::check($request->old_password,$request->user()->password)){
                return RespondActive::clientError('Incorrect old password.');

            }
        }
        $request->user()->update([ 'password'  => bcrypt($request->password) ]);

        $user = $request->user();

//        $token = $user->createToken($user->device_token()->latest()->first())->plainTextToken;

        $user['token'] = $user->createToken('token')->plainTextToken;

        $user = new UserResource($user);

        return RespondActive::success('Updated successfully.', $user);
    }
}
