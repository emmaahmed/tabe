<?php

namespace App\Http\Controllers\Api\V100\Packages;

use App\Http\Controllers\Controller;
use App\Http\Resources\Packages\PackagesResource;
use App\Http\Resources\Settings\SettingsResource;
use App\Models\Package;
use App\Models\Setting;
use App\Services\RespondActive;

class GetPackages extends Controller
{
    public function __invoke()
    {
        return RespondActive::success('The action ran successfully!',  PackagesResource::collection(Package::
        select('id','title_'.app()->getLocale().' as title','desc_'.app()->getLocale().' as desc','period','image','advertisements_count','price' )->active()->get()));
    }
}
