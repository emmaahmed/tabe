<?php

namespace App\Http\Controllers\Api\V100\Countries;

use App\Http\Controllers\Controller;
use App\Http\Resources\Categories\CategoriesResource;
use App\Models\Category;
use App\Models\Country;
use App\Services\RespondActive;

class GetCountries extends Controller
{
    public function __invoke()
    {
        $countries = Country::select('id', 'name', 'currency', 'image')->get();

        return RespondActive::success('The action ran successfully!', $countries);
    }
}
