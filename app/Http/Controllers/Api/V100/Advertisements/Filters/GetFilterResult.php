<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Filters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Filters\GetFilterResultRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Categories\AttributesCategoryResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Models\Advertisement;
use App\Models\AdvertisementType;
use App\Models\AttributeCategory;
use App\Services\RespondActive;

class GetFilterResult extends Controller
{
    public function __invoke(GetFilterResultRequest $request)
    {
        $advertisements = Advertisement::
        visible()
            ->where('user_id', '!=', optional(auth('sanctum')->user())->id)

            ->where('premium', $request->vip)
            ->where('country_id', $request->header('country'))
            ->when($request->min_price && $request->max_price, function ($query) use ($request) {
                $query->whereBetween('price', [$request->min_price, $request->max_price]);
            })
//        ->when($request->value_ids, function ($query) use ($request){
//            $query->whereHas('sub_category_child', function ($query) use ($request){
//                $query->whereIn('sub_category_child_id', $request->value_ids);
//            });
//        })
            ->when($request->properties, function ($query) use ($request) {
                $query->whereHas('properties', function ($query) use ($request) {
                    foreach ($request->properties as $property) {
                        $text=isset($property['text'])?$property['text']:null;
                                 $query->where('advertisement_property.property_id', $property['id'])->orWhere(function($query)use($property,$text){
                                         $query->where('advertisement_property.property_id', $property['id'])->where('advertisement_property.text', 'like', '%' . $text . '%');
                                 });


//                        $query->when(isset($property['from']) && isset($property['to']), function ($query) use($property){
//                            $query->whereBetween('advertisement_property.text', [$property['from'], $property['to']]);
//
//                        });
                    }
                });
            })

            ->when($request->category_id && $request->category_id != 0 && !$request->sub_category_id, function ($query) use ($request) {
                $query->where('category_id', $request->category_id);
            })
//            ->when($request->max_price && $request->min_price, function ($query) use ($request) {
//                $query->where('price', '>=', $request->min_price)->where('price', '<=', $request->max_price);
//            })
            ->when($request->tag_id, function ($query) use ($request) {
                $query->where('type_id', $request->tag_id);
            })
            ->with('tag', 'images')
            ->orderBy('id', 'desc')

            ->paginate();
        $attribute =
            AttributeCategory::where('category_id', $request->category_id)
                ->where('used_tag', 1)
                ->with('values')
                ->first();

        if ($attribute) {
            $data['tags'] = ValuesAttributeCategoryResource::collection(AdvertisementType::where('category_id', $request->category_id)->get());
        }

        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
