<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Filters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Categories\GetAttributesCategoryRequest;
use App\Http\Resources\Categories\AttributesCategoryResource;
use App\Models\Advertisement;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Models\Property;
use App\Services\RespondActive;

class GetFilterDetails extends Controller
{
    public function __invoke(GetAttributesCategoryRequest $request)
    {
        if($request->category_id) {
            $category = Category::whereId($request->category_id)->first();
//        $attributes = AttributeCategory::where('category_id', $request->category_id)->get();

            $data['attributes'] = AttributesCategoryResource::collection($category->properties);
            $data['ads_count'] = Advertisement::where('category_id',$request->category_id)->count();

        }
        else{
            $data['attributes'] = AttributesCategoryResource::collection(Property::all());
            $data['ads_count'] = Advertisement::count();

        }
//        $minPrice=Advertisement::where('category_id',$request->category_id)->min('price');
//        $maxPrice=Advertisement::where('category_id',$request->category_id)->max('price');
//        $data['price_range']=['min_price'=>$minPrice,'max_price'=>$maxPrice];

        return RespondActive::success('The action ran successfully!', $data);
    }
}
