<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Filters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Filters\GetOrderByRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Models\Advertisement;
use App\Services\RespondActive;

class GetOrderBy extends Controller
{
    public function __invoke(GetOrderByRequest $request)
    {
        $advertisements = Advertisement::visible()
        ->where('category_id', $request->category_id)
        ->where('premium', $request->vip)
        ->when($request->order_by == 'newest', function ($query){
            $query->orderBy('id', 'desc');
        })
        ->when($request->order_by == 'oldest', function ($query){
            $query->orderBy('id', 'asc');
        })
        ->when($request->order_by == 'lowest', function ($query){
            $query->orderBy('price', 'asc');
        })
        ->when($request->order_by == 'highest', function ($query){
            $query->orderBy('price', 'desc');
        })
        ->paginate();

        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
