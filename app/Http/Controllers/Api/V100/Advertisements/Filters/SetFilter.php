<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Filters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Filters\SetFilterRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\Companies\CompaniesResource;
use App\Models\Advertisement;
use App\Models\AdvertisementType;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Models\Property;
use App\Models\User;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SetFilter extends Controller
{

    public function __invoke(SetFilterRequest $request)
    {
          Storage::disk('local')->put('setfilter', 'str: '.(json_encode($request->all())));
        $data = null;
        $properties=collect(
            $request->properties
        );

        if ($request->search_type == 'advertise') {
            $advertisements = Advertisement::query()
//                ->where([
////            'category_id'   => $request->category_id,
//                    'country_id'    => $request->header('country')
//                ])
                ->when(!isset($request->country_id),function ($query)use($request){
                    $query->where('country_id', $request->header('country'));
                })

                ->when($request->title, function ($query) use ($request) {
                    $query->where('title', 'like', '%' . $request->title . '%');
                })
                ->when($request->category_id &&  $request->category_id!=0 && !$request->sub_category_id, function ($query) use ($request) {
                    $query->where('category_id', $request->category_id);
                })
                ->when($request->sub_category_id, function ($query) use ($request) {
                    $query->where('category_id', $request->sub_category_id);
                })
                ->when($request->properties, function ($query) use ($request,$properties) {
                    $query->whereHas('properties', function ($query) use ($request,$properties) {
                        $query->whereIn('advertisement_property.property_id',$properties->pluck('d'));

                        foreach($properties as $property) {

                            $query->orWhere('advertisement_property.property_id',$property['id']);
                            if(isset($property['text'])&&$property['type']=='text'){
                                $query->where('text','LIKE', "%{$property['text']}%");
                            }
                            if(isset($property['text'])&&$property['type']=='integer'){
                                $query->whereBetween('text', [0,$property['text']]);
                            }
                        }

                    });
                })
                ->when($request->country_id, function ($query) use ($request) {
                    $query->whereIn('country_id', $request->country_id);
                })
                ->when($request->max_price!=null && $request->min_price!=null, function ($query) use ($request) {
                    $query->whereBetween('price',[$request->min_price,$request->max_price]);
                })
                ->orderBy('created_at','desc')
                ->paginate();

            if ($request->category_id!=0) {

                $data['tags'] = ValuesAttributeCategoryResource::collection(AdvertisementType::where('category_id',$request->category_id)->get());
            }
            else{
                $data['tags'] = ValuesAttributeCategoryResource::collection(AdvertisementType::all());


            }

            $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();

        } else {
            $data['companies'] = CompaniesResource::collection(User::where('account_type', 'company')
                ->when($request->category_id != 0, function ($query) use ($request) {
                    $query->where('category_id', $request->category_id);


                })
                ->when($request->title, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->title . '%');
                })
                ->when($request->country_id, function ($query) use ($request) {
                    $query->whereIn('country_id', $request->country_id);
                })
                ->paginate(30))->response()->getData();


        }
        return RespondActive::success('The action ran successfully!', $data);

    }
//    public function __invoke(SetFilterRequest $request)
//    {
//        $data=null;
//        if($request->search_type=='advertise') {
//            $advertisements =
//                Advertisement::where('category_id', $request->category_id)
//                    ->when($request->min_price && $request->max_price, function ($query) use ($request) {
//                        $query->whereBetween('price', [$request->min_price, $request->max_price]);
//                    })
//                    ->when($request->title, function ($query) use ($request) {
//                        $query->where('title', 'like', '%' . $request->title . '%');
//                    })
//                    ->when($request->country_id, function ($query) use ($request) {
//                        $query->where('country_id', $request->country_id);
//                    })
//                    ->when($request->search_key && count($request->search_key), function ($query) use ($request) {
//                        foreach ($request->search_key as $index => $key) {
//                            $attribute = AttributeCategory::where('key', $key)->first();
//                            if (is_array($request['attributes'][$index]) && count($request['attributes'][$index]) == 2) {
//                                $query->whereHas('attributes', function ($q) use ($attribute, $index, $request) {
//                                    $q->where('attribute_categories.parent_id', $attribute->id)->whereBetween('value', [$request['attributes'][$index][0], $request['attributes'][$index][1]]);
//                                });
//                            } elseif (is_array($request['attributes'][$index]) && count($request['attributes'][$index]) == 1) {
//                                $query->whereHas('attributes', function ($q) use ($attribute, $index, $request) {
//                                    $q->where('attribute_categories.parent_id', $attribute->id)->where('advertisement_attributes.text', $request['attributes'][$index][0]);
//                                });
//
//                            } else {
//                                $query->whereHas('attributes', function ($q) use ($attribute, $index, $request) {
//                                    $q->where('attribute_categories.parent_id', $attribute->id)->where('advertisement_attributes.attribute_category_id', $request['attributes'][$index]);
//                                });
//
//                            }
//                        }
//                    })
//                    ->paginate(30);
//
//            $attribute =
//                AttributeCategory::where('category_id', $request->category_id)
//                    ->where('used_tag', 1)
//                    ->with('values')
//                    ->first();
//            if ($attribute) {
//                $data['tags'] = ValuesAttributeCategoryResource::collection($attribute->values);
//            }
//
//            $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();
//
//        }
//        else{
//            $data['companies']=CompaniesResource::collection(User::where('account_type','company')
//                ->when($request->category_id!=0,function ($query)use($request){
//                        $query->where('category_id',$request->category_id);
//
//
//                })
//                ->when($request->title, function ($query) use ($request) {
//                    $query->where('name', 'like', '%' . $request->title . '%');
//                })
//                ->when($request->country_id, function ($query) use ($request) {
//                    $query->whereIn('country_id',$request->country_id);
//                })
//                ->paginate(30))->response()->getData();
//
//
//
//        }
//        return RespondActive::success('The action ran successfully!', $data);
//
//    }

}
