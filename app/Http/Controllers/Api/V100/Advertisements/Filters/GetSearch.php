<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Filters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Filters\GetSearchRequest;
use App\Http\Resources\Advertisements\SearchAdvertisementsResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\UserResource;
use App\Models\Advertisement;
use App\Models\Company;
use App\Models\User;
use App\Services\RespondActive;

class GetSearch extends Controller
{
    public function __invoke(GetSearchRequest $request)
    {
        if ($request->search_type == 'advertisement') {

            $advertisements =
            Advertisement::where('title', 'like', '%'.$request->search.'%')
            ->visible()
            ->take(10)
            ->get();

            $data = SearchAdvertisementsResource::collection($advertisements);
        }else {

            $user =
            User::where('name', 'like', '%'.$request->search.'%')
            ->client()
            ->active()
            ->paginate();

            $user = UserResource::collection($user);

            $company = Company::where('name', 'like', '%'.$request->search.'%')->get();

            $company = CompanyResource::collection($company);

            $data = $user->merge($company);
        }

        return RespondActive::success('The action ran successfully!', $data);
    }
}
