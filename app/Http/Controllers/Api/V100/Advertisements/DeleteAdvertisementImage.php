<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementDetailsResource;
use App\Http\Resources\Advertisements\ProductDetailsResource;
use App\Models\Advertisement;
use App\Models\AdvertisementImage;
use App\Models\Product;
use App\Services\RespondActive;

class DeleteAdvertisementImage extends Controller
{
    public function __invoke(AdvertisementImage $advertisementImage)
    {
        $advertisementImage->delete();

        return RespondActive::success('The action ran successfully!');
    }
}
