<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\GetAdvertisementsRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Models\Advertisement;
use App\Models\AdvertisementType;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Services\RespondActive;
use Illuminate\Support\Facades\Storage;

class GetAdvertisements extends Controller
{
    public function __invoke(GetAdvertisementsRequest $request)
    {
        Storage::disk('local')->put('getadvertisment', 'str: '.(json_encode($request->all()).$request->header('country')));

        $properties=collect(
            $request->properties
        );

        $advertisements =
        Advertisement::visible()
        ->where([
//            'category_id'   => $request->category_id,
            'country_id'    => $request->header('country')
        ])
//        ->where('user_id', '!=', optional(auth('sanctum')->user())->id)
            ->when($request->category_id &&  $request->category_id!=0 && !$request->sub_category_id, function ($query) use ($request) {
                $query->where('category_id', $request->category_id);
            })
        ->when($request->search, function ($query) use ($request) {
            $query->where('title', 'like', '%'.$request->search.'%');
        })
        ->when(!$request->search && $request->vip==1, function ($query) use ($request) {
            $query->where('premium', $request->vip);
        })
        ->when($request->tag_id , function ($query) use ($request) {
            $query->where('type_id',$request->tag_id);
        })
        ->when($request->order_by == 'newest', function ($query){
            $query->orderBy('id', 'desc');
        })
        ->when($request->order_by == 'oldest', function ($query){
            $query->orderBy('id', 'asc');
        })
        ->when($request->order_by == 'lowest', function ($query){
            $query->orderBy('price', 'asc');
        })
        ->when($request->order_by == 'highest', function ($query){
            $query->orderBy('price', 'desc');
        })
            ->when($request->title, function ($query) use ($request) {
                $query->where('title', 'like', '%' . $request->title . '%');
            })
            ->when($request->sub_category_id, function ($query) use ($request) {
                $query->where('category_id', $request->sub_category_id);
            })
            ->when($request->properties, function ($query) use ($request,$properties) {
                $query->whereHas('properties', function ($query) use ($request,$properties) {
                    $query->whereIn('advertisement_property.property_id',$properties->pluck('d'));

                    foreach($properties as $property) {

                        $query->orWhere('advertisement_property.property_id',$property['id']);
                        if(isset($property['text'])&&$property['type']=='text'){
                            $query->where('text','LIKE', "%{$property['text']}%");
                        }
                        if(isset($property['text'])&&$property['type']=='integer'){
                            $query->whereBetween('text', [0,$property['text']]);
                        }
                    }

                });
            })
            ->when($request->country_id, function ($query) use ($request) {
                $query->whereIn('country_id', $request->country_id);
            })
            ->when($request->max_price!=null && $request->min_price!=null, function ($query) use ($request) {
                $query->whereBetween('price',[$request->min_price,$request->max_price]);
            })

            ->with('tag', 'images')
            ->whereHas('user')
        ->orderBy('id', 'desc')
        ->paginate(30);

            $data['tags'] = ValuesAttributeCategoryResource::collection(AdvertisementType::where('category_id',$request->category_id)->get());


        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();


        return RespondActive::success('The action ran successfully!', $data);
    }
}
