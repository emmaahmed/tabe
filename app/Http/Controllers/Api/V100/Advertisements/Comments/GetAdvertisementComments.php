<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Comments;

use App\Http\Controllers\Controller;
use App\Http\Resources\Comments\CommentsResource;
use App\Models\Advertisement;
use App\Services\RespondActive;

class GetAdvertisementComments extends Controller
{
    public function __invoke(Advertisement $advertisement)
    {
        $advertisement->with('comments');

        $data['comments_count'] = $advertisement->comments()->count();

        $data['comments'] = CommentsResource::collection($advertisement->comments()->paginate())->response()->getData();

        return RespondActive::success('The action ran successfully!', $data);
    }
}
