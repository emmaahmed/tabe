<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Comments\SetCommentsRequest;
use App\Http\Resources\Comments\CommentsResource;
use App\Models\Advertisement;
use App\Models\Comment;
use App\Services\External\Notification;
use App\Services\RespondActive;

class SetAdvertisementComment extends Controller
{
    public function __invoke(SetCommentsRequest $request)
    {
        $comment = Comment::create($request->all() + ['user_id' => $request->user()->id]);

        $comment = new CommentsResource($comment);

        $advertisement = Advertisement::select('user_id')->find($request->advertisement_id);
        if($advertisement->user_id != auth()->id()) {

            Notification::notify('comment', $advertisement->user_id, $request->user()->id, $request->advertisement_id,
                __('Tabe'), __('New comment') . $request->user()->name, $comment->id);
        }

        return RespondActive::success('The action ran successfully!', $comment);
    }
}
