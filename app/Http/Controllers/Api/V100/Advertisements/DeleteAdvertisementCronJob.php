<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DeleteAdvertisementCronJob extends Controller
{
    public function __invoke(){
        Advertisement::where('ended_at','<',Carbon::now()->toDate())->delete();

    }
}
