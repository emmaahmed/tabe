<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Services\RespondActive;

class GetAdvertisementPremiumCost extends Controller
{
    public function __invoke()
    {
        $premium_cost = (float)Setting::where('key', 'premium_cost')->select('value as cost')->first()->cost;

        return RespondActive::success('The action ran successfully!', $premium_cost);
    }
}
