<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Reports\SetReportsRequest;
use App\Services\RespondActive;

class SetAdvertisementReport extends Controller
{
    public function __invoke(SetReportsRequest $request)
    {
        $request->user()->report()->attach($request->reason_id, [
            'target_ads_id' => $request->advertisement_id
        ]);

        return RespondActive::success('The action ran successfully!');
    }
}
