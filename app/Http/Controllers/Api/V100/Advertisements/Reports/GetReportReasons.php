<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Reports;

use App\Http\Controllers\Controller;
use App\Models\ReportReason;
use App\Services\RespondActive;

class GetReportReasons extends Controller
{
    public function __invoke()
    {
        $reasons = ReportReason::select('id', 'reason')->get();

        return RespondActive::success('The action ran successfully!', $reasons);
    }
}
