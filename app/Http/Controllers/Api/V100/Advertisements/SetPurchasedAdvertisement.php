<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementDetailsResource;
use App\Http\Resources\Advertisements\ProductDetailsResource;
use App\Models\Advertisement;
use App\Models\Product;
use App\Services\RespondActive;

class SetPurchasedAdvertisement extends Controller
{
    public function __invoke(Advertisement $advertisement)
    {
        $advertisement->where('user_id', request()->user()->id)->update([
            'purchased' => 1
        ]);

        return RespondActive::success('The action ran successfully!');
    }
}
