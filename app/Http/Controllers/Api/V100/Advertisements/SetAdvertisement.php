<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\SetAdvertisementRequest;
use App\Models\Advertisement;
use App\Models\Category;
use App\Models\CompanyAttributes;
use App\Models\Setting;
use App\Services\RespondActive;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class SetAdvertisement extends Controller
{
    public function __invoke(SetAdvertisementRequest $request)
    {
//        Storage::disk('local')->put('eslam', 'str: '.$request);

        if($request->premium==1) {
            $expireTime = Setting::where('key', 'vip_advertisement_expire_time_in_weeks')->first()->value;
        }
        else{
            $expireTime = Setting::where('key', 'advertisement_expire_time_in_weeks')->first()->value;

        }
        $advertisement = Advertisement::create(Arr::except($request->validated(),['sub_category_child_ids','properties','images']) + [
            'user_id'           => $request->user()->id,
            'country_id'        => $request->header('country'),
            'ended_at'          => auth()->user()->account_type=='client'?Carbon::now()->addWeeks($expireTime)->toDateTimeString():null,
        ]);

        if(auth()->user()->account_type!='company') {

//        if ($request->premium == 1) {
//            Category::find($request->category_id)->update([
//                'has_premium_ads' => 1
//            ]);
//        }

            $advertisement->sub_category_child()->attach($request->sub_category_child_ids);
//


            foreach ($request->properties as $key=>$value){

                $advertisement->properties()->attach([$request->properties[$key]['id'] => ['text' => isset($request->properties[$key]['text'])?$request->properties[$key]['text']:null]]);

            }

       }
        else{

//            return $request->properties;

            $advertisement->update(['category_id'=>auth()->user()->category_id]);
            if(isset($request->properties)) {
                foreach ($request->properties as $key => $value) {
                    CompanyAttributes::create([
                        'company_id' => auth()->user()->id,
                        'advertise_id' => $advertisement->id,
                        'key' => $request->properties[$key]['text'],
                        'value' => $request->properties[$key]['value']
                    ]);
                }
            }
        }

        foreach ($request->images as $image) {
            $advertisement->images()->create([
                'image' => $image
            ]);
        }
//

        return RespondActive::success('The action ran successfully!');
    }
}
