<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\SetAdvertisementSpecialRequest;
use App\Models\Advertisement;
use App\Models\Category;
use App\Models\Setting;
use App\Services\RespondActive;

class SetAdvertisementVip extends Controller
{
    public function __invoke(SetAdvertisementSpecialRequest $request)
    {
        $cost = Setting::where('key', 'premium_cost')->first();

        $request->user()->advertisement_premium()->attach($request->advertisement_ids, [
            'cost' => $cost->value
        ]);

        foreach ($request->advertisement_ids as $advertisement_id) {
            $advertisement = Advertisement::find($advertisement_id);

            $advertisement->update([ 'premium' => 1 ]);

//            Category::find($advertisement->category_id)->update([
//                'has_premium_ads' => 1
//            ]);
        }

        return RespondActive::success('The action ran successfully!');
    }
}
