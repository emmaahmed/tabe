<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Commercial;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Commercial\SetCommercialAdvertisementRequest;
use App\Models\CommercialAdvertisement;
use App\Services\RespondActive;
use Carbon\Carbon;

class SetCommercialAdvertisement extends Controller
{
    public function __invoke(SetCommercialAdvertisementRequest $request)
    {
        CommercialAdvertisement::create($request->validated() +
            [
                'category_id'=>$request->category_id,
                'country_id'        => $request->header('country'),
            ]);
//

        return RespondActive::success('The action ran successfully!');
    }
}
