<?php

namespace App\Http\Controllers\Api\V100\Advertisements\Commercial;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\Commercial\SetCommercialAdvertisementRequest;
use App\Http\Resources\Advertisements\CommercialAdvertisementsResource;
use App\Models\CommercialAdvertisement;
use App\Services\RespondActive;
use Carbon\Carbon;

class GetCommercialAdvertisements extends Controller
{
    public function __invoke()
    {
        $data=CommercialAdvertisementsResource::collection($advertisements=CommercialAdvertisement::visible()->approved()->orderBy('created_at','desc')->paginate(30))->response()->getData();


        return RespondActive::success('The action ran successfully!',$data);
    }
}
