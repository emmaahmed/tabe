<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\GetTextSearchResultRequest;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Categories\ValuesAttributeCategoryResource;
use App\Http\Resources\UserResource;
use App\Models\Advertisement;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Models\User;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class SearchAdvertisement extends Controller
{
    public function searchResults(GetTextSearchResultRequest $request){


         $advertisements=Advertisement::visible()
             ->where([
//            'category_id'   => $request->category_id,
                 'country_id'    => $request->header('country')
             ])

             ->where('title','like','%'.$request->text.'%')
             ->select('id','title')
//             ->with('used_tag_value', 'images')
            ->orderBy('id', 'desc')
            ->get();
//         $advertisements->put('type','advertisement');
         $companies = User::where('account_type', 'company')
             ->where([
//            'category_id'   => $request->category_id,
                 'country_id'    => $request->header('country')
             ])

             ->where('name','like','%'.$request->text.'%')
             ->select('id','name as title','account_type')

             ->get();
//        $companies->put('type','company');
        $data=$advertisements->merge($companies);


        return RespondActive::success('The action ran successfully!', $data);



    }
    public function searchAdvertisementByText(GetTextSearchResultRequest $request){
         $advertisements=Advertisement::visible()->where('title','like','%'.$request->text.'%')->with('used_tag_value', 'images')
          ->orderBy('id', 'desc')
          ->paginate(30);
        $attribute =
            AttributeCategory::where('category_id', $request->category_id)
                ->where('used_tag', 1)
                ->with('values')
                ->first();

        if ($attribute) {
            $data['tags'] = ValuesAttributeCategoryResource::collection($attribute->values);
        }

        $data['advertisements'] = AdvertisementsResource::collection($advertisements)->response()->getData();


        return RespondActive::success('The action ran successfully!', $data);


    }

}
