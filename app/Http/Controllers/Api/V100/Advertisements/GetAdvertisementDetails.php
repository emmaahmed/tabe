<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementDetailsResource;
use App\Models\Advertisement;
use App\Services\RespondActive;

class GetAdvertisementDetails extends Controller
{
    public function __invoke($id)
    {

        $advertisement =
        Advertisement::with('category', 'wishlist', 'sub_category_child','properties', 'used_tag_value', 'user', 'comments','companyAttributes')
        ->findOrFail($id);

        $advertisement->where('user_id', '!=', optional(auth('sanctum')->user())->id)->update([
            'view_count' => $advertisement->view_count + 1
        ]);

        $advertisement = new AdvertisementDetailsResource($advertisement);

        return RespondActive::success('The action ran successfully!', $advertisement);
    }
}
