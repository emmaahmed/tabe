<?php

namespace App\Http\Controllers\Api\V100\Advertisements;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Advertisements\SetAdvertisementRequest;
use App\Models\Advertisement;
use App\Models\Category;
use App\Models\CompanyAttributes;
use App\Models\Setting;
use App\Services\RespondActive;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UpdateAdvertisement extends Controller
{
    public function __invoke(SetAdvertisementRequest $request, Advertisement $advertisement)
    {
        $modify_time = Setting::getValueByKey(Setting::ADVERTISEMENT_MODIFY_TIME)->first()->value;

        if (auth()->user()->account_type=='user' && $advertisement->created_at->addHours($modify_time) < now()) {
            return RespondActive::clientError('The advertisement cannot be modified after the time allowed for modification!');
        }

        $advertisement->update(Arr::except($request->validated(), ['sub_category_child_ids', 'properties', 'images']) + [
                'user_id' => $request->user()->id,
                'country_id' => $request->header('country'),
            ]);


        if (auth()->user()->account_type != 'company') {
            $advertisement->sub_category_child()->sync($request->sub_category_child_ids);


            $advertisement->properties()->detach();

            foreach ($request->properties as $key => $value) {
//                dd($key, $value);
                $advertisement->properties()->attach([$request->properties[$key]['id'] => ['text' => isset($request->properties[$key]['text'])?$request->properties[$key]['text']:null]]);

            }
        } else {
            $advertisement->companyAttributes()->delete();


            foreach ($request->properties as $key => $value) {
                CompanyAttributes::create([
                    'company_id' => auth()->user()->id,
                    'advertise_id' => $advertisement->id,
                    'key' => $request->properties[$key]['text'],
                    'value' => $request->properties[$key]['value']
                ]);
            }

        }
//        $advertisement->images()->delete();
if(isset($request->images) && count($request->images)) {
    foreach ($request->images as $image) {

        $advertisement->images()->create([
            'image' => $image
        ]);
    }

}
        return RespondActive::success('The action ran successfully!');
    }
}
