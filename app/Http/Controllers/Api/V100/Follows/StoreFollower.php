<?php

namespace App\Http\Controllers\Api\V100\Follows;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Follows\FollowRequest;
use App\Models\User;
use App\Services\External\Notification;
use App\Services\RespondActive;

class StoreFollower extends Controller
{
    public function __invoke(FollowRequest $request)
    {
        $check = $request->user()->following->contains($request->user_id);

        if( $check ) {
            $request->user()->following()->detach($request->user_id);
            \App\Models\Notification::where(['user_id'=>$request->user_id,'action_by_id'=>auth()->id()])->delete();

        }else {
            $user = User::find($request->user_id);

            $request->user()->following()->attach($request->user_id, [
                'type' => $user->account_type
            ]);

            Notification::notify('follow', $request->user_id, $request->user()->id,
                null, __('Tabe'), __('New follower').$request->user()->name);
        }

        return RespondActive::success('The action ran successfully!');
    }
}
