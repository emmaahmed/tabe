<?php

namespace App\Http\Controllers\Api\V100\Home;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisements\AdvertisementsResource;
use App\Http\Resources\Advertisements\LatestAdvertisementsResource;
use App\Http\Resources\Categories\CategoriesResource;
use App\Http\Resources\UserResource;
use App\Models\Advertisement;
use App\Models\Banner;
use App\Models\Category;
use App\Services\RespondActive;
use Illuminate\Support\Facades\Request;

class GetHomeDetails extends Controller
{
    public function __invoke()
    {
        if (auth('sanctum')->user()) {
            auth('sanctum')->user()->update([
                'country_id'    => request()->header('country'),
            ]);
        }

        $data['slider'] =
        Banner::select('id', 'link_type', 'link_id', 'image', 'large_image', 'website_link')
        ->where(function ($query) {
            $query->where('country_id', request()->header('country'))
                  ->orWhereNull('country_id');
        })
        ->get();

        $categories = Category::mainCategory()->get();

        $data['categories'] = CategoriesResource::collection($categories);

        $data['notification_count'] =
            auth('sanctum')->user()? auth('sanctum')->user()->notification_count:0;

        $data['advertisements'] = LatestAdvertisementsResource::collection(Advertisement::visible()
            ->where(['country_id'    => getallheaders()['Country']
        ])
            ->with('category', 'wishlist', 'user')->latest()->take(10)->get());

        return RespondActive::success('The action ran successfully!', $data);
    }
}
