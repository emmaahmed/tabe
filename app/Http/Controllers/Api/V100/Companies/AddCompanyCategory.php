<?php

namespace App\Http\Controllers\Api\V100\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Companies\AddCompanyCategoryRequest;
use App\Models\User;
use App\Services\RespondActive;
use Illuminate\Http\Request;

class AddCompanyCategory extends Controller
{
    public function __invoke(AddCompanyCategoryRequest $request)
    {
        $user= auth()->user();
        $user->update([
            'category_id'=>$request->category_id
        ]);

        return RespondActive::success('Category Updated Successfully.');
    }
}
