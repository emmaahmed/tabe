<?php

namespace App\Http\Controllers\Api\V100\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V100\Companies\GetCompanyRequest;
use App\Http\Resources\UserResource;
use App\Models\AttributeCategory;
use App\Models\User;
use App\Services\RespondActive;

class GetCompanies extends Controller
{
    public function __invoke(GetCompanyRequest $request)
    {
//        $attribute = AttributeCategory::with('advertisements')->find($request->attribute_category_id);
//
//        $pluck_companies_id = $attribute->advertisements->pluck('user_id');


        $companies = UserResource::collection( User::where('account_type', 'company')
            ->where([
                'category_id'=>$request->category_id,
            'country_id'=>$request->header('country')
            ])->get());

        return RespondActive::success('The action ran successfully!', $companies);
    }
}
