<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Categories\StoreCategoryRequest;
use App\Http\Requests\Admin\Categories\UpdateCategoryRequest;
use App\Imports\CategoriesImport;
use App\Models\AdvertisementType;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class CategoryController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Category::class);

        return view('admin.categories.index');
    }

    public function show(Category $category)
    {
        $this->authorize('view', Category::class);

        return view('admin.categories.show', compact('category'));
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->authorize('create', Category::class);

        Category::create([
            'name'          => $request->name,
            'parent_id'     => $request->parent_id,
            'title_name'    => $request->title_name,
            'is_company'    => $request->is_company ? 1 : 0,
            'image'         => $request->image,
            'language'      => 'ar',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $this->authorize('update', Category::class);

        $category->update([
            'name'          => $request->edit_name,
            'title_name'    => $request->edit_title_name,
            'order'         => $request->order,
            'is_company'    => $request->edit_is_company ? 1 : 0,
        ]);

        if ($request->hasFile('edit_image')) {
            $category->update([ 'image' => $request->edit_image ]);

            if(!empty($category->image)) {
                Storage::delete($category->image);
            }
        }
        if (!$request->hasFile('edit_image')) {


            if(!empty($category->image)) {
                Storage::delete($category->image);
            }
            $category->update(['image' => null]);

        }


        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Category $category)
    {
        $this->authorize('delete', Category::class);

        Storage::delete($category->image);

        $category->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function excel(Request $request)
    {
        Excel::import(new CategoriesImport, $request->file);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function changeStatus($categoryId)
    {
        $status = request()->status == "true" ? 1 : 0;

        Category::find($categoryId)->update([ 'active' => $status ]);
    }

//    public function destroyAll(Request $request)
//    {
//        dd($request->all());
//    }
    public function getAdvertisementsTypes(Request $request){
        $category=Category::whereId($request->category_id)->first();
        $tags= $category->types;
        return view('admin.categories.properties.types', compact('tags'));

    }
    public function addAdvertisementsTypes(Request $request){
        AdvertisementType::create([
            'name'=>$request->name,
            'category_id'=>$request->category_id
        ]);
        return back()->with('success', __('The action ran successfully!'));

    }
    public function updateAdvertisementsTypes(Request $request,$id){
        $tag=AdvertisementType::whereId($id)->first();
        $tag->update([
            'name'=>$request->name,

        ]);
        return back()->with('success', __('The action ran successfully!'));

    }
    public function deleteAdvertisementsTypes($id){
        $this->authorize('delete', Category::class);

        $tag=AdvertisementType::whereId($id)->first();
        $tag->delete();

        return back()->with('success', __('The action ran successfully!'));

    }

}
