<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ComplainSuggestion;
use App\Models\ContactUs;

class ComplainSuggestionController extends Controller
{
    public function index()
    {
        return view('admin.settings.complain_suggestions.index');
    }

    public function destroy($id)
    {
        ComplainSuggestion::findOrFail($id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
