<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function viewLoginPage()
    {
        if(auth()->user())
        {
            return redirect()->route('admin.dashboard.index');
        }

        return view('admin.login.index');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'name'      => ['required', 'string', 'max:190'],
            'password'  => ['required', 'min:6']
        ]);

        if(auth()->attempt(['name' => $request->name, 'password' => $request->password, 'active' => 1,
            'account_type' => 'admin'], false)) {

            $user = auth()->user()->permissions;

            if($user->isEmpty()) {
                auth()->logout();
                return abort('403');
            }

            $slug = $user->first()->slug;
            $route_name = substr($slug, strpos($slug, "_") + 1);
            auth()->user()->history()->create(['logged_in_at'=>Carbon::now()]);

            return redirect()->route('admin.'.$route_name.'.index');
        }

        return back()->with('error', 'Invalid Credentials');
    }

    public function logout()
    {
        $history=auth()->user()->history()->where('logged_out_at',null)->first();
        if($history ){
            $history->update(['logged_out_at'=>Carbon::now()]);
        }

        auth()->logout();
        return redirect()->route('admin.login');
    }
}
