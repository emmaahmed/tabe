<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function orders()
    {
        return view('admin.reports.orders');
    }

    public function products()
    {
        return view('admin.reports.products');
    }

    public function productsReceipt()
    {
        return view('admin.reports.products_receipt');
    }

    public function delegates()
    {
        return view('admin.reports.delegates');
    }

    public function customers()
    {
        return view('admin.reports.customers');
    }
}
