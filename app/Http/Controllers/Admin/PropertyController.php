<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Property\PropertyRequest;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = Property::with('translation', 'hubFiles')->whereNull('parent_id')->paginate();

        return view('Admin.pages.properties.index', compact('properties'));
    }

    public function create()
    {
        return view('Admin.pages.properties.create');
    }

    public function store(PropertyRequest $request)
    {
        Property::create([
            'ar'                => ['name' => $request->name_arabic],
            'en'                => ['name' => $request->name_english],
            'parent_id'         => $request->parent_id,
            'type'              => $request->type,
            'is_category_type'  => $request->boolean('is_category_type'),
        ]);

        $redirect = $request->parent_id ? back() : redirect()->route('admin.properties.index');

        return $redirect->with('success', __('Category added successfully'));
    }

    public function show($id)
    {
        $properties = Property::with('translation', 'hubFiles')->where('parent_id', $id)->paginate();

        return view('Admin.pages.properties.show', compact('id', 'properties'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
