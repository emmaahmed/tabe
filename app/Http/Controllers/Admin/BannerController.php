<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banners\StoreBannerRequest;
use App\Http\Requests\Admin\Banners\UpdateBannerRequest;
use App\Models\Advertisement;
use App\Models\Banner;
use App\Models\Country;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Banner::class);

        $companies = User::whereAccountType('company')->get();

        $countries = Country::get(['id', 'name']);

        return view('admin.banners.index', compact('companies', 'countries'));
    }

    public function store(StoreBannerRequest $request)
    {
        $this->authorize('create', Banner::class);

        Banner::create([
            'link_type'     => $request->link_type,
            'country_id'    => $request->country_id == 'all' ? Null : $request->country_id,
            'link_id'       => $request->company_id,
            'website_link'  => $request->website_link,
            'image'         => $request->image,
            'large_image'   => $request->large_image?$request->large_image:null,
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateBannerRequest $request, Banner $banner)
    {
        $this->authorize('update', Banner::class);

        $banner->update([
            'link_type'     => $request->edit_link_type,
            'country_id'    => $request->edit_country_id  == 'all' ? Null : $request->edit_country_id,
            'link_id'       => $request->edit_company_id,
            'website_link'  => $request->edit_website_link,
        ]);

        if ($request->hasFile('edit_image')) {
            $banner->update([ 'image' => $request->edit_image ]);

            if(!empty($banner->image)) {
                Storage::delete($banner->image);
            }
        }

        if ($request->hasFile('edit_large_image')) {
            $banner->update([ 'large_image' => $request->edit_large_image ]);

            if(!empty($banner->large_image)) {
                Storage::delete($banner->large_image);
            }
        }
        if (!$request->hasFile('edit_large_image')) {

            $banner->update(['large_image' => null]);

            if(!empty($banner->large_image)) {
                Storage::delete($banner->large_image);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Banner $banner)
    {
        $this->authorize('update', Banner::class);

        Storage::delete($banner->image);

        $banner->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
