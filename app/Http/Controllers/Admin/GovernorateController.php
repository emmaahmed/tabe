<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Governorates\StoreGovernoratesRequest;
use App\Http\Requests\Admin\Governorates\UpdateGovernoratesRequest;
use App\Models\Country;
use App\Models\Governorate;

class GovernorateController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Governorate::class);

        $countries = Country::whereNull('translation_id')->get(['id', 'name']);

        return view('admin.locations.governorates.index', compact('countries'));
    }

    public function store(StoreGovernoratesRequest $request)
    {
        $this->authorize('create', Governorate::class);

        Governorate::create([
            'name'          => $request->name,
            'country_id'    => $request->country_id,
            'language'      => 'ar',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateGovernoratesRequest $request, Governorate $governorate)
    {
        $this->authorize('update', Governorate::class);

        $governorate->update([
            'name'          => $request->edit_name,
            'country_id'    => $request->edit_country_id,
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', Governorate::class);

        $governorate = Governorate::findOrFail($id);

        $governorate->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
