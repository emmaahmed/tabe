<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Areas\StoreAreaRequest;
use App\Http\Requests\Admin\Areas\UpdateAreaRequest;
use App\Models\Area;
use App\Models\City;

class AreaController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Area::class);

        $cities = City::whereNull('translation_id')->get();

        return view('admin.locations.areas.index', compact('cities'));
    }

    public function store(StoreAreaRequest $request)
    {
        $this->authorize('create', Area::class);

        Area::create([
            'name'      => $request->name,
            'city_id'   => $request->city_id,
            'language'  => 'ar',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateAreaRequest $request, Area $area)
    {
        $this->authorize('update', Area::class);

        $area->update([
            'name'    => $request->edit_name,
            'city_id' => $request->edit_city_id,
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', Area::class);

        $area = Area::findOrFail($id);

        $area->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
