<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AdvertisementsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Advertisements\UpdateAdvertisementRequest;
use App\Models\Advertisement;
use App\Models\AdvertisementImage;
use App\Models\Category;
use App\Models\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdvertisementController extends Controller
{
    public function index(AdvertisementsDataTable $dataTable)
    {
        $this->authorize('viewAny', Advertisement::class);

        $categories = Category::whereNull(['translation_id', 'parent_id'])->where('id', '!=', 1)->get();

        $countries = Country::whereNull('translation_id')->get();

        return $dataTable->render('admin.advertisements.index', compact('categories', 'countries'));
    }

    public function update(UpdateAdvertisementRequest $request, Advertisement $advertisement)
    {
        $this->authorize('update', Advertisement::class);

        $advertisement->update([
            'title'             => $request->edit_title,
            'category_id'       => $request->edit_category_id,
            'price'             => $request->edit_price,
            'country_id'        => $request->edit_country_id,
            'negotiable'        => $request->edit_negotiable ? 1 : 0,
            'premium'           => $request->edit_premium ? 1 : 0,
            'description'       => $request->edit_description,
            'ended_at'          => $request->edit_ended_at,
        ]);

        if ($request->hasFile('edit_images')) {

            $advertisement->images()->delete();

            foreach ($request->edit_images as $image) {
                $advertisement->images()->create([
                    'image' => $image
                ]);

                Storage::delete($advertisement->image);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Advertisement $advertisement)
    {
        $this->authorize('delete', Advertisement::class);

        $advertisement->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroyReportedAdvertisements($id)
    {
        $this->authorize('delete', Advertisement::class);

        DB::table('reports')->where('id', $id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function deleteImage($imageId)
    {
        AdvertisementImage::findOrFail($imageId)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function reportedAdvertisementsUpdate($id)
    {
        Advertisement::find($id)->reports()->update([
            'admin_seen' => 1
        ]);
    }
}
