<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\Setting;

class ContactUsController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Setting::class);

        return view('admin.settings.contact_us.index');
    }

    public function destroy($id)
    {
        $this->authorize('delete', Setting::class);

        ContactUs::findOrFail($id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
