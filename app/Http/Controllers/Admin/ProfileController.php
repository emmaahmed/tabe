<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', \App\Models\Notification::class);

        $users = User::where('account_type', 'client')->select('id', 'name')->get();

        return view('admin.profile.index', compact('users'));
    }

    public function store(Request $request)
    {
        User::update([
            ''
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

}
