<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CommercialAdvertisementsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Commercial\UpdateCommercialAdvertisementRequest;
use App\Models\CommercialAdvertisement;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class CommercialAdvertisementsController extends Controller
{
    public function index(CommercialAdvertisementsDataTable $dataTable)
    {
        $this->authorize('viewAny', CommercialAdvertisement::class);


        return $dataTable->render('admin.commercial_advertisements.index');
    }
    public function changeStatus($id)
    {
        $commercialAdvertisement = CommercialAdvertisement::find($id);

        $approved = $commercialAdvertisement->approved == 0 ? 1 : 0;

        $commercialAdvertisement->update([ 'approved' => $approved ]);

        return back()->with('success', __('The action ran successfully!'));
    }
        public function update(UpdateCommercialAdvertisementRequest $request, $id)
    {
        $this->authorize('update', CommercialAdvertisement::class);
        $commercialAdvertisement = CommercialAdvertisement::find($id);


        if ($request->hasFile('image')) {
            $commercialAdvertisement->update([ 'image' => $request->image ]);
            $commercialAdvertisement->update([
                'start_date'     => $commercialAdvertisement->type=='commercial_banners'?Carbon::now()->toDateTimeString():null,
                'end_date'    => $commercialAdvertisement->type=='commercial_banners'?Carbon::now()->addMonths($commercialAdvertisement->period)->toDateTimeString():null,
            ]);

        }


        return back()->with('success', __('The action ran successfully!'));
    }



//    public function store(StoreBannerRequest $request)
//    {
//        $this->authorize('create', Banner::class);
//
//        Banner::create([
//            'link_type'     => $request->link_type,
//            'country_id'    => $request->country_id == 'all' ? Null : $request->country_id,
//            'link_id'       => $request->company_id,
//            'website_link'  => $request->website_link,
//            'image'         => $request->image,
//            'large_image'   => $request->large_image?$request->large_image:null,
//        ]);
//
//        return back()->with('success', __('The action ran successfully!'));
//    }
//
//    public function update(UpdateBannerRequest $request, Banner $banner)
//    {
//        $this->authorize('update', Banner::class);
//
//        $banner->update([
//            'link_type'     => $request->edit_link_type,
//            'country_id'    => $request->edit_country_id  == 'all' ? Null : $request->edit_country_id,
//            'link_id'       => $request->edit_company_id,
//            'website_link'  => $request->edit_website_link,
//        ]);
//
//        if ($request->hasFile('edit_image')) {
//            $banner->update([ 'image' => $request->edit_image ]);
//
//            if(!empty($banner->image)) {
//                Storage::delete($banner->image);
//            }
//        }
//
//        if ($request->hasFile('edit_large_image')) {
//            $banner->update([ 'large_image' => $request->edit_large_image ]);
//
//            if(!empty($banner->large_image)) {
//                Storage::delete($banner->large_image);
//            }
//        }
//        if (!$request->hasFile('edit_large_image')) {
//
//            $banner->update(['large_image' => null]);
//
//            if(!empty($banner->large_image)) {
//                Storage::delete($banner->large_image);
//            }
//        }
//
//        return back()->with('success', __('The action ran successfully!'));
//    }
//
    public function destroy($id)
    {
        $commercialAdvertisement = CommercialAdvertisement::find($id);

        Storage::delete($commercialAdvertisement->image);

        $commercialAdvertisement->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
