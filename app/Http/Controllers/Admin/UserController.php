<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UsersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\StoreUserRequest;
use App\Http\Requests\Admin\Users\UpdateUserRequest;
use App\Models\Country;
use App\Models\User;
use App\Services\External\Notification;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        $this->authorize('viewAny', User::class);

        $countries = Country::all(['id', 'name']);

        return $dataTable->render('admin.users.index', compact('countries'));
    }

    public function store(StoreUserRequest $request)
    {
        $this->authorize('create', User::class);

        User::create([
            'active'        => 1 ,
            'account_type'  => 'client',
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'country_id'    => $request->country_id,
            'image'         => $request->image,
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', User::class);

        $user->update([
            'name'          => $request->edit_name,
            'email'         => $request->edit_email,
            'phone'         => $request->edit_phone,
            'country_id'    => $request->edit_country_id,
        ]);

        if($request->edit_password) {
            $user->update([ 'password' => bcrypt($request->edit_password) ]);
        }

        if ($request->hasFile('edit_image')) {
            $user->update([ 'image' => $request->edit_image ]);

            if(!empty($user->image)) {
                Storage::delete($user->image);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', User::class);

        Storage::delete($user->image);

        $user->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function sendNotification($userId)
    {
        Notification::notify('admin', $userId, null, null,
            __('Tabe'), request()->text);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function changeStatus($userId)
    {
        $user = User::find($userId);

        $status = $user->active == 1 ? 2 : 1;

        $user->update([ 'active' => $status ]);
        if($status==2) {
            Notification::notify('deactivated', $user->id, auth()->id(), null,
                __('Tabe'), __('Your Account Has Been Suspended!'));
        }else{
            Notification::notify('deactivated', $user->id, auth()->id(), null,
                __('Tabe'), __('Account activated Successfully'));

        }


        return back()->with('success', __('The action ran successfully!'));
    }
    public function changeNotification($userId)
    {
        $user = User::find($userId);

        $status = $user->company_notification_flag == 1 ? 2 : 1;

        $user->update([ 'company_notification_flag' => $status ]);

        return back()->with('success', __('The action ran successfully!'));
    }
}
