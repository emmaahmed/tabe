<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Cities\StoreCityRequest;
use App\Http\Requests\Admin\Cities\UpdateCityRequest;
use App\Models\City;
use App\Models\Governorate;

class CityController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', City::class);

        $governorates = Governorate::whereNull('translation_id')->get();

        return view('admin.locations.cities.index', compact('governorates'));
    }

    public function store(StoreCityRequest $request)
    {
        $this->authorize('create', City::class);

        City::create([
            'name'          => $request->name,
            'governorate_id' => $request->governorate_id,
            'language'  => 'ar',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateCityRequest $request, City $city)
    {
        $this->authorize('update', City::class);

        $city->update([
            'name'          => $request->edit_name,
            'governorate_id' => $request->edit_governorate_id,
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', City::class);

        $city = City::findOrFail($id);

        $city->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
