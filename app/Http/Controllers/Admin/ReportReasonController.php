<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ReportReasons\StoreReportReasonRequest;
use App\Http\Requests\Admin\ReportReasons\UpdateReportReasonRequest;
use App\Models\ReportReason;
use App\Models\Setting;

class ReportReasonController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Setting::class);

        return view('admin.settings.report_reasons.index');
    }

    public function store(StoreReportReasonRequest $request)
    {
        $this->authorize('create', Setting::class);

        ReportReason::create([
            'reason' => $request->reason
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateReportReasonRequest $request, $id)
    {
        $this->authorize('update', Setting::class);

        ReportReason::find($id)->update([
            'reason' => $request->edit_reason
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', Setting::class);

        ReportReason::find($id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
