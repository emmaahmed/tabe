<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\ProperitiesImport;
use App\Models\AttributeCategory;
use App\Models\Category;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class PropertiesController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', Category::class);

        $sub_categories = Category::find($request->category_id)->subcategory()->pluck('id');

//        $attributes =
//        AttributeCategory::where('category_id', $request->category_id)
//        ->orWhereIn('sub_category_id', $sub_categories)
//        ->with('translation')
//        ->get();
        $attributes = Property::whereHas('categories', function ($query) use ($sub_categories,$request) {
            $query->whereIn('category_id', $sub_categories)->orWhere('category_id',$request->category_id);
        })
            ->get();


        return view('admin.categories.properties.index', compact('attributes'));
    }

    public function show($id)
    {
        $this->authorize('viewAny', Category::class);

//        $attributes =
//            AttributeCategory::where('parent_id', $id)
//                ->with('parent', 'translation')
//                ->get();
        $attributes =
            Property::where('parent_id', $id)
                ->with('parent')
                ->get();

//        $category_id = Property::findOrFail($id)->category_id;
//        $category_id = 5;

        return view('admin.categories.properties.show', compact('attributes', 'id'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', Category::class);
//        dd($request->all());

        if ($request->name) {
//            $attribute = AttributeCategory::create([
//                'category_id' => $request->category_id,
//                'key' => $request->key_ar,
//                'value_type' => $request->value_type,
//                'language' => 'ar'
//            ]);
//
//            $attribute->translation()->updateOrCreate([
//                'key' => $request->key_en,
//                'language' => 'en'
//            ]);
            $attribute = Property::create([
//                'category_id' => $request->category_id,
                'name' => $request->name,
                'type' => $request->value_type,
                'property_type' => $request->property_type,
                'image'                     => $request->image,

                'language' => 'ar'
            ]);

//            $attribute->translation()->updateOrCreate([
//                'name' => $request->key_en,
//                'language' => 'en'
//            ]);
            $attribute->categories()->attach($request->category_id);
//            dd($attribute);
        }

        if ($request->value_ar) {
            $value = Property::create([
                'parent_id' => $request->parent_id,
                'name' => $request->value_ar,
                'image' => $request->image,

                'language' => 'ar'
            ]);

//            $value->translation()->updateOrCreate([
//                'name' => $request->value_en,
//                'language' => 'en'
//            ]);
        }


        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update', Category::class);

        $attributeCategory = Property::findOrFail($id);
        if ($request->edit_value_ar) {
            $attributeCategory->update([
                'name' => $request->edit_value_ar,
                'type' => $request->edit_value_type,
                'property_type' => $request->property_type,
            ]);

            if ($request->edit_value_type == 'text') {
                $attributeCategory->values()->delete();
            }
            if($request->edit_image) {
                $attributeCategory->update([
                    'image' => $request->edit_image
                ]);
            }


//            $attributeCategory->translation()->updateOrCreate(
//                [
//                    'translation_id' => $attributeCategory->id,
//                    'language' => 'en'
//                ],
//                [
//                    'name' => $request->edit_key_en,
//                    'language' => 'en'
//                ]
//            );
        }

        if ($request->edit_value_ar) {
            $attributeCategory->update([
                'name' => $request->edit_value_ar,
            ]);

//            $attributeCategory->translation()->updateOrCreate(
//                [
//                    'translation_id' => $attributeCategory->id,
//                    'language' => 'en'
//                ],
//                [
////                    'value' => $request->edit_value_en,
//                    'name' => $request->edit_value_text_en,
//                    'language' => 'en'
//                ]
//            );
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', Category::class);

        $attributeCategory = Property::findOrFail($id);

        if ($attributeCategory->advertisements()->count()) {
            return back()->with('error', 'Attributes cannot be deleted because of related ads');
        }
//        $attributeCategory->translation()->delete();

        $attributeCategory->delete();


        if ($attributeCategory->values()->count()) {
            $attributeCategory->values()->delete();
        }

        return back()->with('success', __('The action ran successfully!'));
    }
    public function excel(Request $request)
    {
        Excel::import(new ProperitiesImport(), $request->file);

        return back()->with('success', __('The action ran successfully!'));
    }

}
