<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Packages\StorePackageRequest;
use App\Http\Requests\Admin\Categories\UpdateCategoryRequest;
use App\Imports\CategoriesImport;
use App\Models\Category;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class PackagesController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Package::class);

        return view('admin.packages.index');
    }

    public function show(Package $package)
    {
        $this->authorize('view', Package::class);

        return view('admin.packages.show', compact('package'));
    }

    public function store(StorePackageRequest $request)
    {
        $this->authorize('create', Package::class);

        Package::create($request->validated());

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(StorePackageRequest $request, Package $package)
    {
        $this->authorize('update', Category::class);
//dd($request->validated());
        $package->update($request->validated());
        if ($request->hasFile('image')) {
            $package->update([ 'image' => $request->image ]);

            if(!empty($package->image)) {
                Storage::delete($package->image);
            }
        }


        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Package $package)
    {
        $this->authorize('delete', Package::class);


        $package->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function excel(Request $request)
    {
        Excel::import(new CategoriesImport, $request->file);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function changeStatus($package_id)
    {
        $status = request()->status == "true" ? 1 : 0;

        Package::find($package_id)->update([ 'active' => $status ]);
    }

}
