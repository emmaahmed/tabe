<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CompaniesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Companies\StoreCompanyRequest;
use App\Http\Requests\Admin\Companies\UpdateCompanyRequest;
use App\Models\Category;
use App\Models\Country;
use App\Models\Package;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function index(CompaniesDataTable $dataTable)
    {
        $this->authorize('viewAny', 'Company');

        $countries = Country::all(['id', 'name']);
        $categories = Category::where('is_company',1)->get(['id','name']);
        $packages = Package::all();

        return $dataTable->render('admin.companies.index', compact('countries','categories','packages'));
    }

    public function store(StoreCompanyRequest $request)
    {
        $this->authorize('create', 'Company');

        $company = User::create([
            'active'        => 1 ,
            'account_type'  => 'company',
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'category_id'    => $request->category_id,
            'landline_phone'    => $request->landline_phone,
            'country_id'    => $request->country_id,
            'description'   => $request->description,
            'image'         => $request->image,
            'package_id'         => $request->package_id,
        ]);

        if($request->images) {
            foreach ($request->images as $image) {
                $company->images()->create([
                    'image' => $image
                ]);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateCompanyRequest $request, $id)
    {
        $this->authorize('update', 'Company');

        $company = User::findOrFail($id);

        $company->update([
            'name'          => $request->edit_name,
            'email'         => $request->edit_email,
            'phone'         => $request->edit_phone,
            'country_id'    => $request->edit_country_id,
            'category_id'    => $request->edit_category_id,
            'landline_phone'    => $request->edit_landline_phone,

            'description'   => $request->edit_description,
            'package_id'   => $request->edit_package_id,
        ]);

        if($request->edit_password) {
            $company->update([ 'password' => bcrypt($request->edit_password) ]);
        }

        if ($request->hasFile('edit_image')) {
            $company->update([ 'image' => $request->edit_image ]);

            if(!empty($company->image)) {
                Storage::delete($company->image);
            }
        }

        if($request->hasFile('edit_images')) {

            $company->images()->delete();

            foreach ($request->edit_images as $image) {
                $company->images()->create([
                    'image' => $image
                ]);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', 'Company');

        $company = User::findOrFail($id);

        Storage::delete($company->image);

        $company->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
