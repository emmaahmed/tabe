<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\AttributesImport;
use App\Models\AttributeCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class AttributeCategoryController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', Category::class);

        $attributes = AttributeCategory::where('category_id', $request->category_id)->get();

        return view('admin.categories.fields.index', compact('attributes'));
    }

    public function show($id)
    {
        $this->authorize('viewAny', Category::class);

        $attributes =
        AttributeCategory::where('parent_id', $id)
        ->with('parent')
        ->orderBy('value', 'desc')
        ->get();

        $category_id = AttributeCategory::findOrFail($id)->category_id;

        return view('admin.categories.fields.show', compact('attributes', 'id', 'category_id'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', Category::class);

        if ($request->key) {
            AttributeCategory::create([
                'category_id'               => $request->category_id,
                'key'                       => $request->key,
                'attribute_type'            => $request->attribute_type,
                'graphical_control_element' => $request->graphical_control_element,
                'used_tag'                  => $request->used_tag ? 1 : 0,
                'image'                     => $request->image,
                'language'                  => 'ar'
            ]);
        }

        if ($request->value) {
            AttributeCategory::create([
                'parent_id'     => $request->parent_id,
                'value'         => $request->value,
                'language'      => 'ar'
            ]);
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update', Category::class);

        $attributeCategory = AttributeCategory::findOrFail($id);

        if ($request->edit_key) {
            $attributeCategory->update([
                'key'                       => $request->edit_key,
                'attribute_type'            => $request->edit_attribute_type,
                'graphical_control_element' => $request->edit_graphical_control_element,
                'used_tag'                  => $request->edit_used_tag ? 1 : 0,
                'language'                  => 'ar'
            ]);
        }

        if($request->edit_image) {
            $attributeCategory->update([
                'image' => $request->edit_image
            ]);
        }

        if ($request->edit_value) {
            $attributeCategory->update([
                'value' => $request->edit_value,
            ]);
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', Category::class);

        $attributeCategory = AttributeCategory::findOrFail($id);

        if ($attributeCategory->values()->count() || $attributeCategory->advertisements()->count()) {
            return back()->with('error', 'Attributes cannot be deleted because of related ads');
        }

        $attributeCategory->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function excel(Request $request)
    {
        Excel::import(new AttributesImport, $request->file);

        return back()->with('success', __('The action ran successfully!'));
    }
}
