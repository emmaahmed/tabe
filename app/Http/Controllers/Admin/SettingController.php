<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function show($type)
    {
        $this->authorize('viewAny', Setting::class);

        return view('admin.settings.index', compact('type'));
    }

    public function update(Request $request, $type)
    {
        $this->authorize('update', Setting::class);

        $setting = Setting::where('key', $type)->first();

        $setting->update([
            'value' => $request->edit_value
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }
}
