<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Countries\StoreCountryRequest;
use App\Http\Requests\Admin\Countries\UpdateCountryRequest;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CountryController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Country::class);

        return view('admin.locations.countries.index');
    }

    public function store(StoreCountryRequest $request)
    {
        $this->authorize('create', Country::class);

        Country::create([
            'name'      => $request->name,
            'currency'  => $request->currency,
            'image'     => $request->image,
            'language'  => 'ar',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateCountryRequest $request, Country $country)
    {
        $this->authorize('update', Country::class);

        $country->update([
            'name'      => $request->edit_name,
            'currency'  => $request->edit_currency,
        ]);

        if ($request->hasFile('edit_image')) {
            $country->update([ 'image' => $request->edit_image ]);

            if(!empty($country->image)) {
                Storage::delete($country->image);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Country $country)
    {
        $this->authorize('delete', Country::class);

        Storage::delete($country->image);

        $country->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
