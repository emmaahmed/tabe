<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Advertisement::class);


        return view('admin.advertisements.comments');
    }

    public function destroy(Comment $comment)
    {
        $this->authorize('delete', Advertisement::class);

        $comment->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
