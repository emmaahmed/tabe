<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function reportStatistics()
    {
        $this->authorize('viewAny', 'Dashboard');

        $clients_count = User::where('account_type', 'client')->count();

        $advertisements_count = Advertisement::count();

        $advertisements_avg =(int) Advertisement::avg('price');

        $advertisements_profit = DB::table('advertisement_premium')->sum('cost');

        $advertisement_current_month = Advertisement::whereMonth('created_at', now()->month)->count();

        $advertisement_last_month = Advertisement::whereMonth('created_at', now()->subMonth()->month)->count();

        $last_profit_month_advertisement =
        DB::table('advertisement_premium')->whereMonth('created_at', now()->subMonth()->month)->sum('cost');

        $current_profit_month_advertisement =
        DB::table('advertisement_premium')->whereMonth('created_at', now()->month)->sum('cost');

        return view('admin.dashboard.index',
            compact('clients_count', 'advertisements_count', 'advertisements_avg',
            'advertisements_profit', 'advertisement_current_month', 'advertisement_last_month',
            'current_profit_month_advertisement', 'last_profit_month_advertisement'));
    }
}
