<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Notifications\StoreNotificationRequest;
use App\Models\Country;
use App\Models\PersonalDeviceToken;
use App\Models\User;
use App\Services\External\FCM;
use App\Services\External\Notification;

class NotificationController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', \App\Models\Notification::class);

        $users = User::where('account_type', 'client')->select('id', 'name')->get();
        $companies = User::where('account_type', 'company')->select('id', 'name')->get();
        $countries=Country::all();

        return view('admin.notifications.index', compact('users','companies','countries'));
    }

    public function store(StoreNotificationRequest $request)
    {
        $this->authorize('create', \App\Models\Notification::class);
//dd($request->validated());

        if ($request->notify_type == 'per_companies') {

//            Notification::notify('admin', $request->notify_company, null, null,

//                __('Tabe'), $request->text);
            \App\Models\Notification::create([
                'type'  => 'admin',
                'title'  => $request->title,
                'text'  => $request->text,
                'is_all'=> 2
            ]);

            User::where(['account_type' => 'company', 'active' => 1])
                ->chunk(100, function ($users) use ($request) {
                    foreach ($users as $user) {
                        $user->increment('notification_count');


                        $device_token = PersonalDeviceToken::where('user_id', $user->id)->pluck('device_token')->toArray();

                        $data = [
                            'type'  => 'admin',
                            'title' => __('Tabe'),
                            'body'  => $request->text,
                            'sound' => 'default',
                        ];

                        FCM::send($device_token, $data);
                    }
                });

        }
        elseif ($request->notify_type == 'per_user') {
            \App\Models\Notification::create([
                'type'  => 'admin',
                'title'  => $request->title,
                'text'  => $request->text,
                'is_all'=> 1
            ]);

            User::where(['active' => 1,'account_type' => 'client'])
                ->chunk(100, function ($users) use ($request) {
                    foreach ($users as $user) {


                            $user->increment('notification_count');

                            $device_token = PersonalDeviceToken::where('user_id', $user->id)->pluck('device_token')->toArray();



                        $data = [
                            'type'  => 'admin',
                            'title' => $request->title,
                            'body'  => $request->text,
                            'sound' => 'default',
                        ];
//                        if($user->id==2) {

                            FCM::send($device_token, $data);
//                        }
                    }
                });

        }
        else {
            \App\Models\Notification::create([
                'type'  => 'admin',
                'title'  => $request->title,
                'text'  => $request->text,
                'is_all'=> 3,
                'country_id'=>$request->notify_country
            ]);

            User::where(['country_id' => $request->notify_country, 'active' => 1])
                ->chunk(100, function ($users) use ($request) {
                    foreach ($users as $user) {
                        $user->increment('notification_count');


                        $device_token = PersonalDeviceToken::where('user_id', $user->id)->pluck('device_token')->toArray();

                        $data = [
                            'type'  => 'admin',
                            'title' => $request->title,
                            'body'  => $request->text,
                            'sound' => 'default',
                        ];

                        FCM::send($device_token, $data);
                    }
                });
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy($id)
    {
        $this->authorize('delete', \App\Models\Notification::class);

        \App\Models\Notification::find($id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
