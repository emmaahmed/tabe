<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Welcomes\StoreWelcomeRequest;
use App\Http\Requests\Admin\Welcomes\UpdateWelcomeRequest;
use App\Models\Welcome;

class WelcomeController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Welcome::class);


        return view('admin.welcomes.index');
    }

    public function store(StoreWelcomeRequest $request)
    {
        $this->authorize('create', Welcome::class);

        Welcome::create([
            'title'     => $request->title,
            'content'   => $request->text,
            'image'     => $request->image
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateWelcomeRequest $request, Welcome $welcome)
    {
        $this->authorize('update', Welcome::class);

        $welcome->update([
            'title'    => $request->edit_title,
            'content'  => $request->edit_content
        ]);

        if ($request->hasFile('edit_image')) {
            $welcome->update([ 'image' => $request->edit_image ]);
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Welcome $welcome)
    {
        $this->authorize('delete', Welcome::class);

        $welcome->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
