<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Products\StoreProductRequest;
use App\Http\Requests\Admin\Products\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Product::class);

        $categories = Category::with('translation')
            ->whereNull('translation_id')
            ->whereNotNull('parent_id')
            ->get();
        return view('admin.products.index', compact('categories'));
    }

    public function store(StoreProductRequest $request)
    {
        $this->authorize('create', Product::class);

        $product = Product::create([
            'name'              => $request->name_ar,
            'category_id'       => $request->category_id,
            'sku'               => $request->sku,
            'min_price'         => $request->min_price,
            'max_price'         => $request->max_price,
            'stock_quantity'    => $request->stock_quantity,
            'onsale'            => $request->onsale ? 1 : 0,
            'stock_status'      => $request->stock_status,
            'brand'             => $request->brand,
            'weight'            => $request->weight,
            'type'              => $request->type,
            'validity_period'   => $request->validity_period,
            'is_new'            => $request->is_new ? 1 : 0,
            'description'       => $request->description_ar,
            'language'          => 'ar',
        ]);

        foreach ($request->images as $image) {
            $product->images()->create([
                'image' => $image
            ]);
        }

        $product->translations()->create([
            'name'          => $request->name_en,
            'description'   => $request->description_en,
            'language'      => 'en',
        ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $this->authorize('update', $product);

        $product->update([
            'name'              => $request->edit_name_ar,
            'category_id'       => $request->edit_category_id,
            'sku'               => $request->edit_sku,
            'min_price'         => $request->edit_min_price,
            'max_price'         => $request->edit_max_price,
            'stock_quantity'    => $request->edit_stock_quantity,
            'onsale'            => $request->edit_onsale,
            'stock_status'      => $request->edit_stock_status,
            'brand'             => $request->edit_brand,
            'weight'            => $request->edit_weight,
            'type'              => $request->edit_type,
            'validity_period'   => $request->edit_validity_period,
            'is_new'            => $request->edit_is_new,
            'description'       => $request->edit_description_ar,
        ]);

        $product->translation()->update([
            'name'          => $request->edit_name_en,
            'description'   => $request->edit_description_en,
        ]);

        if ($request->hasFile('edit_image')) {
            $product->update([ 'image' => $request->edit_image ]);

            if(!empty($product->image)) {
                Storage::delete($product->image);
            }
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        Storage::delete($product->image);
        $product->delete();
        $product->translation()->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
}
