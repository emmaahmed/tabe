<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AdminsDataTable;
use App\DataTables\HistoryDataTable;
use App\DataTables\HistoryLogDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admins\StoreAdminRequest;
use App\Http\Requests\Admin\Admins\UpdateAdminRequest;
use App\Models\AdminHistory;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Models\Activity;

class AdminController extends Controller
{
    public function index(AdminsDataTable $dataTable)
    {
        $this->authorize('viewAny', 'Admin');

        return $dataTable->render('admin.admins.index');
    }

    public function store(StoreAdminRequest $request)
    {
        $this->authorize('create', 'Admin');

        $admin = User::create([
            'active'        => 1 ,
            'account_type'  => 'admin',
            'name'          => $request->name,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'password'      => bcrypt($request->password),
            'image'         => $request->image,
        ]);

        $permissions = Permission::whereIn('slug', $request->permissions)->pluck('id');

        $admin->permissions()->sync($permissions);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function show(User $admin, HistoryLogDataTable $dataTable)
    {
        $this->authorize('viewAny', 'Admin');

        return $dataTable->render('admin.admins.history_log.index', compact('admin'));
    }

    public function update(UpdateAdminRequest $request, $id)
    {
        $this->authorize('update', 'Admin');

        $admin = User::findOrFail($id);

        $admin->update([
            'name'  => $request->edit_name,
            'email' => $request->edit_email,
        ]);

        if($request->edit_password) {
            $admin->update([ 'password' => bcrypt($request->edit_password) ]);
        }

        if ($request->hasFile('edit_image')) {
            $admin->update([ 'image' => $request->edit_image ]);

            if(!empty($admin->image)) {
                Storage::delete($admin->image);
            }
        }

        if($request->permissions) {
            $permissions = Permission::whereIn('slug', $request->permissions)->pluck('id');

            $admin->permissions()->sync($permissions);
        }

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroy(User $admin)
    {
        $this->authorize('delete', 'Admin');

        $admin->permissions()->detach();

        Storage::delete($admin->image);

        $admin->delete();

        return back()->with('success', __('The action ran successfully!'));
    }

    public function changeStatus($userId)
    {
        $user = User::find($userId);

        $status = $user->active == 1 ? 2 : 1;

        $user->update([ 'active' => $status ]);

        return back()->with('success', __('The action ran successfully!'));
    }

    public function destroyHistoryLog($id)
    {
        $this->authorize('viewAny', 'Admin');

        Activity::find($id)->delete();

        return back()->with('success', __('The action ran successfully!'));
    }
    public function loginHistory($admin_id)
    {
        $this->authorize('viewAny', 'Admin');

        $histories=User::find($admin_id)->history;

        return view('admin.admins.login_history.index',compact('histories','admin_id'));
    }
}
