<?php

namespace App\Http\Requests\Admin\ReportReasons;

use Illuminate\Foundation\Http\FormRequest;

class UpdateReportReasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_reason'   => ['required', 'unique:report_reasons,reason,'.request()->report_reason],
        ];
    }
}
