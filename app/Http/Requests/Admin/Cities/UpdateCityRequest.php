<?php

namespace App\Http\Requests\Admin\Cities;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name'          => ['required', 'string', 'max:250', 'unique:cities,name,'.request()->city->id.',id'],
            'edit_governorate_id' => ['required', 'exists:governorates,id']
        ];
    }
}
