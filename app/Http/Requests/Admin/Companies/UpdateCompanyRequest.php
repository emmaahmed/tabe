<?php

namespace App\Http\Requests\Admin\Companies;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name'         => ['required', 'string'],
            'edit_landline_phone'         => ['required'],
            'edit_email'        => ['nullable', 'email'],
            'edit_phone'        => ['required', 'string'],
            'edit_password'     => ['nullable', 'min:6', 'max:100'],
            'edit_image'        => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'edit_description'  => ['nullable', 'string', 'max:350'],
            'edit_images'       => ['nullable', 'array', 'min:1'],
            'edit_images.*'     => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
        ];
    }
}
