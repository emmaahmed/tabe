<?php

namespace App\Http\Requests\Admin\Companies;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => ['required', 'string', 'max:200', 'unique:users,name,NULL,id,deleted_at,NULL,account_type,client'],
            'email'         => ['nullable', 'email', 'max:200', 'unique:users,email,NULL,id,deleted_at,NULL,account_type,client'],
            'phone'         => ['required', 'string', 'unique:users,phone,NULL,id,deleted_at,NULL,account_type,client'],
            'password'      => ['required', 'string', 'min:6', 'max:100'],
            'image'         => ['sometimes', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'description'   => ['nullable', 'string', 'max:350'],
            'images'        => ['nullable', 'array', 'min:1'],
            'images.*'      => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
            'category_id'      => ['required'],
            'landline_phone'      => ['nullable'],
            'package_id'      => ['nullable','exists:packages,id'],
        ];
    }
}
