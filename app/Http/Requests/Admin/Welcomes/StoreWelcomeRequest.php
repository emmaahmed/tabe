<?php

namespace App\Http\Requests\Admin\Welcomes;

use Illuminate\Foundation\Http\FormRequest;

class StoreWelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'    => ['required'],
            'text'     => ['required'],
            'image'    => ['required', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
