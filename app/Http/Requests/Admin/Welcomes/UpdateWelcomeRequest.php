<?php

namespace App\Http\Requests\Admin\Welcomes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateWelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_title'    => ['required'],
            'edit_content'  => ['required'],
            'edit_image'    => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
