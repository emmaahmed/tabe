<?php

namespace App\Http\Requests\Admin\Areas;

use Illuminate\Foundation\Http\FormRequest;

class StoreAreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => ['required', 'string', 'max:250', 'unique:areas,name,ar,language'],
            'city_id' => ['required', 'exists:cities,id']
        ];
    }
}
