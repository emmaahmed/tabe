<?php

namespace App\Http\Requests\Admin\Advertisements;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdvertisementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_title'            => ['required', 'string', 'max:200'],
            'edit_category_id'      => ['required', 'exists:categories,id'],
            'edit_price'            => ['required', 'numeric', 'min:1', 'max:999999999'],
            'edit_country_id'       => ['required', 'exists:countries,id'],
            'edit_negotiable'       => ['nullable'],
            'edit_premium'          => ['nullable'],
            'edit_description'      => ['nullable', 'string', 'max:450'],
            'edit_ended_at'         => ['nullable'],
            'edit_images'           => ['nullable', 'array', 'distinct', 'min:1'],
            'edit_images.*'         => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
        ];
    }
}
