<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name'      => ['required', 'string'],
            'edit_email'     => ['nullable', 'email'],
            'edit_phone'     => ['required', 'string'],
            'edit_password'  => ['nullable', 'min:6', 'max:100'],
            'edit_image'     => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
