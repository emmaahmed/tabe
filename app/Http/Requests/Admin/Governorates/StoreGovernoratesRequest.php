<?php

namespace App\Http\Requests\Admin\Governorates;

use Illuminate\Foundation\Http\FormRequest;

class StoreGovernoratesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => ['required', 'string', 'max:250', 'unique:governorates,name,ar,language'],
            'country_id'    => ['required', 'string', 'max:250', 'exists:countries,id,translation_id,NULL'],
        ];
    }
}
