<?php

namespace App\Http\Requests\Admin\Governorates;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGovernoratesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name'         => ['required', 'string', 'max:250'],
            'edit_country_id'   => ['required', 'string', 'max:250', 'exists:countries,id,translation_id,NULL'],
        ];
    }
}
