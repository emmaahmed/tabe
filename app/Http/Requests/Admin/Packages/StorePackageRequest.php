<?php

namespace App\Http\Requests\Admin\Packages;

use Illuminate\Foundation\Http\FormRequest;

class StorePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ar'          => ['required', 'string', 'max:100'],
            'image'    => ['nullable'],
            'period'    => ['required'],
            'advertisements_count'         => ['required'],
            'price'         => ['required'],
            'desc_ar'         => ['nullable'],
        ];
    }
}
