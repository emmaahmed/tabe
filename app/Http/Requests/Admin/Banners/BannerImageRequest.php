<?php

namespace App\Http\Requests\Admin\Banners;

use Illuminate\Foundation\Http\FormRequest;

class BannerImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_arabic_images'    => ['required_with:edit_english_images', 'distinct', 'array', 'min:1', 'max:5'],
            'edit_arabic_images.*'  => ['required', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
            'edit_english_images'   => ['required_with:edit_arabic_images', 'distinct', 'array', 'min:1', 'max:5'],
            'edit_english_images.*' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
        ];
    }
}
