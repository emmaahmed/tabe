<?php

namespace App\Http\Requests\Admin\Banners;

use Illuminate\Foundation\Http\FormRequest;

class StoreBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link_type'         => ['required', 'in:public,company,website'],
            'company_id'        => ['nullable', 'exists:users,id,account_type,company'],
            'website_link'      => ['nullable', 'string', 'max:190'],
            'image'             => ['required', 'mimes:jpeg,jpg,png,gif', 'max:20000'],
            'large_image'       => ['nullable','mimes:jpeg,jpg,png,gif', 'max:20000'],
        ];
    }
}
