<?php

namespace App\Http\Requests\Admin\Categories\Attributes;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttributeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => ['required', 'string', 'max:100', 'unique:categories,name,ar,language,deleted_at,NULL'],
            'name_ar' => ['required', 'string', 'max:100', 'unique:categories,name,en,language,deleted_at,NULL'],
            'image'   => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
