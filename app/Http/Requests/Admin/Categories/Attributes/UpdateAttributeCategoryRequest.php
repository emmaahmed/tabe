<?php

namespace App\Http\Requests\Admin\Categories\Attributes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name_ar' => ['required', 'string', 'max:100', 'unique:categories,name,'.request()->category->id.',id,deleted_at,NULL,language,ar'],
            'edit_name_en' => ['required', 'string', 'max:100', 'unique:categories,name,'.request()->category->id.',translation_id,deleted_at,NULL,language,en'],
            'edit_image'   => ['sometimes', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
