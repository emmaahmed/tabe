<?php

namespace App\Http\Requests\Admin\Admins;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name'      => ['required', 'string', 'max:200'],
            'edit_email'     => ['nullable', 'email', 'max:200'],
            'edit_phone'     => ['required', 'string'],
            'edit_password'  => ['nullable', 'confirmed', 'min:6', 'max:100'],
            'edit_image'     => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ];
    }
}
