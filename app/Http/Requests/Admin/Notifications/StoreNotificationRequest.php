<?php

namespace App\Http\Requests\Admin\Notifications;

use Illuminate\Foundation\Http\FormRequest;

class StoreNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => ['required', 'string', 'max:800'],
            'text'        => ['required', 'string', 'max:800'],
            'notify_type' => ['required', 'in:per_company,per_user,per_country,per_companies'],
//            'notify_user' => ['nullable', 'exists:users,id,account_type,client'],
//            'notify_company' => ['nullable', 'exists:users,id,account_type,company'],
//            'notify_country' => ['nullable', 'exists:countries,id'],
        ];
    }
}
