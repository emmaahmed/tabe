<?php

namespace App\Http\Requests\Api\V100\Auth;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_type'  => ['required', 'in:client,company'],
            'name'          => ['required', 'string', 'max:200'],
            'category_id'          => ['required_if:account_type,==,company', 'string', 'max:200'],
            'email'         => ['required', 'email', 'max:200', 'unique:users,email,NULL,id,deleted_at,NULL,account_type,!admin'],
            'phone'         => ['required', 'string', 'unique:users,phone,NULL,id,deleted_at,NULL,account_type,!admin'],
            'password'      => ['required', 'confirmed', 'min:6'],
            'device_token'  => ['required', 'string'],
            'landline_phone'  => ['required_if:account_type,==,company', 'string'],
            'company_license'  => ['nullable', 'image'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
