<?php

namespace App\Http\Requests\Api\V100\Advertisements;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class SetAdvertisementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        if(auth()->user()->account_type!='company'){

        return [
                'title'             => ['required', 'string', 'max:190'],
                'price'             => ['required', 'max:190'],
                'negotiable'                     => ['required', 'in:0,1'],
                'contact_on'                     => ['required', 'max:15'],
                'premium'                            => ['nullable', 'in:0,1'],
                'address'                        => ['nullable', 'string', 'max:190'],
                'latitude'                       => ['nullable', 'string', 'max:100'],
                'longitude'                      => ['nullable', 'string', 'max:100'],
                'description'                    => ['nullable', 'string', 'max:500'],
                'category_id'                    => [Rule::requiredIf(auth()->user()->account_type!='company'), 'exists:categories,id,parent_id,NULL,translation_id,NULL'],
                'type_id'                    => ['required','exists:advertisement_types,id'],
                'sub_category_child_ids'         => [Rule::requiredIf(auth()->user()->account_type!='company'), 'array', 'distinct', 'min:1'],
                'sub_category_child_ids.*'       => ['required', 'exists:categories,id,parent_id,!NULL'],
                'properties'      => ['nullable', 'array', 'min:1'],
                'properties.*.id'    => [
                    'nullable',
                    Rule::exists('properties', 'id')
                ],
                'properties.*.text'    => ['nullable','string' ],
                'properties.*.value'    => ['nullable','string' ],
                'images'                         => ['nullable', 'array', 'min:1', 'max:12'],
                'images.*'                       => ['nullable', 'mimes:jpeg,jpg,png,gif'],
];
//        }
//            return [
//                'attributes' => ['required', 'array', 'min:1'],
//                'images' => ['required', 'array', 'min:1', 'max:12'],
//                'images.*' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:50000'],
//                'title' => ['required', 'string', 'max:200'],
//                'price' => ['required', 'numeric', 'min:1', 'max:999999999'],
//                'negotiable' => ['required', 'in:0,1'],
//                'contact_on' => ['required', 'string', 'max:15'],
//                'vip' => ['required', 'in:0,1'],
//                'address' => ['nullable', 'string', 'max:190'],
//                'latitude' => ['nullable', 'string', 'max:100'],
//                'longitude' => ['nullable', 'string', 'max:100'],
//                'description' => ['nullable', 'string', 'max:500'],
//            ];

    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
