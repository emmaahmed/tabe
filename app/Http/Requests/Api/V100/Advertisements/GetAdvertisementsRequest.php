<?php

namespace App\Http\Requests\Api\V100\Advertisements;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class GetAdvertisementsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => ['nullable', 'exists:categories,id,parent_id,NULL,translation_id,NULL'],
//            'tag_id'        => ['nullable', 'exists:attribute_categories,id,parent_id,!NULL,translation_id,NULL'],
            'tag_id'        => ['nullable','exists:advertisement_types,id'],

            'vip'           => ['nullable', 'in:0,1']
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
