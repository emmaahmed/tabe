<?php

namespace App\Http\Requests\Api\V100\Advertisements\Commercial;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class SetCommercialAdvertisementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'category_id'                    => ['exists:categories,id,parent_id,NULL,translation_id,NULL'],

            'type' => ['required', 'in:home_slider,commercial_banners'],
            'period' => [Rule::requiredIf($this->type == 'commercial_banners'), 'in:0,1,2,3'],
            'country_code' => ['required'],
            'phone' => ['required', 'max:15'],
            'email' => ['required', 'email', 'max:200'],
            'company_name' => ['required', 'string', 'max:190'],

            'description' => ['nullable', 'string', 'max:500'],
            'notes' => ['required', 'string', 'max:190'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
