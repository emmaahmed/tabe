<?php

namespace App\Http\Requests\Api\V100\Advertisements\Filters;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class SetFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_type' => ['required', 'in:advertise,company'],
            'title' => ['nullable', 'string', 'max:200'],
            'category_id'       => [
                'nullable'
            ],
            'sub_category_id'   => [
                'nullable',
                Rule::exists('categories', 'id')
                    ->where('active', 1)
                    ->where('parent_id', request()->category_id)
            ],
            'properties'        => ['nullable', 'array', 'min:1'],
            'properties.*.id'   => ['nullable', Rule::exists('properties', 'id')],
            'properties.*.from' => ['nullable', 'numeric', 'min:0'],
            'properties.*.to'   => ['nullable', 'numeric'],
            'country_id'        => [
                'nullable', 'array', 'min:1'
            ],
            'country_id.*'        => [
                'nullable',
                Rule::exists('countries', 'id')
            ],

//            'city_ids'          => ['required', 'array', 'min:1'],
//            'city_ids.*'        => [
//                'required',
//                Rule::exists('cities', 'id')
//                    ->where('active', 1)
//            ],
            'min_price'         => ['nullable', 'numeric', 'min:0'],
            'max_price'         => ['nullable', 'numeric'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
