<?php

namespace App\Http\Requests\Api\V100\Advertisements\Filters;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class GetFilterResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vip'             => ['required'],
            'min_price'             => ['required_with:max_price', 'numeric', 'min:0'],
            'max_price'             => ['required_with:min_price', 'numeric', 'max:999999999'],
            'value_ids'             => ['nullable', 'array', 'distinct'],
            'value_ids.*'           => ['nullable', 'exists:attribute_categories,id,category_id,NULL'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
