<?php

namespace App\Http\Requests\Api\V100\Profile;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => ['required', 'string', 'max:200'],
            'email'             => ['nullable', 'email', 'max:200', 'unique:users,email,'.request()->user()->id.',id,deleted_at,NULL,account_type,client'],
            'phone'             => ['required', 'string', 'unique:users,phone,'.request()->user()->id.',id,deleted_at,NULL,account_type,client'],
            'image'             => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'country_id'        => ['nullable', 'exists:countries,id'],
            'category_id'        => ['nullable', 'exists:categories,id'],
            'hide_phone'        => ['nullable', 'in:0,1'],
            'hide_messages'     => ['nullable', 'in:0,1'],
            'address'           => ['nullable', 'max:190'],
            'description'       => ['nullable', 'max:500'],
            'landline_phone'       => ['nullable', 'max:500'],
            'company_images'    => ['nullable', 'array', 'min:1'],
            'company_images.*'  => ['required', 'mimes:jpeg,jpg,png,gif', 'max:10000']
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
