function openModalViewDetails(log, text) {
    $('#tableViewDetails tbody').empty();

    if (log.log_name === 'user') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.old.name}</td>
            </tr>
                <tr class="bg-light">
                <th>البريد الالكتروني</th>
                <td>${log.properties.old.email}</td>
            </tr>
            <tr class="bg-light">
                <th>الهاتف</th>
                <td>${log.properties.old.phone}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.attributes.name}</td>
            </tr>
            <tr class="bg-light">
                <th>البريد الالكتروني</th>
                <td>${log.properties.attributes.email}</td>
            </tr>
            <tr class="bg-light">
                <th>الهاتف</th>
                <td>${log.properties.attributes.phone}</td>
            </tr>`
        );
    }

    if (log.log_name === 'advertisement') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.old.title}</td>
            </tr>
                <tr class="bg-light">
                <th>السعر</th>
                <td>${log.properties.old.price}</td>
            </tr>
            <tr class="bg-light">
                <th>قابل للتفاوض</th>
                <td>${log.properties.old.negotiable}</td>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.old.address}</td>
            </tr>
                <tr class="bg-light">
                <th>مميز</th>
                <td>${log.properties.old.premium}</td>
            </tr>
            </tr>
                <tr class="bg-light">
                <th>الوصف</th>
                <td>${log.properties.old.description}</td>
            </tr>
            <tr class="bg-light">
                <th>ينتهي في</th>
                <td>${moment(log.properties.old.ended_at).format('YYYY-MM-DD')}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.attributes.title}</td>
            </tr>
            <tr class="bg-light">
                <th>السعر</th>
                <td>${log.properties.attributes.price}</td>
            </tr>
            <tr class="bg-light">
                <th>قابل للتفاوض</th>
                <td>${log.properties.attributes.negotiable}</td>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.attributes.address}</td>
            </tr>
            <tr class="bg-light">
                <th>مميز</th>
                <td>${log.properties.attributes.premium}</td>
            </tr>
            <tr class="bg-light">
                <th>الوصف</th>
                <td>${log.properties.attributes.description}</td>
            </tr>
            <tr class="bg-light">
                <th>ينتهي في</th>
                <td>${moment(log.properties.attributes.ended_at).format('YYYY-MM-DD')}</td>
            </tr>`
        );
    }

    if (log.log_name === 'category') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.old.name}</td>
            </tr>
                <tr class="bg-light">
                <th>التفعيل</th>
                <td>${log.properties.old.active === 1 ? 'مفعل' : 'غير مفعل'}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.attributes.name}</td>
            </tr>
            <tr class="bg-light">
                <th>التفعيل</th>
                <td>${log.properties.attributes.active === 1 ? 'مفعل' : 'غير مفعل'}</td>
            </tr>`
        );
    }

    if (log.log_name === 'attribute_category') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.old.key}</td>
            </tr>
            <tr class="bg-light">
                <th>الاسم الفرعي</th>
                <td>${log.properties.old.value}</td>
            </tr>
            <tr class="bg-light">
                <th>نوع الخصائص</th>
                <td>${log.properties.old.attribute_type}</td>
            </tr>
            <tr class="bg-light">
                <th>نوع الحقول</th>
                <td>${log.properties.old.graphical_control_element}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.attributes.key}</td>
            </tr>
            <tr class="bg-light">
                <th>الاسم الفرعي</th>
                <td>${log.properties.attributes.value}</td>
            </tr>
            <tr class="bg-light">
                <th>نوع الخصائص</th>
                <td>${log.properties.attributes.attribute_type}</td>
            </tr>
            <tr class="bg-light">
                <th>نوع الحقول</th>
                <td>${log.properties.attributes.graphical_control_element}</td>
            </tr>`
        );
    }

    if (log.log_name === 'banner') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>نوع اللافته</th>
                <td>${log.properties.old.link_type}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>نوع اللافته</th>
                <td>${log.properties.attributes.link_type}</td>
            </tr>`
        );
    }

    if (log.log_name === 'comment') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>المحتوي</th>
                <td>${log.properties.attributes.content}</td>
            </tr>`
        );
    }

    if (log.log_name === 'message' || log.log_name === 'contact_us' || log.log_name === 'complaint_suggestion') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>رسالة</th>
                <td>${log.properties.attributes.message}</td>
            </tr>`
        );
    }

    if (log.log_name === 'country') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.old.name}</td>
            </tr>
            <tr class="bg-light">
                <th>العملة</th>
                <td>${log.properties.old.currency}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>الاسم</th>
                <td>${log.properties.attributes.name}</td>
            </tr>
            <tr class="bg-light">
                <th>العملة</th>
                <td>${log.properties.attributes.currency}</td>
            </tr>`
        );
    }

    if (log.log_name === 'notification') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>المحتوي</th>
                <td>${log.properties.attributes.text}</td>
            </tr>`
        );
    }

    if (log.log_name === 'welcome') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.attributes.title}</td>
            </tr>
            <tr class="bg-light">
                <th>المحتوي</th>
                <td>${log.properties.attributes.content}</td>
            </tr>`
        );
    }

    if (log.log_name === 'setting') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>المعلومات قبل التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.old.key}</td>
            </tr>
              <tr class="bg-light">
                <th>المحتوي</th>
                <td>${log.properties.old.value}</td>
            </tr>
            <tr class="bg-light">
                <th>المعلومات بعد التغيير</th>
            </tr>
            <tr class="bg-light">
                <th>العنوان</th>
                <td>${log.properties.attributes.key}</td>
            </tr>
            <tr class="bg-light">
                <th>المحتوي</th>
                <td>${log.properties.attributes.value}</td>
            </tr>`
        );
    }

    if (log.log_name === 'report_reason') {
        $('#tableViewDetails tbody').append(
            `<tr class="bg-light">
                <th>Old</th>
            </tr>
            <tr class="bg-light">
                <th>السبب</th>
                <td>${log.properties.old.reason}</td>
            </tr>
            <tr class="bg-light">
                <th>Changed</th>
            </tr>
            <tr class="bg-light">
                <th>السبب</th>
                <td>${log.properties.attributes.reason}</td>
            </tr>`
        );
    }

    $('#viewModal').modal('show');
}
